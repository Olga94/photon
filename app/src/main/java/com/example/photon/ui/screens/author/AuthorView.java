package com.example.photon.ui.screens.author;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.photon.R;
import com.example.photon.data.storage.dto.OwnerDto;
import com.example.photon.di.DaggerService;
import com.example.photon.mvp.views.AbstractView;
import com.example.photon.mvp.views.IAuthorView;
import com.example.photon.utils.CircularTransformation;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import butterknife.BindView;

public class AuthorView extends AbstractView<AuthorScreen.AuthorPresenter> implements IAuthorView {

    @Inject
    Picasso mPicasso;

    @BindView(R.id.list_album)
    RecyclerView mListAlbum;
    @BindView(R.id.image_author)
    ImageView mImageAuthor;
    @BindView(R.id.txt_name)
    TextView mNameAuthor;
    @BindView(R.id.txt_count_album)
    TextView mCountAlbum;
    @BindView(R.id.txt_count_card)
    TextView mCountCard;
    private AuthorAdapter adapter;

    public AuthorView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean viewOnBackPressed() {
        return false;
    }

    @Override
    protected void initDagger(Context context) {
        DaggerService.<AuthorScreen.Component>getDaggerComponent(context).inject(this);
    }

    @Override
    public void initView(OwnerDto ownerDto) {
        if (ownerDto.getAvatar() == null)
            mPicasso
                    .load(R.drawable.no_profile)
                    .transform(new CircularTransformation())
                    .into(mImageAuthor);
        else
            mPicasso
                    .load(ownerDto.getAvatar())
                    .resize(140, 140)
                    .transform(new CircularTransformation())
                    .centerCrop()
                    .into(mImageAuthor);

        mNameAuthor.setText(ownerDto.getName());
        mCountAlbum.setText(String.valueOf(ownerDto.getAlbumCount()));
        mCountCard.setText(String.valueOf(ownerDto.getPhotocardCount()));

        adapter = new AuthorAdapter(ownerDto.getAlbums());
        GridLayoutManager layoutManager = new GridLayoutManager(getContext(), 2);
        mListAlbum.setLayoutManager(layoutManager);
        mListAlbum.setAdapter(adapter);
    }

    public AuthorAdapter getAdapter() {
        return adapter;
    }
}