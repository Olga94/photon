package com.example.photon.ui.dialogs;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.photon.R;
import com.example.photon.di.DaggerService;
import com.example.photon.ui.screens.profile.ProfileScreen;
import com.example.photon.utils.Validation;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class ProfileDialog extends DialogFragment {

    @BindView(R.id.title_dialog)
    TextView mTitleDialog;
    @BindView(R.id.et_name)
    EditText mName;
    @BindView(R.id.et_login)
    EditText mLogin;
    @BindView(R.id.et_email)
    EditText mEmail;
    @BindView(R.id.et_password)
    EditText mPassword;
    @BindView(R.id.error_name)
    TextView mErrorName;
    @BindView(R.id.error_login)
    TextView mErrorLogin;
    @BindView(R.id.error_email)
    TextView mErrorEmail;
    @BindView(R.id.error_pass)
    TextView mErrorPass;
    @BindView(R.id.btn_ok)
    Button mBtnOk;
    @BindView(R.id.btn_cancel)
    Button mBtnCancel;

    private static ClickOnBtn mClickOnBtn;

    public static ProfileDialog newInstance(String name, String login, ClickOnBtn clickOnBtn) {

        Bundle args = new Bundle();
        args.putString("name", name);
        args.putString("login", login);
        mClickOnBtn = clickOnBtn;
        ProfileDialog fragment = new ProfileDialog();
        fragment.setArguments(args);
        return fragment;
    }

    public static ProfileDialog newInstance(boolean isReg, ClickOnBtn clickOnBtn) {

        Bundle args = new Bundle();
        args.putBoolean("isReg", isReg);
        mClickOnBtn = clickOnBtn;
        ProfileDialog fragment = new ProfileDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.DialogTheme);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_login, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        ButterKnife.bind(this, view);

        boolean isReg = getArguments().getBoolean("isReg");
        String name = getArguments().getString("name");
        String login = getArguments().getString("login");

        if (name != null) {
            mTitleDialog.setText("Редактирование");
            mLogin.setText(login);
            mName.setText(name);
            mEmail.setVisibility(GONE);
            mPassword.setVisibility(GONE);
        } else {
            mTitleDialog.setText(isReg ? "Регистрация" : "Вход в аккаунт");
            if (!isReg) {
                mName.setVisibility(GONE);
                mLogin.setVisibility(GONE);
            }
        }
        mLogin.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!Validation.validLogin(s.toString())) {
                    showError(mLogin, true);
                    mErrorLogin.setVisibility(VISIBLE);
                } else {
                    showError(mLogin, false);
                    mErrorLogin.setVisibility(GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        mEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!Validation.validEmail(s.toString())) {
                    showError(mEmail, true);
                    mErrorEmail.setVisibility(VISIBLE);
                } else {
                    showError(mEmail, false);
                    mErrorEmail.setVisibility(GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        mPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!Validation.validPassword(s.toString())) {
                    mErrorPass.setVisibility(VISIBLE);
                } else {
                    mErrorPass.setVisibility(GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        mName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!Validation.validName(s.toString())) {
                    mErrorName.setVisibility(VISIBLE);
                } else {
                    mErrorName.setVisibility(GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        mBtnOk.setOnClickListener(v -> {
            getDialog().dismiss();
            if (name != null) {
                mClickOnBtn.onClickEdit(mName.getText().toString(), mLogin.getText().toString());
                return;
            }
            if (isReg)
                mClickOnBtn.onClickSignUp(mName.getText().toString(), mLogin.getText().toString(),
                        mEmail.getText().toString(), mPassword.getText().toString());
            else
                mClickOnBtn.onClickSignIn(mEmail.getText().toString(), mPassword.getText().toString());
        });

        mBtnCancel.setOnClickListener(v -> getDialog().dismiss());
    }

    private void showError(EditText view, boolean isError) {
        if (isError) {
            view.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.error_edit));
            view.setTextColor(ContextCompat.getColor(getContext(), R.color.color_red));
        } else {
            view.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.border_edit_text));
            view.setTextColor(ContextCompat.getColor(getContext(), android.R.color.black));
        }
    }

    public interface ClickOnBtn {
        void onClickSignIn(String email, String pass);

        void onClickSignUp(String name, String login, String email, String pass);

        void onClickEdit(String name, String login);
    }
}
