package com.example.photon.ui.screens.upload;

import android.content.Context;
import android.util.AttributeSet;

import com.example.photon.R;
import com.example.photon.di.DaggerService;
import com.example.photon.mvp.views.AbstractView;

import butterknife.OnClick;

public class UploadView extends AbstractView<UploadScreen.UploadPresenter> {

    public UploadView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean viewOnBackPressed() {
        return false;
    }

    @Override
    protected void initDagger(Context context) {
        DaggerService.<UploadScreen.Component>getDaggerComponent(context).inject(this);
    }

    @OnClick(R.id.btn_select_photo)
    void onClickSelect() {
        mPresenter.openGallery();
    }
}
