package com.example.photon.ui.screens.find.search;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.photon.R;
import com.example.photon.data.storage.model.Tag;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.SearchViewHolder> {

    private List<Tag> mTags = new ArrayList<>();
    private Context mContext;

    public SearchAdapter(Context context) {
        mContext = context;
    }

    @Override
    public SearchViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new SearchViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_tag, parent, false));
    }

    @Override
    public void onBindViewHolder(SearchViewHolder holder, int position) {
        holder.mTag.setText("#" + mTags.get(position).getTag());
    }

    @Override
    public int getItemCount() {
        return mTags.size();
    }

    public void addItem(Tag tag) {
        mTags.add(tag);
        notifyDataSetChanged();
    }

    public List<Tag> getTags() {
        return mTags;
    }

    public class SearchViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.text)
        TextView mTag;

        public SearchViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.text)
        void onClick() {
            mTags.get(getAdapterPosition()).setCheck(!mTags.get(getAdapterPosition()).isCheck());
            if (mTags.get(getAdapterPosition()).isCheck()) {
                mTag.setTextColor(mContext.getResources().getColor(R.color.color_accent));
                mTag.setBackground(mContext.getResources().getDrawable(R.drawable.border_blue_item));
            } else {
                mTag.setTextColor(mContext.getResources().getColor(R.color.color_primary_dark));
                mTag.setBackground(mContext.getResources().getDrawable(R.drawable.border_item));
            }
        }
    }
}