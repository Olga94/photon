package com.example.photon.ui.screens.splash;

import android.content.Context;
import android.util.AttributeSet;

import com.example.photon.di.DaggerService;
import com.example.photon.mvp.views.AbstractView;
import com.example.photon.mvp.views.IView;

public class SplashView extends AbstractView<SplashScreen.SplashPresenter>{

    public SplashView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean viewOnBackPressed() {
        return false;
    }

    @Override
    protected void initDagger(Context context) {
        DaggerService.<SplashScreen.Component>getDaggerComponent(context).inject(this);
    }
}
