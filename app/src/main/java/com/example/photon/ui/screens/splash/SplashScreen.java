package com.example.photon.ui.screens.splash;

import android.os.Bundle;
import android.os.Handler;

import com.example.photon.R;
import com.example.photon.data.network.res.PhotoCardRes;
import com.example.photon.data.storage.model.Find;
import com.example.photon.di.DaggerService;
import com.example.photon.di.scopes.SplashScope;
import com.example.photon.flow.AbstractScreen;
import com.example.photon.flow.Screen;
import com.example.photon.mvp.models.SplashModel;
import com.example.photon.mvp.presenters.AbstractPresenter;
import com.example.photon.mvp.presenters.ISplashPresenter;
import com.example.photon.ui.activities.RootActivity;
import com.example.photon.ui.screens.main.MainScreen;

import dagger.Provides;
import flow.Direction;
import flow.Flow;
import flow.History;
import mortar.MortarScope;
import rx.Subscriber;

@Screen(R.layout.screen_splash)
public class SplashScreen extends AbstractScreen<RootActivity.RootComponent> {

    @Override
    public Object createScreenComponent(RootActivity.RootComponent patentComponent) {
        return DaggerSplashScreen_Component.builder()
                .module(new Module())
                .rootComponent(patentComponent)
                .build();
    }

    //region =========================== DI =====================

    @dagger.Module
    public class Module {
        @Provides
        @SplashScope
        SplashPresenter provideSplashPresenter() {
            return new SplashPresenter();
        }

        @Provides
        @SplashScope
        SplashModel provideSplashModel() {
            return new SplashModel();
        }
    }

    @dagger.Component(dependencies = RootActivity.RootComponent.class, modules = Module.class)
    @SplashScope
    public interface Component {
        void inject(SplashPresenter presenter);

        void inject(SplashView view);
    }
    //endregion

    //region =========================== Presenter =====================

    public class SplashPresenter extends AbstractPresenter<SplashView, SplashModel> implements ISplashPresenter {

        private Handler mHandler = new Handler();
        private boolean isUploadData, isTimerEnd;

        private Runnable startMainScreen = new Runnable() {
            @Override
            public void run() {
                isTimerEnd = true;
                if (isUploadData && getView() != null) {
                    startMainScreen();
                }
            }
        };

        @Override
        protected void initActionBar() {
            mRootPresenter.newActionBarBuilder()
                    .setVisible(false)
                    .build();
        }

        @Override
        protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);
            loadData();
        }

        @Override
        protected void initDagger(MortarScope scope) {
            ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
        }

        private void loadData() {
            mModel.getTags().subscribe();
            mModel.getPhotoCards()
                    .subscribe(new Subscriber<PhotoCardRes>() {
                        @Override
                        public void onCompleted() {
                            startMainScreen();
                        }

                        @Override
                        public void onError(Throwable e) {
                            if (getRootView() != null)
                                getRootView().showError(e);
                            startMainScreen();
                        }

                        @Override
                        public void onNext(PhotoCardRes photoCardRes) {

                        }
                    });
        }

        @Override
        public void startMainScreen() {
            Flow.get(getView()).setHistory(History.single(new MainScreen(new Find())), Direction.FORWARD);
        }
    }
    //endregion
}