package com.example.photon.ui.screens.album;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.photon.R;
import com.example.photon.data.storage.dto.PhotoCardDto;
import com.example.photon.di.DaggerService;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AlbumAdapter extends RecyclerView.Adapter<AlbumAdapter.AlbumViewHolder> {

    @Inject
    Picasso mPicasso;
    @Inject
    AlbumScreen.AlbumPresenter mPresenter;

    private List<PhotoCardDto> mPhotoCards = new ArrayList<>();

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        DaggerService.<AlbumScreen.Component>getDaggerComponent(recyclerView.getContext()).inject(this);
        super.onAttachedToRecyclerView(recyclerView);
    }

    public AlbumAdapter(List<PhotoCardDto> photoCards) {
        mPhotoCards = photoCards;
    }

    @Override
    public AlbumViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_card, parent, false);
        return new AlbumViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AlbumViewHolder holder, int position) {
        mPicasso
                .load(mPhotoCards.get(position).getPhoto())
                .error(R.drawable.no_image)
                .into(holder.mCard);
    }

    @Override
    public int getItemCount() {
        return mPhotoCards.size();
    }

    public class AlbumViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.card)
        ImageView mCard;

        public AlbumViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            mCard.setOnClickListener(v -> mPresenter.onClickCard(mPhotoCards.get(getAdapterPosition())));
        }
    }
}
