package com.example.photon.ui.screens.find;

import android.os.Bundle;

import com.example.photon.R;
import com.example.photon.di.DaggerService;
import com.example.photon.di.scopes.FindScope;
import com.example.photon.flow.AbstractScreen;
import com.example.photon.flow.Screen;
import com.example.photon.mvp.models.FindModel;
import com.example.photon.mvp.presenters.AbstractPresenter;
import com.example.photon.mvp.presenters.RootPresenter;
import com.example.photon.ui.activities.RootActivity;

import dagger.Provides;
import mortar.MortarScope;

@Screen(R.layout.screen_find)
public class FindScreen extends AbstractScreen<RootActivity.RootComponent> {

    @Override
    public Object createScreenComponent(RootActivity.RootComponent patentComponent) {
        return DaggerFindScreen_Component.builder()
                .module(new Module())
                .rootComponent(patentComponent)
                .build();
    }

    //region =========================== DI =====================

    @dagger.Module
    public class Module {
        @Provides
        @FindScope
        FindPresenter provideFindPresenter() {
            return new FindPresenter();
        }

        @Provides
        @FindScope
        FindModel provideFindModel() {
            return new FindModel();
        }
    }

    @dagger.Component(dependencies = RootActivity.RootComponent.class, modules = Module.class)
    @FindScope
    public interface Component {
        void inject(FindPresenter presenter);
        void inject(FindView view);
        RootPresenter getRootPresenter();
    }
    //endregion

    public class FindPresenter extends AbstractPresenter<FindView, FindModel> {


        @Override
        protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);
            getView().initView();
        }

        @Override
        protected void onSave(Bundle outState) {
            super.onSave(outState);
        }

        @Override
        protected void initActionBar() {
            mRootPresenter.newActionBarBuilder()
                    .setVisible(false)
                    .setTab(getView().getFilterPager())
                    .build();
        }

        @Override
        protected void initDagger(MortarScope scope) {
            ((Component)scope.getService(DaggerService.SERVICE_NAME)).inject(this);
        }
    }
}
