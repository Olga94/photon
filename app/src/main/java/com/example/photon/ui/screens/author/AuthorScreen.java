package com.example.photon.ui.screens.author;

import android.os.Bundle;

import com.example.photon.R;
import com.example.photon.data.storage.dto.AlbumDto;
import com.example.photon.data.storage.dto.OwnerDto;
import com.example.photon.di.DaggerService;
import com.example.photon.di.scopes.ProfileScope;
import com.example.photon.flow.AbstractScreen;
import com.example.photon.flow.Screen;
import com.example.photon.mvp.models.ProfileModel;
import com.example.photon.mvp.presenters.AbstractPresenter;
import com.example.photon.mvp.presenters.IAuthorPresenter;
import com.example.photon.mvp.presenters.RootPresenter;
import com.example.photon.ui.activities.RootActivity;
import com.example.photon.ui.screens.album.AlbumScreen;
import com.squareup.picasso.Picasso;

import dagger.Provides;
import flow.Flow;
import mortar.MortarScope;

@Screen(R.layout.screeen_author)
public class AuthorScreen extends AbstractScreen<RootActivity.RootComponent> {

    private OwnerDto mOwnerDto;

    public AuthorScreen(OwnerDto ownerDto) {
        mOwnerDto = ownerDto;
    }

    @Override
    public Object createScreenComponent(RootActivity.RootComponent patentComponent) {
        return DaggerAuthorScreen_Component.builder()
                .rootComponent(patentComponent)
                .module(new Module())
                .build();
    }

    //region =========================== DI =====================

    @dagger.Module
    public class Module {
        @Provides
        @ProfileScope
        AuthorPresenter provideAuthorPresenter() {
            return new AuthorPresenter();
        }

        @Provides
        @ProfileScope
        ProfileModel provideMainModel() {
            return new ProfileModel();
        }
    }

    @dagger.Component(dependencies = RootActivity.RootComponent.class, modules = Module.class)
    @ProfileScope
    public interface Component {
        void inject(AuthorPresenter presenter);

        void inject(AuthorView view);

        void inject(AuthorAdapter adapter);

        RootPresenter getRootPresenter();

        Picasso getPicasso();
    }
    //endregion

    public class AuthorPresenter extends AbstractPresenter<AuthorView, ProfileModel> implements IAuthorPresenter {

        @Override
        protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);
            getView().initView(mOwnerDto);
        }

        @Override
        protected void initActionBar() {
            mRootPresenter.newActionBarBuilder()
                    .setTitle("Автор")
                    .setBackArrow(true)
                    .build();
        }

        @Override
        protected void initDagger(MortarScope scope) {
            ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
        }

        @Override
        public void openAlbum(AlbumDto album) {
            Flow.get(getView()).set(new AlbumScreen(album));
        }
    }
}
