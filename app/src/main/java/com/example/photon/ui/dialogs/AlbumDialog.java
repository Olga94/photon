package com.example.photon.ui.dialogs;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.photon.R;
import com.example.photon.utils.Validation;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class AlbumDialog extends DialogFragment {

    @BindView(R.id.title_dialog)
    TextView mTitleDialog;
    @BindView(R.id.et_name)
    EditText mName;
    @BindView(R.id.et_description)
    EditText mDescription;
    @BindView(R.id.error_name)
    TextView mErrorName;
    @BindView(R.id.error_description)
    TextView mErrorDescription;
    @BindView(R.id.btn_ok)
    Button mBtnOk;
    @BindView(R.id.btn_cancel)
    Button mBtnCancel;

    private static ClickOnBtn mClickOnBtn;

    public static AlbumDialog newInstance(String name, String description, ClickOnBtn clickOnBtn) {

        Bundle args = new Bundle();
        args.putString("name", name);
        args.putString("description", description);
        mClickOnBtn = clickOnBtn;
        AlbumDialog fragment = new AlbumDialog();
        fragment.setArguments(args);
        return fragment;
    }

    public static AlbumDialog newInstance(ClickOnBtn clickOnBtn) {

        Bundle args = new Bundle();
        mClickOnBtn = clickOnBtn;
        AlbumDialog fragment = new AlbumDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.DialogTheme);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_add_album, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        ButterKnife.bind(this, view);

        String name = getArguments().getString("name");
        String description = getArguments().getString("description");

        if (name != null) {
            mTitleDialog.setText("Редактирование");
            mName.setText(name);
            mDescription.setText(description);
        } else {
            mTitleDialog.setText("Новый альбом");
        }
        mName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!Validation.validNameAlbum(s.toString())) {
                    showError(mName, true);
                    mErrorName.setVisibility(VISIBLE);
                } else {
                    showError(mName, false);
                    mErrorName.setVisibility(GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        mDescription.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!Validation.validDescription(s.toString())) {
                    showError(mDescription, true);
                    mErrorDescription.setVisibility(VISIBLE);
                } else {
                    showError(mDescription, false);
                    mErrorDescription.setVisibility(GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mBtnOk.setOnClickListener(v -> {
            getDialog().dismiss();
            if (name != null) {
                mClickOnBtn.onClickEdit(mName.getText().toString(), mDescription.getText().toString());
            } else {
                mClickOnBtn.onClickAdd(mName.getText().toString(), mDescription.getText().toString());
            }
        });

        mBtnCancel.setOnClickListener(v -> getDialog().dismiss());
    }

    private void showError(EditText view, boolean isError) {
        if (isError) {
            view.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.error_edit));
            view.setTextColor(ContextCompat.getColor(getContext(), R.color.color_red));
        } else {
            view.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.border_edit_text));
            view.setTextColor(ContextCompat.getColor(getContext(), android.R.color.black));
        }
    }

    public interface ClickOnBtn {
        void onClickAdd(String name, String description);

        void onClickEdit(String name, String description);
    }
}