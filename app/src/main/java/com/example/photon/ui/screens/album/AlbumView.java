package com.example.photon.ui.screens.album;

import android.content.Context;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.photon.R;
import com.example.photon.data.storage.dto.AlbumDto;
import com.example.photon.di.DaggerService;
import com.example.photon.mvp.views.AbstractView;
import com.example.photon.mvp.views.IAlbumView;
import com.example.photon.ui.dialogs.AlbumDialog;
import com.example.photon.ui.dialogs.MessageDialog;
import com.example.photon.utils.ConstantManager;
import com.example.photon.utils.MenuHelper;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import butterknife.BindView;

public class AlbumView extends AbstractView<AlbumScreen.AlbumPresenter> implements IAlbumView,
        PopupMenu.OnMenuItemClickListener, AlbumDialog.ClickOnBtn, MessageDialog.ClickOnOkBtn{

    @BindView(R.id.txt_title_album)
    TextView mTitleAlbum;
    @BindView(R.id.txt_count_card)
    TextView mCountCard;
    @BindView(R.id.txt_description_card)
    TextView mDescriptionCard;
    @BindView(R.id.list_card)
    RecyclerView mListCard;
    @BindView(R.id.txt_not_card)
    TextView mNotCard;

    private AlbumDto mAlbumDto;
    private PopupMenu popup;

    public AlbumView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean viewOnBackPressed() {
        return false;
    }

    @Override
    protected void initDagger(Context context) {
        DaggerService.<AlbumScreen.Component>getDaggerComponent(context).inject(this);
    }

    @Override
    public void initView(AlbumDto album) {
        mAlbumDto = album;

        mTitleAlbum.setText(album.getTitle());
        mCountCard.setText(String.valueOf(album.getPhotocards().size()));
        mDescriptionCard.setText(album.getDescription());

        if (album.getPhotocards().size() == 0) mNotCard.setVisibility(VISIBLE);

        AlbumAdapter adapter = new AlbumAdapter(album.getPhotocards());
        GridLayoutManager layoutManager = new GridLayoutManager(getContext(), 3);
        mListCard.setLayoutManager(layoutManager);
        mListCard.setAdapter(adapter);
    }

    @Override
    public void showPopupMenu(View view) {
        popup = MenuHelper.createMenu(getContext(), R.menu.menu_album, view);
        popup.setOnMenuItemClickListener(this);
        if (mAlbumDto.isFavorite())
            popup.getMenu().findItem(R.id.menu_delete).setVisible(false);
        popup.show();
    }

    @Override
    public void dismissPopupMenu() {
        if (popup != null) popup.dismiss();
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_upload:
                mPresenter.openUploadScreen();
                break;
            case R.id.menu_edit:
                DialogFragment dialogEditAlbum = AlbumDialog.newInstance(mAlbumDto.getTitle(),
                        mAlbumDto.getDescription(), this);
                dialogEditAlbum.show(mPresenter.getFragManager(), "AlbumDialog");
                break;
            case R.id.menu_delete:
                DialogFragment dialogRemove = MessageDialog.newInstance("Удаление альбома",
                        getContext().getResources().getString(R.string.text_delete), this);
                dialogRemove.show(mPresenter.getFragManager(), "MessageDialog");
                break;
        }
        return true;
    }

    @Override
    public void onClickAdd(String name, String description) {

    }

    @Override
    public void onClickEdit(String name, String description) {
        mPresenter.editAlbum(name, description);
    }

    @Override
    public void onClickOk() {
        mPresenter.deleteAlbum();
    }
}
