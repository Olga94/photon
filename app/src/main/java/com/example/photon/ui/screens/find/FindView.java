package com.example.photon.ui.screens.find;

import android.content.Context;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.View;

import com.example.photon.R;
import com.example.photon.di.DaggerService;
import com.example.photon.mvp.views.AbstractView;
import com.example.photon.mvp.views.IFindView;

import butterknife.BindView;

public class FindView extends AbstractView<FindScreen.FindPresenter> implements IFindView {

    @BindView(R.id.filter_pager)
    ViewPager mFilterPager;
    /*@BindView(R.id.filter_tabs)
    TabLayout mFilterTab;*/

    public FindView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean viewOnBackPressed() {
        return false;
    }

    @Override
    protected void initDagger(Context context) {
        DaggerService.<FindScreen.Component>getDaggerComponent(context).inject(this);
    }

    public ViewPager getFilterPager() {
        return mFilterPager;
    }

    @Override
    public void initView() {
        setupViewPager();
    }

    private void setupViewPager() {
        mFilterPager.setAdapter(new FindAdapter(getContext()));
        //mFilterTab.setupWithViewPager(mFilterPager);
    }
}
