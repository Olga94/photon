package com.example.photon.ui.screens.new_card;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.IntegerRes;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.example.photon.R;
import com.example.photon.data.network.req.PhotoCardReq;
import com.example.photon.data.network.res.PhotoCardRes;
import com.example.photon.data.storage.dto.FiltersDto;
import com.example.photon.data.storage.dto.PhotoCardDto;
import com.example.photon.di.DaggerService;
import com.example.photon.mvp.views.AbstractView;
import com.example.photon.mvp.views.INewCardView;
import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.google.android.flexbox.JustifyContent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;

public class NewCardView extends AbstractView<NewCardScreen.NewCardPresenter> implements INewCardView{

    private NewCardAdapter mAdapter;

    @BindView(R.id.txt_select_album)
    TextView mTxtSelectAlbum;
    @BindView(R.id.list_album)
    RecyclerView mListAlbum;
    @BindView(R.id.container_no_album)
    LinearLayout mContainerNoAlbum;
    @BindView(R.id.autocomplete_tags)
    AutoCompleteTextView mTags;
    @BindView(R.id.et_title_card)
    EditText mEtTitleCard;
    @BindView(R.id.rg_dish)
    RadioGroup mRgDish;
    @BindView(R.id.rg_decor)
    RadioGroup mRgDecor;
    @BindView(R.id.rg_temperature)
    RadioGroup mRgTemperature;
    @BindView(R.id.rg_light)
    RadioGroup mRgLight;
    @BindView(R.id.rg_light_dir)
    RadioGroup mRgLightDir;
    @BindView(R.id.rg_light_count)
    RadioGroup mRgLightCount;
    @BindView(R.id.cb_red)
    AppCompatCheckBox mCbRed;
    @BindView(R.id.cb_orange)
    AppCompatCheckBox mCbOrange;
    @BindView(R.id.cb_yellow)
    AppCompatCheckBox mCbYellow;
    @BindView(R.id.cb_green)
    AppCompatCheckBox mCbGreen;
    @BindView(R.id.cb_light_blue)
    AppCompatCheckBox mCbLightBlue;
    @BindView(R.id.cb_blue)
    AppCompatCheckBox mCbBlue;
    @BindView(R.id.cb_purple)
    AppCompatCheckBox mCbPurple;
    @BindView(R.id.cb_brown)
    AppCompatCheckBox mCbBrown;
    @BindView(R.id.cb_black)
    AppCompatCheckBox mCbBlack;
    @BindView(R.id.cb_white)
    AppCompatCheckBox mCbWhite;
    @BindView(R.id.list_tags)
    RecyclerView mListTags;

    private TagAdapter mTagAdapter;
    private ArrayAdapter<String> mListTagAdapter;

    public NewCardView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean viewOnBackPressed() {
        return false;
    }

    @Override
    protected void initDagger(Context context) {
        DaggerService.<NewCardScreen.Component>getDaggerComponent(context).inject(this);
    }

    @Override
    public void initView() {
        mTxtSelectAlbum.setVisibility(VISIBLE);
        mAdapter = new NewCardAdapter();
        GridLayoutManager layoutManager = new GridLayoutManager(getContext(), 2);
        mListAlbum.setAdapter(mAdapter);
        mListAlbum.setLayoutManager(layoutManager);
    }

    @Override
    public void initTag() {
        mTagAdapter = new TagAdapter();
        FlexboxLayoutManager flexboxLayoutManager = new FlexboxLayoutManager(getContext());
        flexboxLayoutManager.setFlexDirection(FlexDirection.ROW);
        flexboxLayoutManager.setJustifyContent(JustifyContent.FLEX_START);
        mListTags.setAdapter(mTagAdapter);
        mListTags.setLayoutManager(flexboxLayoutManager);

        mListTagAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1);
        mTags.setAdapter(mListTagAdapter);
        mTags.setOnItemClickListener((parent, view, position, id) -> {
            String tag = (String) parent.getItemAtPosition(position);
            addTag(tag);
            mTags.setText("");
        });
    }

    @Override
    public void showAlbums() {
        mListAlbum.setVisibility(VISIBLE);
    }

    public ArrayAdapter<String> getListTagAdapter() {
        return mListTagAdapter;
    }

    private void addTag(String tag) {
        mTagAdapter.addItem(tag);
    }

    @Override
    public PhotoCardReq getCard() {
        List<String> tags = mTagAdapter.getTags();

        FiltersDto filters = new FiltersDto();
        int id = mRgDish.getCheckedRadioButtonId();
        filters.setDish(getDish(id));
        filters.setNuances(getNuances());
        id = mRgDecor.getCheckedRadioButtonId();
        filters.setDecor(getDecor(id));
        id = mRgTemperature.getCheckedRadioButtonId();
        filters.setTemperature(getTemperature(id));
        id = mRgLight.getCheckedRadioButtonId();
        filters.setLight(getLight(id));
        id = mRgLightDir.getCheckedRadioButtonId();
        filters.setLightDirection(getLightDirection(id));
        id = mRgLightCount.getCheckedRadioButtonId();
        filters.setLightSource(getLightCount(id));

        return new PhotoCardReq(mEtTitleCard.getText().toString(),
                tags, filters);
    }

    private String getDish(int id) {
        switch (id) {
            case R.id.rb_meat:
                return "meat";
            case R.id.rb_fish:
                return "fish";
            case R.id.rb_vegetables:
                return "vegetables";
            case R.id.rb_fruit:
                return "fruit";
            case R.id.rb_cheese:
                return "cheese";
            case R.id.rb_dessert:
                return "dessert";
            case R.id.rb_drinks:
                return "drinks";
        }
        return "";
    }

    public String getNuances() {
        String nuances = "";
        nuances += mCbRed.isChecked() ? "red, " : "";
        nuances += mCbOrange.isChecked() ? "orange, " : "";
        nuances += mCbYellow.isChecked() ? "yellow, " : "";
        nuances += mCbGreen.isChecked() ? "green, " : "";
        nuances += mCbLightBlue.isChecked() ? "lightBlue, " : "";
        nuances += mCbBlue.isChecked() ? "blue, " : "";
        nuances += mCbPurple.isChecked() ? "violet, " : "";
        nuances += mCbBrown.isChecked() ? "brown, " : "";
        nuances += mCbBlack.isChecked() ? "black, " : "";
        nuances += mCbWhite.isChecked() ? "white" : "";
        if (nuances.length() != 0 && nuances.charAt(nuances.length() - 1) == ' ')
            nuances = nuances.substring(0, nuances.length() - 2);
        return nuances;
    }

    private String getDecor(int id) {
        switch (id) {
            case R.id.rb_simple:
                return "simple";
            case R.id.rb_holiday:
                return "holiday";
        }
        return "";
    }

    private String getTemperature(int id) {
        switch (id) {
            case R.id.rb_hot:
                return "hot";
            case R.id.rb_middle:
                return "middle";
            case R.id.rb_cold:
                return "cold";
        }
        return "";
    }

    private String getLight(int id) {
        switch (id) {
            case R.id.rb_natural:
                return "natural";
            case R.id.rb_synthetic:
                return "synthetic";
            case R.id.rb_mixed:
                return "mixed";
        }
        return "";
    }

    private String getLightDirection(int id) {
        switch (id) {
            case R.id.rb_direct:
                return "direct";
            case R.id.rb_back_light:
                return "backlight";
            case R.id.rb_side_light:
                return "sideLight";
            case R.id.rb_mixed_light:
                return "mixed";
        }
        return "";
    }

    private String getLightCount(int id) {
        switch (id) {
            case R.id.rb_one_light:
                return "one";
            case R.id.rb_two_light:
                return "two";
            case R.id.rb_three_light:
                return "three";
        }
        return "";
    }

    public NewCardAdapter getAdapter() {
        return mAdapter;
    }

    @OnClick(R.id.btn_ok)
    void onClickOk() {
        mPresenter.onClickOk();
    }

    @OnClick(R.id.btn_cancel)
    void onClickCancel() {
        mPresenter.onClickCancel();
    }

    @OnClick(R.id.clear_tags)
    void onClickClearTag() {
        mTags.setText("");
    }

    @OnClick(R.id.check_tags)
    void onAddTag() {
        if (!mTags.getText().toString().isEmpty()) {
            String tag = mTags.getText().toString();
            addTag(tag);
            mTags.setText("");
        }
    }

    @OnClick(R.id.clear_title)
    void onClickClearTitle() {
        mEtTitleCard.setText("");
    }
}
