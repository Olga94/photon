package com.example.photon.ui.screens.main;

import android.os.Bundle;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;

import com.example.photon.R;
import com.example.photon.data.storage.dto.PhotoCardDto;
import com.example.photon.data.storage.model.Find;
import com.example.photon.di.DaggerService;
import com.example.photon.di.scopes.SplashScope;
import com.example.photon.flow.AbstractScreen;
import com.example.photon.flow.Screen;
import com.example.photon.mvp.models.MainModel;
import com.example.photon.mvp.presenters.AbstractPresenter;
import com.example.photon.mvp.presenters.IMainPresenter;
import com.example.photon.mvp.presenters.RootPresenter;
import com.example.photon.ui.activities.RootActivity;
import com.example.photon.ui.screens.details.DetailsScreen;
import com.example.photon.ui.screens.find.FindScreen;
import com.example.photon.utils.MenuItemHolder;
import com.squareup.picasso.Picasso;

import dagger.Provides;
import flow.Flow;
import mortar.MortarScope;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

@Screen(R.layout.screen_main)
public class MainScreen extends AbstractScreen<RootActivity.RootComponent> {

    private Find mFindModel;

    public MainScreen(Find findModel) {
        mFindModel = findModel;
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof MainScreen && mFindModel.equals(((MainScreen) o).mFindModel);
    }

    @Override
    public int hashCode() {
        return mFindModel.hashCode();
    }

    @Override
    public Object createScreenComponent(RootActivity.RootComponent patentComponent) {
        return DaggerMainScreen_Component.builder()
                .module(new Module())
                .rootComponent(patentComponent)
                .build();
    }

    //region =========================== DI =====================

    @dagger.Module
    public class Module {
        @Provides
        @SplashScope
        MainPresenter provideMainPresenter() {
            return new MainPresenter();
        }

        @Provides
        @SplashScope
        MainModel provideMainModel() {
            return new MainModel();
        }
    }

    @dagger.Component(dependencies = RootActivity.RootComponent.class, modules = MainScreen.Module.class)
    @SplashScope
    public interface Component {
        void inject(MainPresenter presenter);

        void inject(MainView view);

        void inject(MainAdapter adapter);

        Picasso getPicasso();

        RootPresenter getRootPresenter();
    }
    //endregion

    public class MainPresenter extends AbstractPresenter<MainView, MainModel> implements IMainPresenter {

        @Override
        protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);
            if (getRootView() != null) getRootView().setVisibleBottomNavigation(true);
            if (getView() != null) getView().initView();

            if (mFindModel.getSearch() != null) {
                mCompositeSubscription.add(subscribeOnSearchPhotoCardsRealmObs());
                if (getRootView() != null) getView().showSnackBar(R.color.color_accent, "Применет поиск");
            } else if (mFindModel.getFilter() != null) {
                mCompositeSubscription.add(subscribeOnFilterPhotoCardsRealmObs());
                if (getRootView() != null) getView().showSnackBar(R.color.black, "Применен фильтр");
            } else
                mCompositeSubscription.add(subscribeOnAllPhotoCardsRealmObs());
        }

        @Override
        protected void onSave(Bundle outState) {
            getView().dismissPopupMenu();
            super.onSave(outState);
        }

        @Override
        protected void initActionBar() {
            mRootPresenter.newActionBarBuilder()
                    .setTitle("Фотон")
                    .addAction(new MenuItemHolder("Поиск",
                            mFindModel.getSearch() != null ? R.drawable.ic_search_blue_24dp
                                    : mFindModel.getFilter() != null ? R.drawable.ic_style_blue_24dp
                                    : R.drawable.ic_search_black_24dp, item -> {
                        Flow.get(getView()).set(new FindScreen());
                        return true;
                    }))
                    .addAction(new MenuItemHolder("Меню", R.layout.icon_settings, v -> {
                        getView().showPopupMenu(v);
                    }))
                    .setVisible(true)
                    .build();
        }

        @Override
        protected void initDagger(MortarScope scope) {
            ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
        }

        @Override
        protected void onExitScope() {
            getView().dismissSnackBar();
            super.onExitScope();

        }

        @Override
        public void onClickPhotoCard(PhotoCardDto photoCard) {
            getView().dismissSnackBar();
            mModel.addView(photoCard.getId()).subscribe();
            Flow.get(getView()).set(new DetailsScreen(photoCard));
        }

        @Override
        public boolean isAuth() {
            return mModel.isSignInUser();
        }

        @Override
        public FragmentManager getFragManager() {
            if (getRootView() != null)
                return getRootView().getFM();
            return null;
        }

        @Override
        public void signIn(String email, String pass) {
            mModel.signIn(email, pass).subscribe();
        }

        @Override
        public void logout() {
            mModel.logout();
        }

        @Override
        public void signUp(String name, String login, String email, String pass) {
            mModel.signUp(name, login, email, pass).subscribe();
        }

        @Override
        public void clearSearch() {
            getView().dismissSnackBar();
            mFindModel = new Find();
            initActionBar();
            getView().getAdapter().clearList();
            mCompositeSubscription.add(subscribeOnAllPhotoCardsRealmObs());
        }

        private Subscription subscribeOnAllPhotoCardsRealmObs() {
            return mModel.getAllPhotoCards()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new PhotoCardSubscriber());

        }

        private Subscription subscribeOnSearchPhotoCardsRealmObs() {
            return mModel.getSearchPhotoCard(mFindModel.getSearch())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new PhotoCardSubscriber());

        }

        private Subscription subscribeOnFilterPhotoCardsRealmObs() {
            return mModel.getFilterPhotoCard(mFindModel.getFilter())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new PhotoCardSubscriber());

        }

        private class PhotoCardSubscriber extends Subscriber<PhotoCardDto> {
            MainAdapter mAdapter = getView().getAdapter();

            @Override
            public void onCompleted() {
            }

            @Override
            public void onError(Throwable e) {
                if (getRootView() != null) {
                    getRootView().showError(e);
                }
            }

            @Override
            public void onNext(PhotoCardDto photoCard) {
                mAdapter.addItem(photoCard);
            }
        }
    }
}
