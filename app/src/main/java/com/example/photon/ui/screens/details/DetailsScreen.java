package com.example.photon.ui.screens.details;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.FragmentManager;

import com.example.photon.R;
import com.example.photon.data.storage.dto.OwnerDto;
import com.example.photon.data.storage.dto.PhotoCardDto;
import com.example.photon.di.DaggerService;
import com.example.photon.di.scopes.DetailsScope;
import com.example.photon.flow.AbstractScreen;
import com.example.photon.flow.Screen;
import com.example.photon.mvp.models.DetailsModel;
import com.example.photon.mvp.presenters.AbstractPresenter;
import com.example.photon.mvp.presenters.IDetailsPresenter;
import com.example.photon.ui.activities.RootActivity;
import com.example.photon.ui.screens.author.AuthorScreen;
import com.example.photon.ui.screens.main.MainScreen;
import com.example.photon.ui.screens.profile.ProfileScreen;
import com.example.photon.utils.MenuItemHolder;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.io.FileOutputStream;

import dagger.Provides;
import flow.Flow;
import mortar.MortarScope;

@Screen(R.layout.screen_details)
public class DetailsScreen extends AbstractScreen<RootActivity.RootComponent> {

    private PhotoCardDto mPhotoCard;

    public DetailsScreen(PhotoCardDto photoCard) {
        mPhotoCard = photoCard;
    }

    @Override
    public Object createScreenComponent(RootActivity.RootComponent patentComponent) {
        return DaggerDetailsScreen_Component.builder()
                .rootComponent(patentComponent)
                .module(new Module())
                .build();
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof DetailsScreen && mPhotoCard.equals(((DetailsScreen) o).mPhotoCard);
    }

    @Override
    public int hashCode() {
        return mPhotoCard.hashCode();
    }

    //region =========================== DI =====================

    @dagger.Module
    public class Module {
        @Provides
        @DetailsScope
        DetailsPresenter provideDetailsPresenter() {
            return new DetailsPresenter();
        }

        @Provides
        @DetailsScope
        DetailsModel provideDetailsModel() {
            return new DetailsModel();
        }
    }

    @dagger.Component(dependencies = RootActivity.RootComponent.class, modules = Module.class)
    @DetailsScope
    public interface Component {

        void inject(DetailsPresenter presenter);

        void inject(DetailsView view);
    }

    //endregion

    public class DetailsPresenter extends AbstractPresenter<DetailsView, DetailsModel> implements IDetailsPresenter {

        private OwnerDto mOwnerDto;

        @Override
        protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);

            mModel.getUser(mPhotoCard.getOwner()).subscribe(ownerDto -> {
                mOwnerDto = ownerDto;
                getView().initOwner(ownerDto);
            });
            getView().initView(mPhotoCard);
            getView().initIsFavorite(mModel.getFavorite(mPhotoCard.getId()));
        }

        @Override
        protected void onSave(Bundle outState) {
            getView().dismissPopupMenu();
            super.onSave(outState);
        }

        @Override
        public void dropView(DetailsView view) {
            super.dropView(view);
        }

        @Override
        protected void initActionBar() {
            mRootPresenter.newActionBarBuilder()
                    .setTitle("Фотокарточка")
                    .setBackArrow(true)
                    .addAction(new MenuItemHolder("Меню", R.layout.icon_settings, v -> {
                        getView().showPopupMenu(v);
                    }))
                    .build();
        }

        @Override
        protected void initDagger(MortarScope scope) {
            ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
        }

        @Override
        public void onClickFavorite() {
            if (!mModel.isAuth()) {
                getRootView().showMessage("Необходима авторизация");
                return;
            }
            if (mModel.getFavorite(mPhotoCard.getId()))
                mModel.deleteFromFavorite(mPhotoCard.getId()).subscribe(responseBody -> {
                    getView().initIsFavorite(false);
                });
            else
                mModel.addToFavorite(mPhotoCard).subscribe(addResponse -> {
                    if (addResponse.isSuccess()) getView().initIsFavorite(true);
                });
        }

        @Override
        public void onClickAuthor() {
            Flow.get(getView()).set(new AuthorScreen(mOwnerDto));
        }

        @Override
        public void onClickShare() {
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            String shareBody = mPhotoCard.getPhoto();
            String shareSub = "Фото карточки";
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, shareSub);
            shareIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
            mRootPresenter.startActivity(Intent.createChooser(shareIntent, "Поделиться карточкой"));
        }

        @Override
        public FragmentManager getFragManager() {
            if (getRootView() != null)
                return getRootView().getFM();
            return null;
        }

        public Target target = new Target() {
            @Override
            public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
                new Thread(() -> {

                    File file = new File(Environment.getExternalStorageDirectory().getPath()
                            + "/" + Environment.DIRECTORY_DCIM + "/" + mPhotoCard.getTitle() + ".jpg");
                    try {
                        file.createNewFile();
                        FileOutputStream ostream = new FileOutputStream(file);
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, ostream);
                        ostream.close();
                        getRootView().showMessage("Фото сохранено в галерею");
                    } catch (Exception e) {
                        getRootView().showError(e);
                    }
                }).start();
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
            }
        };
    }
}
