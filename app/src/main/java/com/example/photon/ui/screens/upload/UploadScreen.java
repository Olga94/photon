package com.example.photon.ui.screens.upload;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import com.example.photon.R;
import com.example.photon.data.rx.SimpleDisposableSubscriber;
import com.example.photon.data.storage.dto.ActivityPermissionsResultDto;
import com.example.photon.di.DaggerService;
import com.example.photon.di.scopes.UploadScope;
import com.example.photon.flow.AbstractScreen;
import com.example.photon.flow.Screen;
import com.example.photon.mvp.models.UploadModel;
import com.example.photon.mvp.presenters.AbstractPresenter;
import com.example.photon.mvp.presenters.IUploadPresenter;
import com.example.photon.mvp.presenters.RootPresenter;
import com.example.photon.ui.activities.RootActivity;
import com.example.photon.ui.screens.new_card.NewCardScreen;
import com.example.photon.utils.App;
import com.example.photon.utils.ConstantManager;
import com.example.photon.utils.LocalStorageAvatar;
import com.squareup.picasso.Picasso;

import java.io.File;

import dagger.Provides;
import flow.Flow;
import id.zelory.compressor.Compressor;
import mortar.MortarScope;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.Observable;

@Screen(R.layout.screen_upload)
public class UploadScreen extends AbstractScreen<RootActivity.RootComponent>{

    private String mId;

    public UploadScreen(String id) {
        mId = id;
    }

    @Override
    public Object createScreenComponent(RootActivity.RootComponent patentComponent) {
        return DaggerUploadScreen_Component.builder()
                .module(new Module())
                .rootComponent(patentComponent)
                .build();
    }

    //region =========================== DI =====================

    @dagger.Module
    public class Module {

        @Provides
        @UploadScope
        public UploadPresenter provideUploadPresenter() {
            return new UploadPresenter(mId);
        }

        @Provides
        @UploadScope
        public UploadModel provideUploadModel() {
            return new UploadModel();
        }
    }

    @dagger.Component(dependencies = RootActivity.RootComponent.class, modules = Module.class)
    @UploadScope
    public interface Component {
        void inject(UploadPresenter presenter);
        void inject(UploadView view);
        RootPresenter getRootPresenter();
        Picasso getPicasso();
    }
    //endregion

    public class UploadPresenter extends AbstractPresenter<UploadView, UploadModel> implements IUploadPresenter{

        private String id;
        private boolean mIsActivityResultWaiting;
        private boolean mIsActivityPermissionsResultWaiting;

        public UploadPresenter(String id) {
            this.id = id;
        }

        @Override
        protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);
            if (mIsActivityResultWaiting) {
                mRootPresenter.getActivityResultPublishUrlSubject()
                        .subscribe(new SimpleDisposableSubscriber<String>() {
                            @Override
                            public void onNext(String uri) {
                                mIsActivityResultWaiting = false;
                                Flow.get(getView()).set(new NewCardScreen(id, uri));
                            }
                        });
            }

            if (mIsActivityPermissionsResultWaiting) {
                getPermissionsResultPublishSubject()
                        .subscribe(new SimpleDisposableSubscriber<ActivityPermissionsResultDto>() {
                            @Override
                            public void onNext(ActivityPermissionsResultDto result) {
                                mIsActivityPermissionsResultWaiting = false;
                                if (!result.isAllGranted()) {
                                    if (getRootView() != null)
                                        getRootView().showMessage("На операцию нет разрешений");
                                } else {
                                    switch (result.getRequestCode()) {
                                        case ConstantManager.REQUEST_PERMISSION_FOR_GALLERY_PHOTOCARD_CODE:
                                            takePhotoFromGallery();
                                            break;
                                    }
                                }
                            }
                        });
            }
        }

        @Override
        protected void initActionBar() {
            mRootPresenter.newActionBarBuilder()
                    .setVisible(false)
                    .build();
        }

        @Override
        protected void initDagger(MortarScope scope) {
            ((Component)scope.getService(DaggerService.SERVICE_NAME)).inject(this);
        }

        private Observable<ActivityPermissionsResultDto> getPermissionsResultPublishSubject() {
            return mRootPresenter.getActivityPermissionsResultSubject()
                    .filter(res -> res.getRequestCode() == ConstantManager.REQUEST_PERMISSION_FOR_GALLERY_PHOTOCARD_CODE);
        }

        @Override
        public void openGallery() {
            String[] permissions = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE};
            if (mRootPresenter.checkPermissions(permissions)) {
                takePhotoFromGallery();
            } else {
                if (mRootPresenter.requestPermissions(permissions, ConstantManager.REQUEST_PERMISSION_FOR_GALLERY_PHOTOCARD_CODE)) {
                    mIsActivityPermissionsResultWaiting = true;
                } else if (getRootView() != null) {
                    getRootView().showMessage("Нет разрешений на выполнение данной операции");
                }
            }
        }

        private void takePhotoFromGallery() {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_OPEN_DOCUMENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            mRootPresenter.startActivityForResult(intent, ConstantManager.REQUEST_PROFILE_PHOTO_GALLERY);
            mIsActivityResultWaiting = true;
        }
    }
}
