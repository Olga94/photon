package com.example.photon.ui.screens.author;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.photon.R;
import com.example.photon.data.storage.dto.AlbumDto;
import com.example.photon.di.DaggerService;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AuthorAdapter extends RecyclerView.Adapter<AuthorAdapter.AuthorViewHolder> {

    @Inject
    Picasso mPicasso;
    @Inject
    AuthorScreen.AuthorPresenter mPresenter;

    private List<AlbumDto> mAlbumCards = new ArrayList<>();

    public AuthorAdapter(List<AlbumDto> albumCards) {
        mAlbumCards = albumCards;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        DaggerService.<AuthorScreen.Component>getDaggerComponent(recyclerView.getContext()).inject(this);
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public AuthorViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_photo_card, parent, false);
        return new AuthorViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AuthorViewHolder holder, int position) {
        AlbumDto album = mAlbumCards.get(position);

        holder.mContainerTitle.setVisibility(View.VISIBLE);
        holder.mTxtTitle.setText(album.getTitle());
        holder.mCountCard.setText(String.valueOf(album.getPhotocards().size()));
        holder.mTxtFavorites.setText(String.valueOf(album.getFavorits()));
        holder.mTxtViews.setText(String.valueOf(album.getViews()));
        if (album.getPhotocards().size() != 0 && album.getPhotocards().get(0).getPhoto() != null)
            mPicasso
                    .load(album.getPhotocards().get(0).getPhoto())
                    .error(R.drawable.no_image)
                    .into(holder.mImageCard);
        else
            mPicasso
                    .load(R.drawable.no_image)
                    .into(holder.mImageCard);
    }

    @Override
    public int getItemCount() {
        return mAlbumCards.size();
    }

    public void addItem(AlbumDto albumDto) {
        mAlbumCards.add(albumDto);
        notifyDataSetChanged();
    }

    public class AuthorViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.container_album)
        FrameLayout mContainerAlbum;
        @BindView(R.id.image_card)
        ImageView mImageCard;
        @BindView(R.id.txt_favorites)
        TextView mTxtFavorites;
        @BindView(R.id.txt_views)
        TextView mTxtViews;
        @BindView(R.id.container_title)
        LinearLayout mContainerTitle;
        @BindView(R.id.txt_title)
        TextView mTxtTitle;
        @BindView(R.id.count_card)
        TextView mCountCard;

        public AuthorViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            mContainerAlbum.setOnClickListener(v -> mPresenter.openAlbum(mAlbumCards.get(getAdapterPosition())));
        }
    }
}