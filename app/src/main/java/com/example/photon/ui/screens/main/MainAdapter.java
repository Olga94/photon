package com.example.photon.ui.screens.main;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.photon.R;
import com.example.photon.data.storage.dto.PhotoCardDto;
import com.example.photon.di.DaggerService;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainAdapter extends RecyclerView.Adapter<MainAdapter.MainHolder> {

    @Inject
    Picasso mPicasso;
    @Inject
    MainScreen.MainPresenter mPresenter;

    private List<PhotoCardDto> mPhotoCards = new ArrayList<>();

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        DaggerService.<MainScreen.Component>getDaggerComponent(recyclerView.getContext()).inject(this);
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public MainHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.item_photo_card, parent, false);
        return new MainHolder(view);
    }

    @Override
    public void onBindViewHolder(MainHolder holder, int position) {
        PhotoCardDto photoCard = mPhotoCards.get(position);
        holder.mTxtFavorites.setText(String.valueOf(photoCard.getFavorits()));
        holder.mTxtViews.setText(String.valueOf(photoCard.getViews()));
        mPicasso.load(photoCard.getPhoto())
                .error(R.drawable.no_image)
                .into(holder.mImageCard);
    }

    @Override
    public int getItemCount() {
        return mPhotoCards.size();
    }

    public void clearList(){
        mPhotoCards = new ArrayList<>();
        notifyDataSetChanged();
    }

    public void addItem(PhotoCardDto photoCards) {
        mPhotoCards.add(photoCards);
        notifyDataSetChanged();
    }

    public class MainHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.image_card)
        ImageView mImageCard;
        @BindView(R.id.txt_favorites)
        TextView mTxtFavorites;
        @BindView(R.id.txt_views)
        TextView mTxtViews;

        public MainHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            mImageCard.setOnClickListener(v -> mPresenter.onClickPhotoCard(mPhotoCards.get(getAdapterPosition())));
        }
    }
}
