package com.example.photon.ui.screens.find.search;

import android.os.Bundle;

import com.example.photon.R;
import com.example.photon.data.storage.model.Find;
import com.example.photon.data.storage.model.Search;
import com.example.photon.data.storage.model.Tag;
import com.example.photon.di.DaggerService;
import com.example.photon.di.scopes.SearchScope;
import com.example.photon.flow.AbstractScreen;
import com.example.photon.flow.Screen;
import com.example.photon.mvp.models.SearchModel;
import com.example.photon.mvp.presenters.AbstractPresenter;
import com.example.photon.mvp.presenters.ISearchPresenter;
import com.example.photon.mvp.presenters.RootPresenter;
import com.example.photon.ui.screens.find.FindScreen;
import com.example.photon.ui.screens.main.MainScreen;

import java.util.List;

import dagger.Provides;
import flow.Flow;
import mortar.MortarScope;

@Screen(R.layout.screen_search)
public class SearchScreen extends AbstractScreen<FindScreen.Component> {

    @Override
    public Object createScreenComponent(FindScreen.Component patentComponent) {
        return DaggerSearchScreen_Component.builder()
                .component(patentComponent)
                .module(new Module())
                .build();
    }

    //region =========================== DI =====================

    @dagger.Module
    public class Module {
        @Provides
        @SearchScope
        SearchPresenter provideSearchPresenter() {
            return new SearchPresenter();
        }

        @Provides
        @SearchScope
        SearchModel provideSearchModel() {
            return new SearchModel();
        }
    }

    @dagger.Component(dependencies = FindScreen.Component.class, modules = Module.class)
    @SearchScope
    public interface Component {
        void inject(SearchPresenter presenter);
        void inject(SearchView view);
        RootPresenter getRootPresenter();
    }
    //endregion

    public class SearchPresenter extends AbstractPresenter<SearchView, SearchModel> implements ISearchPresenter{

        @Override
        protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);
            getView().initView();
            mModel.getTags().subscribe(tagDto -> getView().getTagAdapter().addItem(new Tag(tagDto.getTag())));
            mModel.getHistory().subscribe(s -> getView().getHistoryAdapter().add(s));
        }

        @Override
        protected void initActionBar() {

        }

        @Override
        protected void initDagger(MortarScope scope) {
            ((Component)scope.getService(DaggerService.SERVICE_NAME)).inject(this);
        }

        @Override
        public void onBack() {
            Flow.get(getView()).goBack();
        }

        @Override
        public void onСlickSearch(String history) {
            mModel.saveHistory(history);
            Search search = new Search();
            search.setKeyword(history);
            List<Tag> tags = getView().getTagAdapter().getTags();
            for (int i = 0; i < tags.size(); i++) {
                if (tags.get(i).isCheck())
                    search.addTag(tags.get(i).getTag());
            }

            Flow.get(getView()).set(new MainScreen(new Find(search)));
        }
    }
}
