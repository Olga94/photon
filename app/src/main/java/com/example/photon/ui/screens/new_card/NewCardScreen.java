package com.example.photon.ui.screens.new_card;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;

import com.example.photon.R;
import com.example.photon.data.network.req.PhotoCardReq;
import com.example.photon.data.storage.dto.AlbumDto;
import com.example.photon.data.storage.realm.AlbumRealm;
import com.example.photon.data.storage.realm.PhotoCardRealm;
import com.example.photon.di.DaggerService;
import com.example.photon.di.scopes.NewCardScope;
import com.example.photon.flow.AbstractScreen;
import com.example.photon.flow.Screen;
import com.example.photon.mvp.models.NewCardModel;
import com.example.photon.mvp.presenters.AbstractPresenter;
import com.example.photon.mvp.presenters.INewCardPresenter;
import com.example.photon.ui.activities.RootActivity;
import com.example.photon.ui.screens.upload.UploadScreen;
import com.example.photon.utils.App;
import com.example.photon.utils.LocalStorageAvatar;

import java.io.File;

import dagger.Provides;
import flow.Direction;
import flow.Flow;
import flow.History;
import flow.TreeKey;
import id.zelory.compressor.Compressor;
import mortar.MortarScope;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.Observable;
import rx.Subscriber;

@Screen(R.layout.screen_new_card)
public class NewCardScreen extends AbstractScreen<RootActivity.RootComponent> {

    private String mId;
    private String mImageUri;

    public NewCardScreen(String id, String uri) {
        mId = id;
        mImageUri = uri;
    }

    @Override
    public Object createScreenComponent(RootActivity.RootComponent patentComponent) {
        return DaggerNewCardScreen_Component.builder()
                .module(new Module())
                .rootComponent(patentComponent)
                .build();
    }

    //region =========================== DI =====================

    @dagger.Module
    public class Module {

        @Provides
        @NewCardScope
        public NewCardPresenter provideNewCardPresenter() {
            return new NewCardPresenter(mId, mImageUri);
        }

        @Provides
        @NewCardScope
        public NewCardModel provideNewCardModel() {
            return new NewCardModel();
        }
    }

    @dagger.Component(dependencies = RootActivity.RootComponent.class, modules = Module.class)
    @NewCardScope
    public interface Component {
        void inject(NewCardPresenter presenter);

        void inject(NewCardView view);

        void inject(NewCardAdapter adapter);
    }
    //endregion

    public class NewCardPresenter extends AbstractPresenter<NewCardView, NewCardModel> implements INewCardPresenter {

        private String mIdAlbum;
        private String mImage;

        public NewCardPresenter(String id, String uri) {
            mIdAlbum = id;
            mImage = uri;
        }

        @Override
        protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);
            if (mIdAlbum == null) {
                getView().initView();
                Observable.from(mModel.getAlbums()).subscribe(new AlbumSubscriber());
            }
            getView().initTag();
            mModel.getTags().subscribe(tagDto -> getView().getListTagAdapter().add(tagDto.getTag()));
        }

        @Override
        protected void initActionBar() {
            mRootPresenter.newActionBarBuilder()
                    .setTitle(getView().getResources().getString(R.string.app_name))
                    .build();
        }

        @Override
        protected void initDagger(MortarScope scope) {
            ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
        }

        @Override
        public void onClickOk() {
            PhotoCardReq photoCardReq = getView().getCard();
            photoCardReq.setAlbum(mIdAlbum);
            mModel.savePhotoCard(photoCardReq, mImage);
            Flow.get(getView()).setHistory(History.single(new UploadScreen(null)), Direction.BACKWARD);
        }

        @Override
        public void onClickCancel() {
            Flow.get(getView()).goBack();
        }

        @Override
        public void onClickCard(String id) {
            mIdAlbum = id;
        }

        private class AlbumSubscriber extends Subscriber<AlbumRealm> {

            NewCardAdapter mAdapter = getView().getAdapter();

            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                if (getRootView() != null) {
                    getRootView().showError(e);
                }
            }

            @Override
            public void onNext(AlbumRealm albumRealm) {
                mAdapter.addItem(new AlbumDto(albumRealm));
                getView().showAlbums();
            }
        }
    }
}
