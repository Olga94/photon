package com.example.photon.ui.screens.find.filter;

import com.example.photon.R;
import com.example.photon.data.storage.model.Filter;
import com.example.photon.data.storage.model.Find;
import com.example.photon.di.DaggerService;
import com.example.photon.di.scopes.FilterScope;
import com.example.photon.flow.AbstractScreen;
import com.example.photon.flow.Screen;
import com.example.photon.mvp.models.FilterModel;
import com.example.photon.mvp.presenters.AbstractPresenter;
import com.example.photon.mvp.presenters.IFilterPresenter;
import com.example.photon.mvp.presenters.RootPresenter;
import com.example.photon.ui.screens.find.FindScreen;
import com.example.photon.ui.screens.main.MainScreen;

import dagger.Provides;
import flow.Flow;
import mortar.MortarScope;

@Screen(R.layout.screen_filter)
public class FilterScreen extends AbstractScreen<FindScreen.Component>{

    @Override
    public Object createScreenComponent(FindScreen.Component patentComponent) {
        return DaggerFilterScreen_Component.builder()
                .component(patentComponent)
                .module(new Module())
                .build();
    }

    //region =========================== DI =====================

    @dagger.Module
    public class Module {
        @Provides
        @FilterScope
        FilterPresenter provideFilterPresenter() {
            return new FilterPresenter();
        }

        @Provides
        @FilterScope
        FilterModel provideFilterModel() {
            return new FilterModel();
        }
    }

    @dagger.Component(dependencies = FindScreen.Component.class, modules = Module.class)
    @FilterScope
    public interface Component {
        void inject(FilterPresenter presenter);
        void inject(FilterView view);
        RootPresenter getRootPresenter();
    }
    //endregion

    public class FilterPresenter extends AbstractPresenter<FilterView, FilterModel> implements IFilterPresenter {

        @Override
        protected void initActionBar() {

        }

        @Override
        protected void initDagger(MortarScope scope) {
            ((Component)scope.getService(DaggerService.SERVICE_NAME)).inject(this);
        }

        @Override
        public void filter(Filter filter) {
            Flow.get(getView()).set(new MainScreen(new Find(filter)));
        }
    }
}
