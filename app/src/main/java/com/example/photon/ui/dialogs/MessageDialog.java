package com.example.photon.ui.dialogs;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.photon.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MessageDialog extends DialogFragment {

    @BindView(R.id.title_dialog)
    TextView mTitleDialog;
    @BindView(R.id.message_dialog)
    TextView mMessage;
    @BindView(R.id.btn_ok)
    Button mBtnOk;
    @BindView(R.id.btn_cancel)
    Button mBtnCancel;

    private static ClickOnOkBtn sClickOnOkBtn;

    public static MessageDialog newInstance(String title, String message, ClickOnOkBtn clickOnOkBtn) {

        Bundle args = new Bundle();
        args.putString("title", title);
        args.putString("message", message);
        sClickOnOkBtn = clickOnOkBtn;
        MessageDialog fragment = new MessageDialog();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.DialogTheme);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_message, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        ButterKnife.bind(this, view);

        String title = getArguments().getString("title");
        String message = getArguments().getString("message");

        mTitleDialog.setText(title);
        mMessage.setText(message);
        mBtnOk.setOnClickListener(v -> {
            getDialog().dismiss();
            sClickOnOkBtn.onClickOk();
        });

        mBtnCancel.setOnClickListener(v -> getDialog().dismiss());
    }

    public interface ClickOnOkBtn {
        void onClickOk();
    }
}