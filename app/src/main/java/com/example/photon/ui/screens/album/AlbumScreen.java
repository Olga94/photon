package com.example.photon.ui.screens.album;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;

import com.example.photon.R;
import com.example.photon.data.storage.dto.AlbumDto;
import com.example.photon.data.storage.dto.PhotoCardDto;
import com.example.photon.di.DaggerService;
import com.example.photon.di.scopes.AlbumScope;
import com.example.photon.flow.AbstractScreen;
import com.example.photon.flow.Screen;
import com.example.photon.mvp.models.AlbumModel;
import com.example.photon.mvp.presenters.AbstractPresenter;
import com.example.photon.mvp.presenters.IAlbumPresenter;
import com.example.photon.mvp.presenters.RootPresenter;
import com.example.photon.ui.activities.RootActivity;
import com.example.photon.ui.screens.details.DetailsScreen;
import com.example.photon.ui.screens.profile.ProfileScreen;
import com.example.photon.ui.screens.upload.UploadScreen;
import com.example.photon.utils.MenuItemHolder;
import com.squareup.picasso.Picasso;

import dagger.Provides;
import flow.Direction;
import flow.Flow;
import flow.History;
import flow.TreeKey;
import mortar.MortarScope;

@Screen(R.layout.screen_album)
public class AlbumScreen extends AbstractScreen<RootActivity.RootComponent> {

    private AlbumDto mAlbum;

    public AlbumScreen(AlbumDto album) {
        mAlbum = album;
    }

    @Override
    public Object createScreenComponent(RootActivity.RootComponent patentComponent) {
        return DaggerAlbumScreen_Component.builder()
                .module(new Module())
                .rootComponent(patentComponent)
                .build();
    }

    //region =========================== DI =====================

    @dagger.Module
    public class Module {

        @Provides
        @AlbumScope
        public AlbumPresenter provideAlbumPresenter() {
            return new AlbumPresenter();
        }

        @Provides
        @AlbumScope
        public AlbumModel provideModel() {
            return new AlbumModel();
        }
    }

    @dagger.Component(dependencies = RootActivity.RootComponent.class, modules = Module.class)
    @AlbumScope
    public interface Component {
        void inject(AlbumPresenter presenter);

        void inject(AlbumView view);

        void inject(AlbumAdapter adapter);

        RootPresenter getRootPresenter();

        Picasso getPicasso();

    }
    //endregion

    public class AlbumPresenter extends AbstractPresenter<AlbumView, AlbumModel> implements IAlbumPresenter {

        @Override
        protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);
            getView().initView(mAlbum);
        }

        @Override
        protected void onSave(Bundle outState) {
            getView().dismissPopupMenu();
            super.onSave(outState);
        }

        @Override
        protected void initActionBar() {

            if (mModel.isUser(mAlbum.getOwner())) {
                mRootPresenter.newActionBarBuilder()
                        .setTitle("Альбом")
                        .setBackArrow(true)
                        .addAction(new MenuItemHolder("Меню", R.layout.icon_settings, v -> {
                            getView().showPopupMenu(v);
                        }))
                        .build();
            } else  {
                mRootPresenter.newActionBarBuilder()
                        .setTitle("Альбом")
                        .setBackArrow(true)
                        .build();
            }
        }

        @Override
        protected void initDagger(MortarScope scope) {
            ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
        }

        @Override
        public FragmentManager getFragManager() {
            if (getRootView() != null)
                return getRootView().getFM();
            return null;
        }

        @Override
        public void openUploadScreen() {
            Flow.get(getView()).setHistory(History.single(new UploadScreen(mAlbum.getId())), Direction.FORWARD);
        }

        @Override
        public void deleteAlbum() {
            mModel.deleteAlbum(mAlbum.getId())
                    .subscribe(responseBody -> Flow.get(getView()).goBack());
        }

        @Override
        public void editAlbum(String name, String description) {
            mModel.updateAlbum(mAlbum.getId(), name, description)
                    .subscribe(createResponse -> {
                        AlbumDto albumDto = new AlbumDto(mModel.getAlbum(createResponse.getId()));
                        getView().initView(albumDto);
                    });
        }

        @Override
        public void onClickCard(PhotoCardDto photoCard) {
            Flow.get(getView()).set(new DetailsScreen(photoCard));
        }
    }
}
