package com.example.photon.ui.screens.details;

import android.content.Context;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.photon.R;
import com.example.photon.data.storage.dto.OwnerDto;
import com.example.photon.data.storage.dto.PhotoCardDto;
import com.example.photon.di.DaggerService;
import com.example.photon.mvp.views.AbstractView;
import com.example.photon.mvp.views.IDetailsView;
import com.example.photon.ui.dialogs.MessageDialog;
import com.example.photon.ui.screens.new_card.TagAdapter;
import com.example.photon.utils.CircularTransformation;
import com.example.photon.utils.ConstantManager;
import com.example.photon.utils.MenuHelper;
import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.google.android.flexbox.JustifyContent;
import com.squareup.picasso.Picasso;
import com.transitionseverywhere.ChangeBounds;
import com.transitionseverywhere.ChangeImageTransform;
import com.transitionseverywhere.Transition;
import com.transitionseverywhere.TransitionManager;
import com.transitionseverywhere.TransitionSet;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.OnClick;

public class DetailsView extends AbstractView<DetailsScreen.DetailsPresenter> implements IDetailsView,
        PopupMenu.OnMenuItemClickListener, View.OnTouchListener {

    @Inject
    Picasso mPicasso;

    @BindView(R.id.image_photo_card)
    ImageView mImagePhotoCard;
    @BindView(R.id.txt_title)
    TextView mTxtTitle;
    @BindView(R.id.image_author)
    ImageView mImageAuthor;
    @BindView(R.id.txt_name)
    TextView mTxtNameAuthor;
    @BindView(R.id.txt_count_album)
    TextView mTxtCountAlbum;
    @BindView(R.id.txt_count_card)
    TextView mTxtCountCard;
    @BindView(R.id.list_tags)
    RecyclerView mTags;
    @BindView(R.id.container_image)
    FrameLayout mContainerImage;
    @BindView(R.id.main_container)
    LinearLayout mContainer;
    @BindView(R.id.img_favorite)
    ImageView mImgFavorite;

    private String url;
    private boolean isStart;
    private boolean isFavorite;
    private DisplayMetrics mMetrics;
    private PopupMenu popup;

    public DetailsView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean viewOnBackPressed() {
        return false;
    }

    @Override
    protected void initDagger(Context context) {
        DaggerService.<DetailsScreen.Component>getDaggerComponent(context).inject(this);
    }

    @Override
    public void initView(PhotoCardDto photoCard) {
        mMetrics = getContext().getResources().getDisplayMetrics();
        mPicasso
                .load(photoCard.getPhoto())
                .into(mImagePhotoCard);
        url = photoCard.getPhoto();

        mTxtTitle.setText(photoCard.getTitle());

        TagAdapter tagAdapter = new TagAdapter(photoCard.getTags());
        FlexboxLayoutManager layoutManager = new FlexboxLayoutManager(getContext());
        layoutManager.setFlexDirection(FlexDirection.ROW);
        layoutManager.setJustifyContent(JustifyContent.FLEX_START);
        mTags.setAdapter(tagAdapter);
        mTags.setLayoutManager(layoutManager);
        mContainerImage.setOnTouchListener(this);
    }

    @Override
    public void initIsFavorite(boolean isFavorite) {
        if (isFavorite) mImgFavorite.setVisibility(VISIBLE);
        else mImgFavorite.setVisibility(GONE);
        this.isFavorite = isFavorite;
    }

    @Override
    public void initOwner(OwnerDto owner) {
        mPicasso
                .load(owner.getAvatar())
                .transform(new CircularTransformation())
                .resize(140, 140)
                .centerCrop()
                .into(mImageAuthor);
        mTxtNameAuthor.setText(owner.getName());
        mTxtCountAlbum.setText(String.valueOf(owner.getAlbumCount()));
        mTxtCountCard.setText(String.valueOf(owner.getPhotocardCount()));
    }

    private void animateImage(boolean isDown) {
        TransitionSet set = new TransitionSet();
        set.addTransition(new ChangeBounds())
                .addTransition(new ChangeImageTransform())
                .setDuration(isDown ? 300 : 1000)
                .setInterpolator(new FastOutSlowInInterpolator());

        if (isDown) {
            set.addListener(new Transition.TransitionListenerAdapter(){
                @Override
                public void onTransitionStart(Transition transition) {
                    super.onTransitionStart(transition);
                    isStart = true;
                }

                @Override
                public void onTransitionEnd(Transition transition) {
                    super.onTransitionEnd(transition);
                    animateImage(false);
                    isStart = false;
                }
            });
        }
        TransitionManager.beginDelayedTransition(mContainer, set);
        ViewGroup.LayoutParams params = mContainerImage.getLayoutParams();
        params.height = isDown ? (int) (mContainerImage.getMeasuredHeight() * 1.3) :
                (int) ((280 * mMetrics.density) + 0.5);
        mContainerImage.setLayoutParams(params);

        mImagePhotoCard.setScaleType(isDown ? ImageView.ScaleType.CENTER_CROP :
                ImageView.ScaleType.FIT_XY);
    }

    @Override
    public void showPopupMenu(View view) {
        popup = MenuHelper.createMenu(getContext(), R.menu.menu_details, view);
        popup.setOnMenuItemClickListener(this);
        if (isFavorite)
            popup.getMenu().findItem(R.id.menu_favorite).setTitle("Удалить из избранного");
        popup.show();
    }

    @Override
    public void dismissPopupMenu() {
        if (popup != null) popup.dismiss();
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_favorite:
                DialogFragment dialogFavorite = MessageDialog.newInstance("Добавить в избранное",
                        getContext().getResources().getString(R.string.text_favorite), onClickFavorite);
                dialogFavorite.show(mPresenter.getFragManager(), "DialogFavorite");
                break;
            case R.id.menu_download:
                DialogFragment dialogDownload = MessageDialog.newInstance("Скачать изображение",
                        getContext().getResources().getString(R.string.text_download), onClickDownload);
                dialogDownload.show(mPresenter.getFragManager(), "DialogDownload");
                break;
            case R.id.menu_share:
                mPresenter.onClickShare();
                break;
        }
        return true;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_MOVE:
                if (!isStart) animateImage(true);
                break;
        }
        return true;
    }

    private MessageDialog.ClickOnOkBtn onClickFavorite = () -> mPresenter.onClickFavorite();

    private MessageDialog.ClickOnOkBtn onClickDownload = () -> mPicasso.load(url).into(mPresenter.target);

    @OnClick(R.id.container_author)
    void onClickAuthor() {
        mPresenter.onClickAuthor();
    }
}
