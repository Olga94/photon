package com.example.photon.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.example.photon.BuildConfig;
import com.example.photon.R;
import com.example.photon.data.storage.model.Find;
import com.example.photon.di.DaggerService;
import com.example.photon.di.components.AppComponent;
import com.example.photon.di.modules.PicassoCacheModule;
import com.example.photon.di.modules.RootModule;
import com.example.photon.di.scopes.RootScope;
import com.example.photon.flow.TreeKeyDispatcher;
import com.example.photon.mvp.presenters.RootPresenter;
import com.example.photon.mvp.views.IActionBarView;
import com.example.photon.mvp.views.IRootView;
import com.example.photon.mvp.views.IView;
import com.example.photon.ui.screens.main.MainScreen;
import com.example.photon.ui.screens.profile.ProfileScreen;
import com.example.photon.ui.screens.splash.SplashScreen;
import com.example.photon.ui.screens.upload.UploadScreen;
import com.example.photon.utils.MenuItemHolder;
import com.google.firebase.crash.FirebaseCrash;
import com.squareup.picasso.Picasso;

import java.util.List;

import javax.annotation.Nullable;
import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import flow.Direction;
import flow.Flow;
import flow.History;
import mortar.MortarScope;
import mortar.bundler.BundleServiceRunner;

public class RootActivity extends AppCompatActivity implements IRootView, IActionBarView,
        BottomNavigationView.OnNavigationItemSelectedListener {

    @Inject
    RootPresenter mRootPresenter;

    @BindView(R.id.coordinator_container)
    CoordinatorLayout mCoordinatorLayout;
    @BindView(R.id.app_bar)
    AppBarLayout mAppBar;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.root_container)
    FrameLayout mRootContainer;
    @BindView(R.id.bottom_navigation)
    BottomNavigationView mBottomNavigation;

    private ActionBar mActionBar;
    private List<MenuItemHolder> mActionBarMenuItem;

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_root);
        BundleServiceRunner.getBundleServiceRunner(this).onCreate(savedInstanceState);
        ButterKnife.bind(this);

        RootComponent rootComponent = DaggerService.getDaggerComponent(this);
        rootComponent.inject(this);
        initToolbar();
        mBottomNavigation.setOnNavigationItemSelectedListener(this);

        mRootPresenter.takeView(this);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        BundleServiceRunner.getBundleServiceRunner(this).onSaveInstanceState(outState);
    }

    @Override
    protected void onResume() {
        mRootPresenter.takeView(this);
        super.onResume();
    }

    @Override
    protected void onPause() {
        mRootPresenter.dropView(this);
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        mRootPresenter.dropView(this);
        if (isFinishing()) {
            //ScreenScoper.destroyScreenScope(CatalogScreen.class.getName());
            //ScreenScoper.destroyScreenScope(AccountScreen.class.getName());
            //ScreenScoper.destroyScreenScope(AuthScreen.class.getName());
        }
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        if (getCurrentScreen() != null && !getCurrentScreen().viewOnBackPressed() && !Flow.get(this).goBack()) {
            super.onBackPressed();
        }
    }

    //endregion

    @Override
    protected void attachBaseContext(Context newBase) {
        newBase = Flow.configure(newBase, this)
                .defaultKey(new SplashScreen())
                .dispatcher(new TreeKeyDispatcher(this))
                .install();
        super.attachBaseContext(newBase);
    }

    @Override
    public Object getSystemService(String name) {
        MortarScope rootActivityScope = MortarScope.findChild(getApplicationContext(), RootActivity.class.getName());
        return rootActivityScope.hasService(name) ? rootActivityScope.getService(name) : super.getSystemService(name);
    }


    //region===================== IRootView ==========================

    @Override
    public void showMessage(String message) {
        Snackbar.make(mCoordinatorLayout, message, BaseTransientBottomBar.LENGTH_SHORT).show();
    }

    @Override
    public void showError(Throwable e) {
        if (BuildConfig.DEBUG) {
            showMessage(e.getMessage());
            e.printStackTrace();
        } else {
            showMessage("Извините, что-то пошло не так");
            FirebaseCrash.report(e);
        }
    }

    @Override
    public void setVisibleBottomNavigation(boolean isVisible) {
        mBottomNavigation.setVisibility(isVisible ? View.VISIBLE : View.GONE);
    }

    @Nullable
    @Override
    public IView getCurrentScreen() {
        return (IView) mRootContainer.getChildAt(0);
    }

    @Override
    public CoordinatorLayout getLayout() {
        return mCoordinatorLayout;
    }

    @Override
    public boolean viewOnBackPressed() {
        return false;
    }

    @Override
    public FragmentManager getFM() {
        return getSupportFragmentManager();
    }

    //endregion

    public boolean isAllGranted(@NonNull String[] permissions, boolean allGranted) {
        for (String permission : permissions) {
            int selfPermission = ContextCompat.checkSelfPermission(this, permission);
            if (selfPermission != PackageManager.PERMISSION_GRANTED) {
                allGranted = false;
                break;
            }
        }
        return allGranted;
    }


    //region===================== Toolbar ==========================

    private void initToolbar() {
        setSupportActionBar(mToolbar);
        mActionBar = getSupportActionBar();
    }

    //endregion

    //region===================== IActionBarView ==========================


    @Override
    public void setTitle(CharSequence title) {
        mActionBar.setTitle(title);
    }

    @Override
    public void setVisibleToolbar(boolean visible) {
        if (visible) {
            mToolbar.setVisibility(View.VISIBLE);
        } else {
            mToolbar.setVisibility(View.GONE);
        }
    }

    @Override
    public void setBackArrow(boolean enabled) {
        if (enabled && getSupportActionBar() != null) {
            mToolbar.setNavigationIcon(R.drawable.ic_back_black_24dp);
            mToolbar.setNavigationOnClickListener(v -> onBackPressed());
        } else {
            mToolbar.setNavigationIcon(null);
            mToolbar.setNavigationOnClickListener(null);
        }
    }

    @Override
    public void setMenuItem(List<MenuItemHolder> items) {
        mActionBarMenuItem = items;
        supportInvalidateOptionsMenu();
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (mActionBarMenuItem != null && !mActionBarMenuItem.isEmpty()) {
            for (MenuItemHolder menuItem : mActionBarMenuItem) {
                MenuItem item = menu.add(menuItem.getTitle());
                item.setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_ALWAYS);
                if (menuItem.isCustomView()) {
                    item.setActionView(menuItem.getIconResId())
                            .getActionView().setOnClickListener(menuItem.getListener());
                } else {
                    item.setIcon(menuItem.getIconResId())
                            .setOnMenuItemClickListener(menuItem.getMenuListener());
                }
            }
        } else {
            menu.clear();
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void setTabLayout(ViewPager pager) {
        View view = mAppBar.getChildAt(1);
        TabLayout tabView;
        if (view == null) {
            tabView = new TabLayout(this);
            tabView.setupWithViewPager(pager);
            tabView.setTabGravity(TabLayout.GRAVITY_FILL);
            mAppBar.addView(tabView);
            pager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabView));
        } else {
            tabView = (TabLayout) view;
            tabView.setupWithViewPager(pager);
            pager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabView));
        }
    }

    @Override
    public void removeTabLayout() {
        View tabView = mAppBar.getChildAt(1);
        if (tabView != null && tabView instanceof TabLayout) {
            mAppBar.removeView(tabView);
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Object key = null;
        switch (item.getItemId()) {
            case R.id.menu_home:
                key = new MainScreen(new Find());
                break;
            case R.id.menu_profile:
                key = new ProfileScreen();
                break;
            case R.id.menu_upload:
                key = new UploadScreen(null);
                break;
        }
        if (key != null) Flow.get(this).setHistory(History.single(key), Direction.FORWARD);
        return true;
    }

    //endregion

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mRootPresenter.onActivityResultHandler(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        mRootPresenter.onRequestPermissionsResultHandler(requestCode, permissions, grantResults);
    }


    //region --------------------DI--------------------------------

    @dagger.Component(dependencies = AppComponent.class, modules = {RootModule.class, PicassoCacheModule.class})
    @RootScope
    public interface RootComponent {
        void inject(RootActivity rootActivity);

        void inject(RootPresenter rootPresenter);

        RootPresenter getRootPresenter();

        Picasso getPicasso();
    }

    //endregion
}
