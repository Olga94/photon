package com.example.photon.ui.screens.find.filter;

import android.content.Context;
import android.support.v7.widget.AppCompatCheckBox;
import android.util.AttributeSet;

import com.example.photon.R;
import com.example.photon.data.storage.model.Filter;
import com.example.photon.di.DaggerService;
import com.example.photon.mvp.views.AbstractView;
import com.example.photon.mvp.views.IFilterView;

import java.util.List;

import butterknife.BindViews;
import butterknife.OnClick;

public class FilterView extends AbstractView<FilterScreen.FilterPresenter> implements IFilterView {

    @BindViews({R.id.cb_meat, R.id.cb_fish, R.id.cb_vegetables, R.id.cb_fruit, R.id.cb_cheese,
            R.id.cb_dessert, R.id.cb_drinks})
    List<AppCompatCheckBox> mListDish;
    @BindViews({R.id.cb_simple, R.id.cb_holiday})
    List<AppCompatCheckBox> mListDecor;
    @BindViews({R.id.cb_hot, R.id.cb_middle, R.id.cb_cold})
    List<AppCompatCheckBox> mListTemperature;
    @BindViews({R.id.cb_natural, R.id.cb_synthetic, R.id.cb_mixed})
    List<AppCompatCheckBox> mListLight;
    @BindViews({R.id.cb_direct, R.id.cb_back_light, R.id.cb_side_light, R.id.cb_mixed_light})
    List<AppCompatCheckBox> mListLightDir;
    @BindViews({R.id.cb_one_light, R.id.cb_two_light, R.id.cb_three_light})
    List<AppCompatCheckBox> mListCountLight;
    @BindViews({R.id.cb_red, R.id.cb_orange, R.id.cb_yellow, R.id.cb_green, R.id.cb_light_blue,
            R.id.cb_blue, R.id.cb_purple, R.id.cb_brown, R.id.cb_black, R.id.cb_white})
    List<AppCompatCheckBox> mListColor;

    private int[] mDishes = {R.string.meat, R.string.fish, R.string.vegetables, R.string.fruit,
            R.string.cheese, R.string.dessert, R.string.drinks};
    private int[] mDecors = {R.string.simple, R.string.holiday};
    private int[] mTemperatures = {R.string.hot, R.string.middle, R.string.cold};
    private int[] mLights = {R.string.natural, R.string.synthetic, R.string.mixed};
    private int[] mLightDirs = {R.string.direct, R.string.back_light, R.string.side_light, R.string.mixed};
    private int[] mCountLights = {R.string.one, R.string.two, R.string.three};
    private int[] mColors = {R.string.red, R.string.orange, R.string.yellow, R.string.green,
            R.string.light_blue, R.string.blue, R.string.purple, R.string.brown, R.string.black, R.string.white};


    public FilterView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean viewOnBackPressed() {
        return false;
    }

    @Override
    protected void initDagger(Context context) {
        DaggerService.<FilterScreen.Component>getDaggerComponent(context).inject(this);
    }

    @OnClick(R.id.btn_find)
    void onClick() {
        Filter filter = getFilter();
        mPresenter.filter(filter);
    }

    private Filter getFilter() {
        Filter filter = new Filter();
        for (int i = 0; i < mListDish.size(); i++)
            if (mListDish.get(i).isChecked())
                filter.addDish(getContext().getResources().getString(mDishes[i]));

        for (int i = 0; i < mListDecor.size(); i++)
            if (mListDecor.get(i).isChecked())
                filter.addDecor(getContext().getResources().getString(mDecors[i]));

        for (int i = 0; i < mListTemperature.size(); i++)
            if (mListTemperature.get(i).isChecked())
                filter.addTemperature(getContext().getResources().getString(mTemperatures[i]));

        for (int i = 0; i < mListLight.size(); i++)
            if (mListLight.get(i).isChecked())
                filter.addLight(getContext().getResources().getString(mLights[i]));

        for (int i = 0; i < mListLightDir.size(); i++)
            if (mListLightDir.get(i).isChecked())
                filter.addLightDir(getContext().getResources().getString(mLightDirs[i]));

        for (int i = 0; i < mListCountLight.size(); i++)
            if (mListCountLight.get(i).isChecked())
                filter.addCountLight(getContext().getResources().getString(mCountLights[i]));

        for (int i = 0; i < mListColor.size(); i++)
            if (mListColor.get(i).isChecked())
                filter.addColor(getContext().getResources().getString(mColors[i]));
        return filter;
    }
}
