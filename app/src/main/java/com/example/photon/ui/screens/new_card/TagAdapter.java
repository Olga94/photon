package com.example.photon.ui.screens.new_card;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.photon.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TagAdapter extends RecyclerView.Adapter<TagAdapter.TagViewHolder> {

    private List<String> mTags = new ArrayList<>();
    private View.OnClickListener mListener;

    public TagAdapter() {
    }

    public TagAdapter(List<String> tags) {
        mTags = tags;
    }

    public TagAdapter(List<String> tags, View.OnClickListener listener) {
        mTags = tags;
        mListener = listener;
    }

    @Override
    public TagViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new TagViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_tag, parent, false));
    }

    @Override
    public void onBindViewHolder(TagViewHolder holder, int position) {
        holder.mTag.setText("#" + mTags.get(position));
    }

    @Override
    public int getItemCount() {
        return mTags.size();
    }

    public List<String> getTags() {
        return mTags;
    }

    public void addItem(String tag) {
        mTags.add(tag);
        notifyDataSetChanged();
    }

    public class TagViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.text)
        TextView mTag;

        public TagViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            mTag.setOnClickListener(mListener);
        }
    }
}
