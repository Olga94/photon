package com.example.photon.ui.screens.find;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.photon.R;
import com.example.photon.di.DaggerService;
import com.example.photon.flow.AbstractScreen;
import com.example.photon.ui.screens.find.filter.FilterScreen;
import com.example.photon.ui.screens.find.search.SearchScreen;

import mortar.MortarScope;

public class FindAdapter  extends PagerAdapter {

    private static final int TABS_NUMBER = 2;

    private Context mContext;

    public FindAdapter(Context context) {
        mContext = context;
    }

    @Override
    public int getCount() {
        return TABS_NUMBER;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return mContext.getString(R.string.find_search);
            case 1:
                return mContext.getString(R.string.find_filter);
            default:
                return super.getPageTitle(position);
        }
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        AbstractScreen screen = null;


        View newView;

        switch (position) {
            case 0:
                screen = new SearchScreen();
                break;
            case 1:
                screen = new FilterScreen();
                break;
            default:
                break;
        }

        MortarScope screenScope = createScreenScopeFromContext(container.getContext(), screen);
        Context screenContext = screenScope.createContext(container.getContext());
        newView = LayoutInflater.from(screenContext).inflate(screen.getLayoutResId(), container, false);
        container.addView(newView);

        return newView;
    }

    private MortarScope createScreenScopeFromContext(Context context, AbstractScreen screen) {
        MortarScope parentScope = MortarScope.getScope(context);
        MortarScope childScope = parentScope.findChild(screen.getScopeName());

        if(childScope == null) {
            Object screenComponent = screen.createScreenComponent(parentScope.getService(DaggerService.SERVICE_NAME));
            if(screenComponent == null) {
                throw new IllegalStateException("don't create screen component for" + screen.getScopeName());
            }

            childScope = parentScope.buildChild()
                    .withService(DaggerService.SERVICE_NAME, screenComponent)
                    .build(screen.getScopeName());
        }

        return childScope;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View)object);
    }
}