package com.example.photon.ui.screens.main;

import android.content.Context;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.MenuItem;
import android.view.View;

import com.example.photon.R;
import com.example.photon.di.DaggerService;
import com.example.photon.mvp.views.AbstractView;
import com.example.photon.mvp.views.IMainView;
import com.example.photon.ui.dialogs.ProfileDialog;
import com.example.photon.utils.MenuHelper;

import butterknife.BindView;

public class MainView extends AbstractView<MainScreen.MainPresenter> implements IMainView,
        PopupMenu.OnMenuItemClickListener, ProfileDialog.ClickOnBtn {

    private MainAdapter mAdapter;

    @BindView(R.id.list_photo_card)
    RecyclerView mListPhotoCards;
    private Snackbar snackbar;
    private PopupMenu popup;

    public MainView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean viewOnBackPressed() {
        return false;
    }

    @Override
    protected void initDagger(Context context) {
        DaggerService.<MainScreen.Component>getDaggerComponent(context).inject(this);
    }

    @Override
    public void initView() {
        mAdapter = new MainAdapter();
        GridLayoutManager layoutManager = new GridLayoutManager(getContext(), 2);
        mListPhotoCards.setAdapter(mAdapter);
        mListPhotoCards.setLayoutManager(layoutManager);
    }

    @Override
    public void showPopupMenu(View v) {
        popup = new PopupMenu(getContext(), v);
        popup.inflate(R.menu.menu_main);
        popup.setOnMenuItemClickListener(this);

        MenuItem signIn = popup.getMenu().findItem(R.id.menu_sign_in);
        MenuItem signUp = popup.getMenu().findItem(R.id.menu_sign_up);
        if (mPresenter.isAuth()) {
            signIn.setTitle("Выйти");
            signUp.setVisible(false);
        } else {
            signIn.setTitle("Войти");
            signUp.setVisible(true);
        }

        popup.show();
    }

    @Override
    public void dismissPopupMenu() {
        if (popup != null) popup.dismiss();
    }

    @Override
    public void showSnackBar(int color, String message) {
        snackbar = Snackbar.make(mPresenter.getRootView().getLayout(), message,
                BaseTransientBottomBar.LENGTH_INDEFINITE)
                .setActionTextColor(ContextCompat.getColor(getContext(), android.R.color.white))
                .setAction("Сбросить поиск", v -> mPresenter.clearSearch());
        snackbar.getView().setBackgroundColor(ContextCompat.getColor(getContext(), color));
        snackbar.show();
    }

    @Override
    public void dismissSnackBar() {
        if (snackbar != null)
            snackbar.dismiss();
    }

    public MainAdapter getAdapter() {
        return mAdapter;
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_sign_in:
                if (mPresenter.isAuth()) {
                    mPresenter.logout();
                } else {
                    DialogFragment dialogSignIn = ProfileDialog.newInstance(false, this);
                    dialogSignIn.show(mPresenter.getFragManager(), "DialogSignIn");
                }
                break;
            case R.id.menu_sign_up:
                DialogFragment dialogSignUp = ProfileDialog.newInstance(true, this);
                dialogSignUp.show(mPresenter.getFragManager(), "DialogSignUp");
                break;
        }
        return true;
    }

    @Override
    public void onClickSignIn(String email, String pass) {
        mPresenter.signIn(email, pass);
    }

    @Override
    public void onClickSignUp(String name, String login, String email, String pass) {
        mPresenter.signUp(name, login, email, pass);
    }

    @Override
    public void onClickEdit(String name, String login) {

    }
}
