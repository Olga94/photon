package com.example.photon.ui.screens.profile;

import android.content.Context;
import android.net.Uri;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.photon.R;
import com.example.photon.data.storage.dto.OwnerDto;
import com.example.photon.di.DaggerService;
import com.example.photon.mvp.views.AbstractView;
import com.example.photon.mvp.views.IProfileView;
import com.example.photon.ui.dialogs.AlbumDialog;
import com.example.photon.ui.dialogs.ProfileDialog;
import com.example.photon.utils.CircularTransformation;
import com.example.photon.utils.MenuHelper;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class ProfileView extends AbstractView<ProfileScreen.ProfilePresenter> implements IProfileView,
        ProfileDialog.ClickOnBtn, PopupMenu.OnMenuItemClickListener, AlbumDialog.ClickOnBtn {

    @Inject
    Picasso mPicasso;

    private DialogFragment mDialogProfile;

    @BindView(R.id.login_panel)
    LinearLayout mLoginPanel;
    @BindView(R.id.profile_panel)
    LinearLayout mProfilePanel;
    @BindView(R.id.list_album)
    RecyclerView mListAlbum;
    @BindView(R.id.image_author)
    ImageView mImageAuthor;
    @BindView(R.id.txt_name)
    TextView mNameAuthor;
    @BindView(R.id.txt_count_album)
    TextView mCountAlbum;
    @BindView(R.id.txt_count_card)
    TextView mCountCard;
    @BindView(R.id.txt_not_album)
    TextView mTxtNotAlbum;

    private ProfileAdapter adapter;
    private PopupMenu popup;

    public ProfileView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean viewOnBackPressed() {
        return false;
    }

    @Override
    protected void initDagger(Context context) {
        DaggerService.<ProfileScreen.Component>getDaggerComponent(context).inject(this);
    }

    @OnClick(R.id.btn_sign_in)
    void onClickSignIn() {
        mDialogProfile = ProfileDialog.newInstance(false, this);
        mDialogProfile.show(mPresenter.getFragManager(), "DialogLogin");
    }

    @OnClick(R.id.btn_registration)
    void onClickRegistration() {
        mDialogProfile = ProfileDialog.newInstance(true, this);
        mDialogProfile.show(mPresenter.getFragManager(), "DialogRegistration");
    }

    @Override
    public void initView() {
        mProfilePanel.setVisibility(VISIBLE);
        mLoginPanel.setVisibility(GONE);
        adapter = new ProfileAdapter();
        GridLayoutManager layoutManager = new GridLayoutManager(getContext(), 2);
        mListAlbum.setLayoutManager(layoutManager);
        mListAlbum.setAdapter(adapter);
    }

    public ProfileAdapter getAdapter() {
        return adapter;
    }

    @Override
    public void showLoginPanel() {
        mProfilePanel.setVisibility(GONE);
        mLoginPanel.setVisibility(VISIBLE);
    }

    @Override
    public void showDialog() {
        DialogFragment dialogAddAlbum = AlbumDialog.newInstance(this);
        dialogAddAlbum.show(mPresenter.getFragManager(), "AlbumDialog");
    }

    @Override
    public void showDialogPhoto() {
        String source[] = {"Загрузить из галлереи", "Сделать фото", "Отмена"};
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
        alertDialog.setTitle("Установить фото");
        alertDialog.setItems(source, (dialog, which) -> {
            switch (which) {
                case 0:
                    mPresenter.chooseGallery();
                    break;
                case 1:
                    mPresenter.chooseCamera();
                    break;
                case 2:
                    dialog.cancel();
                    break;
            }
        });
        alertDialog.show();
    }

    @Override
    public void showPopupMenu(View view) {
        popup = MenuHelper.createMenu(getContext(), R.menu.menu_profile, view);
        popup.setOnMenuItemClickListener(this);
        popup.show();
    }

    @Override
    public void dismissPopupMenu() {
        if (popup != null) popup.dismiss();
    }

    @Override
    public void showDialogEditUser(String name, String login) {
        DialogFragment dialogEditProfile = ProfileDialog.newInstance(name, login, this);
        dialogEditProfile.show(mPresenter.getFragManager(), "ProfileDialog");
    }

    @Override
    public void updateUser(String name, String login) {
        mNameAuthor.setText(name);
    }

    @Override
    public void showUser(OwnerDto ownerDto) {
        if (ownerDto.getAvatar() == null)
            mPicasso
                    .load(R.drawable.no_profile)
                    .transform(new CircularTransformation())
                    .into(mImageAuthor);
        else
            updateAvatarPhoto(Uri.parse(ownerDto.getAvatar()));

        mNameAuthor.setText(ownerDto.getName());
        mCountAlbum.setText(String.valueOf(ownerDto.getAlbumCount()));
        mCountCard.setText(String.valueOf(ownerDto.getPhotocardCount()));
    }

    @Override
    public void showTextEmpty(boolean isVisible) {
        if (isVisible)
            mTxtNotAlbum.setVisibility(VISIBLE);
        else
            mTxtNotAlbum.setVisibility(GONE);
    }

    @Override
    public void updateAvatarPhoto(Uri uri) {
        mPicasso
                .load(uri)
                .resize(140, 140)
                .transform(new CircularTransformation())
                .centerCrop()
                .into(mImageAuthor);
    }

    @Override
    public void onClickSignIn(String email, String pass) {
        mPresenter.onClickSignIn(email, pass);
    }

    @Override
    public void onClickSignUp(String name, String login, String email, String pass) {
        mPresenter.onClickSignUp(name, login, email, pass);
    }

    @Override
    public void onClickAdd(String name, String description) {
        mPresenter.onClickAdd(name, description);
    }

    @Override
    public void onClickEdit(String name, String login) {
        mPresenter.onClickEdit(name, login);
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_add_album:
                showDialog();
                break;
            case R.id.menu_upload:
                mPresenter.takePhoto();
                break;
            case R.id.menu_edit:
                mPresenter.editUser();
                break;
            case R.id.menu_logout:
                mPresenter.logout();
                break;
        }
        return true;
    }
}
