package com.example.photon.ui.screens.profile;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.FragmentManager;
import android.util.Log;

import com.example.photon.R;
import com.example.photon.data.network.req.AlbumReq;
import com.example.photon.data.network.req.OwnerReq;
import com.example.photon.data.rx.SimpleDisposableSubscriber;
import com.example.photon.data.storage.dto.ActivityPermissionsResultDto;
import com.example.photon.data.storage.dto.AlbumDto;
import com.example.photon.data.storage.dto.OwnerDto;
import com.example.photon.data.storage.realm.AlbumRealm;
import com.example.photon.data.storage.realm.OwnerRealm;
import com.example.photon.di.DaggerService;
import com.example.photon.di.scopes.ProfileScope;
import com.example.photon.flow.AbstractScreen;
import com.example.photon.flow.Screen;
import com.example.photon.mvp.models.ProfileModel;
import com.example.photon.mvp.presenters.AbstractPresenter;
import com.example.photon.mvp.presenters.IProfilePresenter;
import com.example.photon.mvp.presenters.RootPresenter;
import com.example.photon.ui.activities.RootActivity;
import com.example.photon.ui.screens.album.AlbumScreen;
import com.example.photon.ui.screens.main.MainScreen;
import com.example.photon.utils.App;
import com.example.photon.utils.ConstantManager;
import com.example.photon.utils.LocalStorageAvatar;
import com.example.photon.utils.MenuItemHolder;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

import dagger.Provides;
import flow.Flow;
import id.zelory.compressor.Compressor;
import io.realm.RealmList;
import mortar.MortarScope;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.Manifest.permission.CAMERA;

@Screen(R.layout.screen_profile)
public class ProfileScreen extends AbstractScreen<RootActivity.RootComponent> {

    @Override
    public Object createScreenComponent(RootActivity.RootComponent patentComponent) {
        return DaggerProfileScreen_Component.builder()
                .rootComponent(patentComponent)
                .module(new Module())
                .build();
    }
    //region =========================== DI =====================

    @dagger.Module
    public class Module {
        @Provides
        @ProfileScope
        ProfilePresenter provideProfilePresenter() {
            return new ProfilePresenter();
        }

        @Provides
        @ProfileScope
        ProfileModel provideMainModel() {
            return new ProfileModel();
        }
    }

    @dagger.Component(dependencies = RootActivity.RootComponent.class, modules = Module.class)
    @ProfileScope
    public interface Component {
        void inject(ProfilePresenter presenter);

        void inject(ProfileView view);

        void inject(ProfileAdapter adapter);

        RootPresenter getRootPresenter();

        Picasso getPicasso();
    }
    //endregion

    public class ProfilePresenter extends AbstractPresenter<ProfileView, ProfileModel> implements IProfilePresenter {

        private boolean mIsActivityResultWaiting;
        private boolean mIsActivityPermissionsResultWaiting;

        @Override
        protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);
            getView().initView();
            if (mModel.isAuthUser()) {
                mModel.getUserInfo()
                        .switchIfEmpty(Observable.create(subscriber -> getView().showTextEmpty(true)))
                        .subscribe(ownerDto -> {
                            if (getView() != null)
                                getView().showUser(ownerDto);
                        });

                List<AlbumRealm> albumRealmList = mModel.getAlbumRealm();
                Observable<AlbumDto> albumsObsRealm = Observable.from(albumRealmList)
                        .subscribeOn(AndroidSchedulers.mainThread())
                        .map(AlbumDto::new);

                mCompositeSubscription.add(subscribe(albumsObsRealm, new ViewSubscriber<AlbumDto>() {
                    @Override
                    public void onNext(AlbumDto albumDto) {
                        getView().showTextEmpty(false);
                        getView().getAdapter().addItem(albumDto);
                    }
                }));
            } else {
                getView().showLoginPanel();
            }

            if (mIsActivityResultWaiting) {
                mRootPresenter.getActivityResultPublishUrlSubject()
                        .subscribe(new SimpleDisposableSubscriber<String>() {
                            @Override
                            public void onNext(String uri) {
                                mIsActivityResultWaiting = false;
                                getView().updateAvatarPhoto(Uri.parse(uri));
                                mModel.saveAvatar(uri);
                            }
                        });
            }

            if (mIsActivityPermissionsResultWaiting) {
                getPermissionsResultPublishSubject()
                        .subscribe(new SimpleDisposableSubscriber<ActivityPermissionsResultDto>() {
                            @Override
                            public void onNext(ActivityPermissionsResultDto result) {
                                mIsActivityPermissionsResultWaiting = false;
                                if (!result.isAllGranted()) {
                                    if (getRootView() != null)
                                        getRootView().showMessage("На операцию нет разрешений");
                                } else {
                                    switch (result.getRequestCode()) {
                                        case ConstantManager.REQUEST_PERMISSION_CAMERA_CODE:
                                            takePhotoFromCamera();
                                            break;
                                        case ConstantManager.REQUEST_PERMISSION_FOR_GALLERY_CODE:
                                            takePhotoFromGallery();
                                    }
                                }
                            }
                        });
            }
        }

        @Override
        protected void onSave(Bundle outState) {
            getView().dismissPopupMenu();
            super.onSave(outState);
        }

        @Override
        protected void initActionBar() {
            if (mModel.isAuthUser())
                mRootPresenter.newActionBarBuilder()
                        .setTitle("Профиль")
                        .addAction(new MenuItemHolder("Меню", R.layout.icon_settings, v -> {
                            getView().showPopupMenu(v);
                        }))
                        .build();
            else
                mRootPresenter.newActionBarBuilder()
                        .setTitle("Профиль")
                        .build();
        }

        @Override
        protected void initDagger(MortarScope scope) {
            ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
        }

        @Override
        public void onClickSignIn(String email, String pass) {
            mModel.signIn(email, pass).subscribe(ownerRes -> {
            }, throwable -> {
                if (getRootView() != null)
                    getRootView().showError(throwable);
            }, this::onLoginSuccess);
        }

        @Override
        public void onClickSignUp(String name, String login, String email, String pass) {
            mModel.signUp(name, login, email, pass).subscribe(ownerRes -> {
            }, throwable -> {
                if (getRootView() != null)
                    getRootView().showError(throwable);
            }, this::onLoginSuccess);
        }

        @Override
        public FragmentManager getFragManager() {
            if (getRootView() != null)
                return getRootView().getFM();
            return null;
        }

        @Override
        public void onClickAdd(String name, String description) {
            AlbumRealm albumRealm = new AlbumRealm(name, description, mModel.getId());
            mModel.createAlbum(albumRealm);

            Observable<AlbumDto> newAlbumObsRealm = Observable.from(new AlbumRealm[]{albumRealm})
                    .subscribeOn(AndroidSchedulers.mainThread())
                    .map(AlbumDto::new);
            mCompositeSubscription.add(subscribe(newAlbumObsRealm, new ViewSubscriber<AlbumDto>() {
                @Override
                public void onNext(AlbumDto albumDto) {
                    getView().getAdapter().addItem(albumDto);
                }
            }));
            getView().getAdapter().notifyDataSetChanged();
        }

        @Override
        public void logout() {
            mModel.logout();
            initActionBar();
            getView().showLoginPanel();
        }

        @Override
        public void onClickEdit(String name, String login) {
            OwnerReq owner = new OwnerReq(name, login, null);
            mModel.updateUser(owner).subscribe(userEditRes -> getView().updateUser(userEditRes.getName(), userEditRes.getAvatar()), throwable -> {
                if (getRootView() != null)
                    getRootView().showError(throwable);
            });
        }

        @Override
        public void editUser() {
            mModel.getUserInfo().subscribe(owner -> {
                getView().showDialogEditUser(owner.getName(), owner.getLogin());
            });
        }

        @Override
        public void openAlbum(AlbumDto album) {
            Flow.get(getView()).set(new AlbumScreen(album));
        }

        @Override
        public void takePhoto() {
            if (getView() != null) {
                getView().showDialogPhoto();
            }
        }

        private void onLoginSuccess() {
            if (getView() != null) {
                initActionBar();
                getView().initView();
                mModel.getUserInfo().subscribe(ownerDto -> {
                    getView().showUser(ownerDto);
                    Observable<AlbumDto> albumsObsRealm = Observable.from(ownerDto.getAlbums())
                            .subscribeOn(AndroidSchedulers.mainThread());

                    mCompositeSubscription.add(subscribe(albumsObsRealm, new ViewSubscriber<AlbumDto>() {
                        @Override
                        public void onNext(AlbumDto albumDto) {
                            getView().getAdapter().addItem(albumDto);
                        }
                    }));
                });
            }
        }

        //region ================= Permissions =================

        private Observable<ActivityPermissionsResultDto> getPermissionsResultPublishSubject() {
            return mRootPresenter.getActivityPermissionsResultSubject()
                    .filter(res ->
                            res.getRequestCode() == ConstantManager.REQUEST_PERMISSION_CAMERA_CODE
                                    || res.getRequestCode() == ConstantManager.REQUEST_PERMISSION_FOR_GALLERY_CODE);
        }

        @Override
        public void chooseCamera() {
            if (getRootView() != null) {
                String[] permissions = new String[]{CAMERA, WRITE_EXTERNAL_STORAGE};
                if (mRootPresenter.checkPermissions(permissions)) {
                    takePhotoFromCamera();
                } else {
                    if (mRootPresenter.requestPermissions(permissions, ConstantManager.REQUEST_PERMISSION_CAMERA_CODE)) {
                        mIsActivityPermissionsResultWaiting = true;
                    } else {
                        getRootView().showMessage("Нет разрешений на выполнение данной операции");
                    }
                }
            }
        }

        private void takePhotoFromCamera() {
            Uri photoFileUri = mRootPresenter.createFileForPhoto();
            if (photoFileUri != null) {
                Intent takeCaptureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                takeCaptureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoFileUri);
                mRootPresenter.startActivityForResult(takeCaptureIntent, ConstantManager.REQUEST_PROFILE_PHOTO_CAMERA);
                mIsActivityResultWaiting = true;
            }
        }

        @Override
        public void chooseGallery() {
            String[] permissions = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE};
            if (mRootPresenter.checkPermissions(permissions)) {
                takePhotoFromGallery();
            } else {
                if (mRootPresenter.requestPermissions(permissions, ConstantManager.REQUEST_PERMISSION_FOR_GALLERY_CODE)) {
                    mIsActivityPermissionsResultWaiting = true;
                } else if (getRootView() != null) {
                    getRootView().showMessage("Нет разрешений на выполнение данной операции");
                }
            }
        }

        private void takePhotoFromGallery() {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_OPEN_DOCUMENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            mRootPresenter.startActivityForResult(intent, ConstantManager.REQUEST_PROFILE_PHOTO_GALLERY);
            mIsActivityResultWaiting = true;
        }

        //endregion
    }
}
