package com.example.photon.ui.screens.new_card;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.photon.R;
import com.example.photon.data.storage.dto.AlbumDto;
import com.example.photon.di.DaggerService;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NewCardAdapter extends RecyclerView.Adapter<NewCardAdapter.NewCardViewHolder> {

    @Inject
    Picasso mPicasso;
    @Inject
    NewCardScreen.NewCardPresenter mPresenter;

    private List<AlbumDto> mAlbumCards = new ArrayList<>();
    private int mPositionSelect = -1;

    public NewCardAdapter() {
    }

    public void addItem(AlbumDto albumDto) {
        mAlbumCards.add(albumDto);
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        DaggerService.<NewCardScreen.Component>getDaggerComponent(recyclerView.getContext()).inject(this);
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public NewCardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_photo_card, parent, false);
        return new NewCardViewHolder(view);
    }

    @Override
    public void onBindViewHolder(NewCardViewHolder holder, int position) {
        AlbumDto album = mAlbumCards.get(position);

        holder.mContainerTitle.setVisibility(View.VISIBLE);
        holder.mTxtTitle.setText(album.getTitle());
        holder.mCountCard.setText(String.valueOf(album.getPhotocards().size()));
        holder.mBottomContainer.setVisibility(View.INVISIBLE);
        if (album.getPhotocards().size() == 0)
            mPicasso
                    .load(R.drawable.no_image)
                    .into(holder.mImageCard);
        else
            mPicasso
                    .load(album.getPhotocards().get(0).getPhoto())
                    .error(R.drawable.no_image)
                    .into(holder.mImageCard);
        holder.mViewSelect.setVisibility(mPositionSelect == position ? View.VISIBLE : View.INVISIBLE);

    }

    @Override
    public int getItemCount() {
        return mAlbumCards.size();
    }


    public class NewCardViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.image_card)
        ImageView mImageCard;
        @BindView(R.id.bottom_container)
        LinearLayout mBottomContainer;
        @BindView(R.id.container_title)
        LinearLayout mContainerTitle;
        @BindView(R.id.txt_title)
        TextView mTxtTitle;
        @BindView(R.id.count_card)
        TextView mCountCard;
        @BindView(R.id.view_select)
        View mViewSelect;

        public NewCardViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            mImageCard.setOnClickListener(v -> {
                mPresenter.onClickCard(mAlbumCards.get(getAdapterPosition()).getId());
                mPositionSelect = getAdapterPosition();
                notifyDataSetChanged();
            });
        }
    }
}
