package com.example.photon.ui.screens.find.search;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageButton;

import com.example.photon.R;
import com.example.photon.di.DaggerService;
import com.example.photon.mvp.views.AbstractView;
import com.example.photon.mvp.views.ISearchView;
import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.google.android.flexbox.JustifyContent;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnTextChanged;

public class SearchView extends AbstractView<SearchScreen.SearchPresenter> implements ISearchView {

    @BindView(R.id.autocomplete_search)
    AutoCompleteTextView mAutoSearch;
    @BindView(R.id.back)
    ImageButton mBack;
    @BindView(R.id.clear_search)
    ImageButton mClearSearch;
    @BindView(R.id.list_tag)
    RecyclerView mListTag;

    public static final int PREVIEW_STATE = 0;
    public static final int SEARCH_STATE = 1;
    private int currentState = 0;
    private SearchAdapter mTagAdapter;
    private ArrayAdapter<String> mHistoryAdapter;

    public SearchView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean viewOnBackPressed() {
        return false;
    }

    @Override
    protected void initDagger(Context context) {
        DaggerService.<SearchScreen.Component>getDaggerComponent(context).inject(this);
    }

    @OnTextChanged(value = R.id.autocomplete_search, callback = OnTextChanged.Callback.TEXT_CHANGED)
    void onChange(CharSequence s) {
        currentState = s.length() > 0 ? SEARCH_STATE : PREVIEW_STATE;
        showState();
    }

    private void showState() {
        switch (currentState) {
            case PREVIEW_STATE:
                mBack.setImageResource(R.drawable.ic_back_arrow_black_24dp);
                break;
            case SEARCH_STATE:
                mBack.setImageResource(R.drawable.ic_done_black_24dp);
                break;
        }
    }

    @Override
    public void initView() {
        mTagAdapter = new SearchAdapter(getContext());
        FlexboxLayoutManager layoutManager = new FlexboxLayoutManager(getContext());
        layoutManager.setFlexDirection(FlexDirection.ROW);
        layoutManager.setJustifyContent(JustifyContent.FLEX_START);
        mListTag.setAdapter(mTagAdapter);
        mListTag.setLayoutManager(layoutManager);

        mHistoryAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1);
        mAutoSearch.setAdapter(mHistoryAdapter);
        mAutoSearch.setOnItemClickListener((parent, view, position, id) -> {
            String tag = (String) parent.getItemAtPosition(position);
            mAutoSearch.setText(tag);
        });
    }

    public SearchAdapter getTagAdapter() {
        return mTagAdapter;
    }

    public ArrayAdapter<String> getHistoryAdapter() {
        return mHistoryAdapter;
    }

    @OnClick(R.id.clear_search)
    void onClickClear() {
        mAutoSearch.setText("");
    }

    @OnClick(R.id.back)
    void onClickBac() {
        switch (currentState) {
            case PREVIEW_STATE:
                mPresenter.onBack();
                break;
            case SEARCH_STATE:
                mPresenter.onСlickSearch(mAutoSearch.getText().toString());
                break;
        }
    }
}
