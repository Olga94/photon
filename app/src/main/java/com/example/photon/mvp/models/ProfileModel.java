package com.example.photon.mvp.models;

import com.example.photon.data.network.req.AlbumReq;
import com.example.photon.data.network.req.LoginReq;
import com.example.photon.data.network.req.OwnerReq;
import com.example.photon.data.network.req.RegistrationReq;
import com.example.photon.data.network.res.OwnerRes;
import com.example.photon.data.network.res.UserEditRes;
import com.example.photon.data.storage.dto.OwnerDto;
import com.example.photon.data.storage.realm.AlbumRealm;
import com.example.photon.jobs.SendAlbum;
import com.example.photon.jobs.SendAvatar;

import java.util.List;

import io.realm.RealmList;
import okhttp3.MultipartBody;
import rx.Observable;

public class ProfileModel extends AbstractModel{

    public Observable<OwnerRes> signIn(String email, String pass) {
        return mDataManager.signInUser(new LoginReq(email, pass));
    }

    public Observable<OwnerRes> signUp(String name, String login, String email, String pass) {
        return mDataManager.signUpUser(new RegistrationReq(name, login, email, pass));
    }

    public Observable<OwnerDto> getUserInfo() {
        return mDataManager.getDataOwner(mDataManager.getUserId());
    }

    public boolean isAuthUser() {
        return mDataManager.isAuth();
    }

    public void logout() {
        mDataManager.removeToken();
    }

    public Observable<UserEditRes> updateUser(OwnerReq owner) {
        return mDataManager.updateUserData(owner);
    }

    public void createAlbum(AlbumRealm album) {
        SendAlbum albumJob = new SendAlbum(new AlbumReq(album));
        mJobManager.addJobInBackground(albumJob);
    }

    public void saveAvatar(String uri){
        SendAvatar avatarJob = new SendAvatar(uri);
        mJobManager.addJobInBackground(avatarJob);
    }

    public String getId() {
        return mDataManager.getUserId();
    }

    public List<AlbumRealm> getAlbumRealm() {
        return mDataManager.getAllAlbums();
    }
}
