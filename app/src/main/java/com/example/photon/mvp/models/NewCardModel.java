package com.example.photon.mvp.models;

import com.example.photon.data.network.req.PhotoCardReq;
import com.example.photon.data.storage.dto.TagDto;
import com.example.photon.data.storage.realm.AlbumRealm;
import com.example.photon.jobs.SendPhotoCard;

import java.util.List;

import rx.Observable;

public class NewCardModel extends AbstractModel{

    public List<AlbumRealm> getAlbums() {
        return mDataManager.getAllAlbums();
    }

    public Observable<TagDto> getTags() {
        return mDataManager.getTagsFromDb();
    }

    public void savePhotoCard(PhotoCardReq photoCardReq, String imageUri) {
        SendPhotoCard photoCardJob = new SendPhotoCard(photoCardReq, imageUri);
        mJobManager.addJobInBackground(photoCardJob);
    }
}
