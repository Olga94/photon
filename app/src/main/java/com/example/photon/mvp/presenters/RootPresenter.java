package com.example.photon.mvp.presenters;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.view.ViewPager;

import com.example.photon.data.storage.dto.ActivityPermissionsResultDto;
import com.example.photon.data.storage.dto.ActivityResultDto;
import com.example.photon.ui.activities.RootActivity;
import com.example.photon.di.DaggerService;
import com.example.photon.mvp.views.IRootView;
import com.example.photon.utils.AvatarHelper;
import com.example.photon.utils.ConstantManager;
import com.example.photon.utils.MenuItemHolder;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;

import mortar.MortarScope;
import mortar.Presenter;
import mortar.bundler.BundleService;
import rx.Observable;
import rx.subjects.BehaviorSubject;
import rx.subjects.PublishSubject;

public class RootPresenter  extends Presenter<IRootView> {

    private static int DEFAULT_MODE = 0;
    private static int TAB_MODE = 1;
    private String mPhotoFileUrl;
    private BehaviorSubject<ActivityResultDto> mActivityResultSubject = BehaviorSubject.create();
    private BehaviorSubject<ActivityPermissionsResultDto> mActivityPermissionsResultSubject = BehaviorSubject.create();

    public RootPresenter() {
    }

    public BehaviorSubject<ActivityResultDto> getActivityResultSubject() {
        return mActivityResultSubject;
    }

    public BehaviorSubject<ActivityPermissionsResultDto> getActivityPermissionsResultSubject() {
        return mActivityPermissionsResultSubject;
    }

    @Override
    protected void onEnterScope(MortarScope scope) {
        super.onEnterScope(scope);
        ((RootActivity.RootComponent) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
    }

    @Override
    protected BundleService extractBundleService(IRootView view) {
        return BundleService.getBundleService((RootActivity) view);
    }

    @Nullable
    public IRootView getRootView() {
        return getView();
    }

    public ActionBarBuilder newActionBarBuilder() {
        return this.new ActionBarBuilder();
    }

    public void startActivityForResult(Intent intent, int requestCode) {
        ((RootActivity) getView()).startActivityForResult(intent, requestCode);
    }

    public void startActivity(Intent intent) {
        ((RootActivity) getView()).startActivity(intent);
    }

    /**
     * Обработчик результата onActivityResult
     */
    public void onActivityResultHandler(int requestCode, int resultCode, Intent data) {
        mActivityResultSubject.onNext(new ActivityResultDto(requestCode, resultCode, data));
    }

    //region ================= Permissions =================

    /**
     * Обработчик запроса разрешений
     */
    public void onRequestPermissionsResultHandler(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        mActivityPermissionsResultSubject.onNext(new ActivityPermissionsResultDto(requestCode, permissions, grantResults));
    }

    public boolean checkPermissions(@NonNull String[] permissions) {
        boolean allGranted = true;
        for (String permission : permissions) {
            int selfPermission = ContextCompat.checkSelfPermission(((RootActivity) getView()), permission);
            if (selfPermission != PackageManager.PERMISSION_GRANTED) {
                allGranted = false;
                break;
            }
        }
        return allGranted;
    }

    public boolean requestPermissions(@NonNull String[] permissions, int requestCode) {
        boolean isRequested = false;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ((RootActivity) getView()).requestPermissions(permissions, requestCode);
            isRequested = true;
        }
        return isRequested;
    }

    public Observable<String> getActivityResultPublishUrlSubject() {
        return getActivityResultSubject()
                .filter(res -> res.getResultCode() == Activity.RESULT_OK
                        && (res.getRequestCode() == ConstantManager.REQUEST_PROFILE_PHOTO_GALLERY
                        || res.getRequestCode() == ConstantManager.REQUEST_PROFILE_PHOTO_CAMERA))
                .map(res -> (res.getIntent() == null || res.getIntent().getData() == null ?
                        mPhotoFileUrl :
                        res.getIntent().getData().toString()))
                .filter(uri -> uri != null);
    }

    //endregion

    public Uri createFileForPhoto() {
        File file = AvatarHelper.createFileForPhoto();

        mPhotoFileUrl = Uri.fromFile(file).toString();

        return FileProvider.getUriForFile((Activity) getView(),
                ConstantManager.FILE_PROVIDER_AUTHORITY,
                file);
    }

    //region ================= Check Permissions =================

    public boolean checkPermissionsAndRequestIfNotGranted(@NonNull String[] permissions, int requestCode) {
        boolean allGranted = true;
        allGranted = ((RootActivity) getView()).isAllGranted(permissions, allGranted);

        if (!allGranted) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                ((RootActivity) getView()).requestPermissions(permissions, requestCode);
            }
            return false;
        }
        return allGranted;
    }

    //endregion

    public class ActionBarBuilder {
        private boolean isGoBack = false;
        private boolean isVisible = true;
        private CharSequence title;
        private List<MenuItemHolder> items = new ArrayList<>();
        private ViewPager pager;
        private int toolbarMode = DEFAULT_MODE;

        public ActionBarBuilder setBackArrow(boolean enable) {
            this.isGoBack = enable;
            return this;
        }

        public ActionBarBuilder setTitle(CharSequence title) {
            this.title = title;
            return this;
        }

        public ActionBarBuilder addAction(MenuItemHolder menuItem) {
            this.items.add(menuItem);
            return this;
        }

        public ActionBarBuilder setTab(ViewPager pager) {
            this.toolbarMode = TAB_MODE;
            this.pager = pager;
            return this;
        }

        public ActionBarBuilder setVisible(boolean visible) {
            this.isVisible = visible;
            return this;
        }

        public void build() {
            if (getRootView() != null) {
                RootActivity activity = (RootActivity) getRootView();
                activity.setVisibleToolbar(isVisible);
                activity.setTitle(title);
                activity.setBackArrow(isGoBack);
                activity.setMenuItem(items);
                if (toolbarMode == TAB_MODE) {
                    activity.setTabLayout(pager);
                } else {
                    activity.removeTabLayout();
                }
            }
        }
    }
}