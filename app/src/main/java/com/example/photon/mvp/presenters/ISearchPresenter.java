package com.example.photon.mvp.presenters;

public interface ISearchPresenter {
    void onBack();
    void onСlickSearch(String search);
}
