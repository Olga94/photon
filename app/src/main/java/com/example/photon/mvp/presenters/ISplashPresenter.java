package com.example.photon.mvp.presenters;

public interface ISplashPresenter {
    void startMainScreen();
}
