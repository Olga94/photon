package com.example.photon.mvp.models;

import com.example.photon.data.storage.dto.TagDto;

import rx.Observable;

public class SearchModel extends AbstractModel {
    public Observable<TagDto> getTags() {
        return mDataManager.getTagsFromDb();
    }

    public Observable<String> getHistory() {
        return mDataManager.getHistory();
    }

    public void saveHistory(String history) {
        mDataManager.saveHistory(history);
    }
}
