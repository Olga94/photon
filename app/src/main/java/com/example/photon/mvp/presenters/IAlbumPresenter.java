package com.example.photon.mvp.presenters;

import android.support.v4.app.FragmentManager;

import com.example.photon.data.storage.dto.PhotoCardDto;

public interface IAlbumPresenter {
    FragmentManager getFragManager();
    void openUploadScreen();
    void deleteAlbum();
    void editAlbum(String name, String description);
    void onClickCard(PhotoCardDto photoCard);
}
