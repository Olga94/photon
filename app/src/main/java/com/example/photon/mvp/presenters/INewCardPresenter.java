package com.example.photon.mvp.presenters;

public interface INewCardPresenter {
    void onClickOk();
    void onClickCancel();
    void onClickCard(String id);
}
