package com.example.photon.mvp.views;

import com.example.photon.data.network.req.PhotoCardReq;

import java.util.List;

public interface INewCardView extends IView {
    void initView();
    void initTag();
    void showAlbums();
    PhotoCardReq getCard();
}
