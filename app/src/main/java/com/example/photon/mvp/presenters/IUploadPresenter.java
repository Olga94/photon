package com.example.photon.mvp.presenters;

public interface IUploadPresenter {
    void openGallery();
}
