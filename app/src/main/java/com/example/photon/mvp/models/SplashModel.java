package com.example.photon.mvp.models;

import com.example.photon.data.network.res.PhotoCardRes;

import java.util.List;

import rx.Observable;

public class SplashModel extends AbstractModel {

    public Observable<PhotoCardRes> getPhotoCards() {
        return mDataManager.getPhotoCardsObsFromNetwork();
    }

    public Observable<List<String>> getTags() {
        return mDataManager.getTags();
    }
}
