package com.example.photon.mvp.views;

import android.view.View;

import com.example.photon.data.storage.dto.AlbumDto;

public interface IAlbumView extends IView{

    void initView(AlbumDto album);
    void showPopupMenu(View view);
    void dismissPopupMenu();
}
