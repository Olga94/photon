package com.example.photon.mvp.presenters;

import com.example.photon.data.storage.dto.AlbumDto;

public interface IAuthorPresenter {
    void openAlbum(AlbumDto album);
}
