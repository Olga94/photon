package com.example.photon.mvp.presenters;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import com.example.photon.mvp.models.AbstractModel;
import com.example.photon.mvp.views.AbstractView;
import com.example.photon.mvp.views.IRootView;
import com.example.photon.utils.App;

import javax.inject.Inject;

import mortar.MortarScope;
import mortar.ViewPresenter;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public abstract class AbstractPresenter <V extends AbstractView, M extends AbstractModel> extends ViewPresenter<V> {

    @Inject
    public M mModel;

    @Inject
    protected RootPresenter mRootPresenter;
    protected CompositeSubscription mCompositeSubscription;


    @Override
    protected void onEnterScope(MortarScope scope) {
        super.onEnterScope(scope);
        initDagger(scope);
    }

    @Override
    protected void onLoad(Bundle savedInstanceState) {
        super.onLoad(savedInstanceState);
        mCompositeSubscription = new CompositeSubscription();
        initActionBar();
    }

    @Override
    public void dropView(V view) {
        if(mCompositeSubscription.hasSubscriptions()) {
            mCompositeSubscription.unsubscribe();
        }
        super.dropView(view);
    }

    @Nullable
    public IRootView getRootView() {
        return mRootPresenter.getRootView();
    }

    protected abstract void initActionBar();
    protected abstract void initDagger(MortarScope scope);

    protected abstract class ViewSubscriber<T> extends Subscriber<T> {
        @Override
        public void onCompleted() {

        }

        @Override
        public void onError(Throwable e) {
            if (getRootView() != null) {
                getRootView().showError(e);
            }
        }

        @Override
        public abstract void onNext(T t);
    }

    protected <T> Subscription subscribe(Observable<T> observable, ViewSubscriber<T> subscriber){
        return observable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscriber);
    }
}