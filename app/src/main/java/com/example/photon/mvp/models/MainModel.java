package com.example.photon.mvp.models;

import com.example.photon.data.network.req.LoginReq;
import com.example.photon.data.network.req.RegistrationReq;
import com.example.photon.data.network.res.AddResponse;
import com.example.photon.data.network.res.AlbumRes;
import com.example.photon.data.network.res.OwnerRes;
import com.example.photon.data.storage.dto.PhotoCardDto;
import com.example.photon.data.storage.model.Filter;
import com.example.photon.data.storage.model.Search;

import rx.Observable;

public class MainModel extends AbstractModel{

    public Observable<PhotoCardDto> getAllPhotoCards() {
        return mDataManager.getAllPhotoCardsFromDB();
    }

    public Observable<PhotoCardDto> getSearchPhotoCard(Search search) {
        return mDataManager.getSearchPhotoCardsFromDB(search);
    }

    public Observable<PhotoCardDto> getFilterPhotoCard(Filter filters) {
        return mDataManager.getFilterPhotoCardsFromDB(filters);
    }

    public boolean isSignInUser() {
        return mDataManager.isAuth();
    }

    public void logout() {
        mDataManager.removeToken();
    }

    public Observable<OwnerRes> signIn(String email, String pass) {
        return mDataManager.signInUser(new LoginReq(email, pass));
    }

    public Observable<OwnerRes> signUp(String name, String login, String email, String pass) {
        return mDataManager.signUpUser(new RegistrationReq(name, login, email, pass));
    }

    public Observable<AddResponse> addView(String id) {
        return mDataManager.addViewToPhotoCard(id);
    }
}
