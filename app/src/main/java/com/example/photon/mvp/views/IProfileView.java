package com.example.photon.mvp.views;

import android.net.Uri;
import android.view.View;

import com.example.photon.data.storage.dto.OwnerDto;

public interface IProfileView {
    void initView();
    void showLoginPanel();
    void showDialog();
    void showDialogPhoto();
    void showPopupMenu(View view);
    void dismissPopupMenu();
    void showDialogEditUser(String name, String login);
    void updateUser(String name, String login);
    void showUser(OwnerDto ownerDto);
    void showTextEmpty(boolean isVisible);
    void updateAvatarPhoto(Uri uri);
}
