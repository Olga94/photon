package com.example.photon.mvp.models;

import com.birbit.android.jobqueue.JobManager;
import com.example.photon.data.managers.DataManager;
import com.example.photon.di.DaggerService;
import com.example.photon.di.components.DaggerModelComponent;
import com.example.photon.di.components.ModelComponent;
import com.example.photon.di.modules.ModelModule;

import javax.inject.Inject;

public abstract class AbstractModel {
    @Inject
    DataManager mDataManager;
    @Inject
    JobManager mJobManager;

    public AbstractModel() {
        ModelComponent component = DaggerService.getComponent(ModelComponent.class);
        if (component == null) {
            component = createDaggerComponent();
            DaggerService.registerComponent(ModelComponent.class, component);
        }
        component.inject(this);
    }

    private ModelComponent createDaggerComponent() {
        return DaggerModelComponent.builder()
                .modelModule(new ModelModule())
                .build();
    }
}
