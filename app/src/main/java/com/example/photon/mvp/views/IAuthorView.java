package com.example.photon.mvp.views;

import com.example.photon.data.storage.dto.OwnerDto;

public interface IAuthorView {
    void initView(OwnerDto ownerDto);
}
