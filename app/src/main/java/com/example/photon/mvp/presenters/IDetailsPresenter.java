package com.example.photon.mvp.presenters;

import android.support.v4.app.FragmentManager;

public interface IDetailsPresenter {
    void onClickFavorite();
    void onClickAuthor();
    void onClickShare();
    FragmentManager getFragManager();
}
