package com.example.photon.mvp.presenters;

import com.example.photon.data.storage.model.Filter;

public interface IFilterPresenter {
    void filter(Filter filter);
}
