package com.example.photon.mvp.models;

import com.example.photon.data.network.req.AlbumReq;
import com.example.photon.data.network.res.CreateResponse;
import com.example.photon.data.storage.dto.AlbumDto;
import com.example.photon.data.storage.realm.AlbumRealm;

import okhttp3.ResponseBody;
import rx.Observable;

public class AlbumModel extends AbstractModel {

    public Observable<ResponseBody> deleteAlbum(String id) {
        return mDataManager.removeAlbum(id);
    }

    public Observable<CreateResponse> updateAlbum(String id, String name, String description) {
        return mDataManager.updateAlbum(id, new AlbumReq(name, description));
    }

    public AlbumRealm getAlbum(String id) {
        return mDataManager.getAlbumFromDb(id);
    }

    public boolean isUser(String id) {
        return mDataManager.isUser(id);
    }
}
