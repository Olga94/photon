package com.example.photon.mvp.views;

import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.FragmentManager;

import javax.annotation.Nullable;

public interface IRootView extends IView {
    void showMessage(String message);
    void showError(Throwable e);
    void setVisibleBottomNavigation(boolean isVisible);
    FragmentManager getFM();
    @Nullable
    IView getCurrentScreen();
    CoordinatorLayout getLayout();
}
