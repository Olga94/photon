package com.example.photon.mvp.models;

import com.example.photon.data.network.res.AddResponse;
import com.example.photon.data.storage.dto.OwnerDto;
import com.example.photon.data.storage.dto.PhotoCardDto;

import okhttp3.ResponseBody;
import retrofit2.Response;
import rx.Observable;

public class DetailsModel extends AbstractModel {

    public Observable<OwnerDto> getUser(String id) {
        return mDataManager.getDataOwner(id);
    }

    public boolean getFavorite(String id) {
        return mDataManager.isFavorite(id);
    }

    public Observable<AddResponse> addToFavorite(PhotoCardDto photoCard) {
        return mDataManager.addToFavorite(photoCard);
    }

    public Observable<ResponseBody> deleteFromFavorite(String id) {
        return mDataManager.removeFromFavorite(id);
    }

    public boolean isAuth() {
        return mDataManager.isAuth();
    }
}
