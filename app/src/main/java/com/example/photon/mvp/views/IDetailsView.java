package com.example.photon.mvp.views;

import android.view.View;

import com.example.photon.data.network.res.OwnerRes;
import com.example.photon.data.storage.dto.OwnerDto;
import com.example.photon.data.storage.dto.PhotoCardDto;
import com.example.photon.data.storage.realm.PhotoCardRealm;

public interface IDetailsView extends IView{

    void initView(PhotoCardDto photoCard);
    void initIsFavorite(boolean isFavorite);
    void initOwner(OwnerDto owner);
    void showPopupMenu(View view);
    void dismissPopupMenu();
}
