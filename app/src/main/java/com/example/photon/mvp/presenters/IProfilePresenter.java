package com.example.photon.mvp.presenters;

import android.support.v4.app.FragmentManager;

import com.example.photon.data.storage.dto.AlbumDto;

public interface IProfilePresenter {
    void onClickSignIn(String email, String pass);
    void onClickSignUp(String name, String login, String email, String pass);
    void onClickAdd(String name, String description);
    void onClickEdit(String name, String login);
    FragmentManager getFragManager();
    void logout();
    void editUser();
    void openAlbum(AlbumDto album);
    void takePhoto();
    void chooseGallery();
    void chooseCamera();
}
