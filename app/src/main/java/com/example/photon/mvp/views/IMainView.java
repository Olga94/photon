package com.example.photon.mvp.views;

import android.view.View;

public interface IMainView extends IView{
    void initView();
    void showPopupMenu(View v);
    void dismissPopupMenu();
    void showSnackBar(int color, String message);
    void dismissSnackBar();
}
