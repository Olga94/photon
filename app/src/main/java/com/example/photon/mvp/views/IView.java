package com.example.photon.mvp.views;

public interface IView {
    boolean viewOnBackPressed();
}
