package com.example.photon.mvp.presenters;

import android.support.v4.app.FragmentManager;

import com.example.photon.data.storage.dto.PhotoCardDto;
import com.example.photon.data.storage.model.Filter;
import com.example.photon.data.storage.model.Search;

public interface IMainPresenter {
    void onClickPhotoCard(PhotoCardDto photoCard);
    boolean isAuth();
    FragmentManager getFragManager();
    void signIn(String email, String pass);
    void logout();
    void signUp(String name, String login, String email, String pass);
    void clearSearch();
}
