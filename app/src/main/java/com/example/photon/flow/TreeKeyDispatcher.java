package com.example.photon.flow;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.animation.FastOutLinearInInterpolator;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;

import com.example.photon.R;
import com.example.photon.mortar.ScreenScoper;
import com.example.photon.ui.screens.splash.SplashScreen;
import com.example.photon.ui.screens.splash.SplashView;
import com.example.photon.utils.ViewHelper;

import java.util.Collections;
import java.util.Map;

import javax.annotation.Nullable;

import flow.Direction;
import flow.Dispatcher;
import flow.KeyChanger;
import flow.State;
import flow.Traversal;
import flow.TraversalCallback;
import flow.TreeKey;

public class TreeKeyDispatcher implements Dispatcher, KeyChanger {

    public static final String TAG = "TreeKeyDispatcher";

    Activity mActivity;
    private Object inKey;

    @Nullable
    private Object outKey;

    private FrameLayout mRootFrame;

    public TreeKeyDispatcher(Activity activity) {
        mActivity = activity;
    }


    @Override
    public void dispatch(Traversal traversal, TraversalCallback callback) {
        Map<Object, Context> contexts;
        State inState = traversal.getState(traversal.destination.top());
        inKey = inState.getKey();
        State outState = traversal.origin == null ? null : traversal.getState(traversal.origin.top());
        outKey = outState == null ? null : outState.getKey();

        mRootFrame = (FrameLayout) mActivity.findViewById(R.id.root_container);

        if (inKey.equals(outKey)) {
            callback.onTraversalCompleted();
            return;
        }

        Context flowContext = traversal.createContext(inKey, mActivity);
        Context mortarContext = ScreenScoper.getScreenScope((AbstractScreen) inKey).createContext(flowContext);
        contexts = Collections.singletonMap(inKey, mortarContext);

        changeKey(outState, inState, traversal.direction, contexts, callback);
    }

    @Override
    public void changeKey(@Nullable State outgoingState, State incomingState, Direction direction, Map<Object,
            Context> incomingContexts, TraversalCallback callback) {

        Context context = incomingContexts.get(inKey);

        if (outgoingState != null) {
            outgoingState.save(mRootFrame.getChildAt(0));
        }

        Screen screen;
        screen = inKey.getClass().getAnnotation(Screen.class);

        if (screen == null) {
            throw new IllegalStateException("@Screen annotation is missing 2: screen " + ((AbstractScreen) inKey).getScopeName());
        } else {

            int layout = screen.value();

            LayoutInflater inflater = LayoutInflater.from(context);
            View newView = inflater.inflate(layout, mRootFrame, false);
            View oldView = mRootFrame.getChildAt(0);

            incomingState.restore(newView);

            Log.e(TAG, "changeKey: " + mRootFrame);
            if (outKey != null && !(inKey instanceof TreeKey)) {
                ((AbstractScreen) outKey).unregisterScope();
            }

            mRootFrame.addView(newView);

            ViewHelper.waitForMeasure(newView, (view, width, height) -> runAnimation(mRootFrame, oldView, newView, direction, callback));
        }
    }

    private void runAnimation(FrameLayout container, View from, View to, Direction direction, TraversalCallback traversalCallback) {
        Animator animator = createAnimation(from, to, direction); //создаем анимацию
        animator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                if (from != null) {
                    container.removeView(from);
                }
                traversalCallback.onTraversalCompleted();
            }
        });
        animator.setInterpolator(new FastOutLinearInInterpolator());
        animator.start();
    }

    @NonNull
    private Animator createAnimation(@Nullable View from, View to, Direction direction) {
        int fromTranslation, toTranslation;
        boolean backward = direction == Direction.BACKWARD;

        AnimatorSet set = new AnimatorSet();

        if (from != null) {
            fromTranslation = backward ? from.getWidth() : -from.getWidth();
            final ObjectAnimator outAnimation = ObjectAnimator.ofFloat(from, "translationX", fromTranslation);
            set.play(outAnimation);
        }
        toTranslation = backward ? -to.getWidth() : to.getWidth();
        final ObjectAnimator inAnimation = ObjectAnimator.ofFloat(to, "translationX", toTranslation, 0);
        set.play(inAnimation);

        return set;
    }
}
