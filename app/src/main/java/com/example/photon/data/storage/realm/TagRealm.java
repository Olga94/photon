package com.example.photon.data.storage.realm;

import java.io.Serializable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class TagRealm extends RealmObject implements Serializable {

    @PrimaryKey
    private String value;

    public TagRealm() {
    }

    public TagRealm(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
