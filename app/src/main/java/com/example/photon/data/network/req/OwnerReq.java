package com.example.photon.data.network.req;

public class OwnerReq {

    private String name;
    private String login;
    private String avatar;

    public OwnerReq() {
    }

    public OwnerReq(String name, String login, String avatar) {
        this.name = name;
        this.login = login;
        this.avatar = avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
}
