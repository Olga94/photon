package com.example.photon.data.managers;

import com.example.photon.data.network.RestCallLastTransformer;
import com.example.photon.data.network.RestCallTransformer;
import com.example.photon.data.network.RestService;
import com.example.photon.data.network.req.AlbumReq;
import com.example.photon.data.network.req.LoginReq;
import com.example.photon.data.network.req.OwnerReq;
import com.example.photon.data.network.req.PhotoCardReq;
import com.example.photon.data.network.req.RegistrationReq;
import com.example.photon.data.network.res.AddResponse;
import com.example.photon.data.network.res.ImageRes;
import com.example.photon.data.network.res.OwnerRes;
import com.example.photon.data.network.res.PhotoCardRes;
import com.example.photon.data.network.res.CreateResponse;
import com.example.photon.data.network.res.UserEditRes;
import com.example.photon.data.storage.dto.OwnerDto;
import com.example.photon.data.storage.dto.PhotoCardDto;
import com.example.photon.data.storage.dto.TagDto;
import com.example.photon.data.storage.model.Filter;
import com.example.photon.data.storage.model.Search;
import com.example.photon.data.storage.realm.AlbumRealm;
import com.example.photon.di.DaggerService;
import com.example.photon.di.components.DaggerDataManagerComponent;
import com.example.photon.di.components.DataManagerComponent;
import com.example.photon.di.modules.LocalModule;
import com.example.photon.di.modules.NetworkModule;
import com.example.photon.utils.App;

import java.util.List;

import javax.inject.Inject;

import io.realm.RealmList;
import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Retrofit;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class DataManager {

    private static DataManager outInstance = null;
    private final RestCallTransformer mRestCallTransformer;
    private final RestCallLastTransformer mRestCallLastTransformer;

    @Inject
    PreferencesManager mPreferencesManager;
    @Inject
    RestService mRestService;
    @Inject
    Retrofit mRetrofit;
    @Inject
    RealmManager mRealmManager;

    public DataManager() {
        DataManagerComponent dataManagerComponent = DaggerService.getComponent(DaggerDataManagerComponent.class);
        if (dataManagerComponent == null) {
            dataManagerComponent = DaggerDataManagerComponent.builder()
                    .appComponent(App.getAppComponent())
                    .localModule(new LocalModule())
                    .networkModule(new NetworkModule())
                    .build();
            DaggerService.registerComponent(DataManagerComponent.class, dataManagerComponent);
        }
        dataManagerComponent.inject(this);
        mRestCallTransformer = new RestCallTransformer<>();
        mRestCallLastTransformer = new RestCallLastTransformer<>();
    }


    public static DataManager getInstance() {
        if (outInstance == null) {
            outInstance = new DataManager();
        }
        return outInstance;
    }

    public PreferencesManager getPreferencesManager() {
        return mPreferencesManager;
    }

    public RealmManager getRealmManager() {
        return mRealmManager;
    }

    //region ======================== PhotoCard ========================
    public Observable<PhotoCardRes> getPhotoCardsObsFromNetwork() {
        return mRestService.getPhotoCardsResObs(mPreferencesManager.getLastPhotoCardUpdate())
                .compose(((RestCallLastTransformer<List<PhotoCardRes>>) mRestCallLastTransformer))
                .flatMap(Observable::from)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(photoCardRes -> {
                    if (!photoCardRes.isActive())
                        mRealmManager.deletePhotoCard(photoCardRes.getId());
                })
                .filter(PhotoCardRes::isActive)
                .doOnNext(photoCardRes -> mRealmManager.savePhotoCardsToRealm(photoCardRes));
    }

    public Observable<PhotoCardDto> getAllPhotoCardsFromDB() {
        return mRealmManager.getAllPhotoCardsFromRealm(mPreferencesManager.getUserId())
                .map(PhotoCardDto::new);
    }

    public Observable<PhotoCardDto> getSearchPhotoCardsFromDB(Search search) {
        return mRealmManager.getSearchPhotoCardsFromReal(mPreferencesManager.getUserId(),
                search).map(PhotoCardDto::new);
    }

    public Observable<PhotoCardDto> getFilterPhotoCardsFromDB(Filter filters) {
        return mRealmManager.getFilterPhotoCardsFromReal(mPreferencesManager.getUserId(),
                filters).map(PhotoCardDto::new);
    }

    public Observable<CreateResponse> sendPhotoCard(MultipartBody.Part body, PhotoCardReq photoCardReq) {
        return mRestService.uploadImage(mPreferencesManager.getToken(),
                mPreferencesManager.getUserId(), body)
                .flatMap(imageRes -> {
                    photoCardReq.setPhoto(imageRes.getImage());
                    return mRestService.addPhotoCard(mPreferencesManager.getToken(),
                            mPreferencesManager.getUserId(),
                            photoCardReq);
                });
    }
    //endregion

    public Observable<TagDto> getTagsFromDb() {
        return mRealmManager.getTags()
                .map(TagDto::new);
    }

    public Observable<String> getHistory() {
        return mRealmManager.getHistory();
    }

    public Observable<List<String>> getTags() {
        return mRestService.getTags()
                .compose(((RestCallTransformer<List<String>>) mRestCallTransformer))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(listResponse -> mRealmManager.saveTags(listResponse));
    }

    public Observable<OwnerRes> getOwnerObsFromNetwork(String userId) {
        return mRestService.getOwner(userId)
                .compose(((RestCallTransformer<OwnerRes>) mRestCallTransformer))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());

    }

    //region ======================== User ========================

    public Observable<OwnerDto> getDataOwner(String id) {
        if (id.equals(mPreferencesManager.getUserId()))
            return mRealmManager.getUserFromDb()
                    .map(OwnerDto::new);
        else
            return getOwnerObsFromNetwork(id)
                    .map(OwnerDto::new);
    }

    public Observable<OwnerRes> signInUser(LoginReq loginReq) {
        return mRestService.signIn(mPreferencesManager.getLastUserUpdate(), loginReq)
                .compose(((RestCallLastTransformer<OwnerRes>) mRestCallLastTransformer))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(ownerRes -> {
                    mPreferencesManager.saveToken(ownerRes.getToken());
                    mPreferencesManager.saveUserId(ownerRes.getId());
                    mRealmManager.saveUser(ownerRes);
                });
    }

    public Observable<OwnerRes> signUpUser(RegistrationReq registrationReq) {
        return mRestService.signUp(mPreferencesManager.getLastUserUpdate(), registrationReq)
                .compose(((RestCallLastTransformer<OwnerRes>) mRestCallLastTransformer))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(ownerRes -> {
                    mPreferencesManager.saveToken(ownerRes.getToken());
                    mPreferencesManager.saveUserId(ownerRes.getId());
                    mRealmManager.saveUser(ownerRes);
                });
    }

    public boolean isAuth() {
        return !mPreferencesManager.getToken().isEmpty();
    }

    public void removeToken() {
        mRealmManager.removeUser(mPreferencesManager.getUserId());
        mPreferencesManager.saveToken("");
        mPreferencesManager.saveUserId("");
    }

    public Observable<UserEditRes> uploadAvatar(MultipartBody.Part body) {
        return mRestService.uploadImage(mPreferencesManager.getToken(),
                mPreferencesManager.getUserId(), body)
                .flatMap(imageRes -> {
                    OwnerReq ownerReq = new OwnerReq();
                    ownerReq.setAvatar(imageRes.getImage());
                    return mRestService.editUser(mPreferencesManager.getToken(),
                            mPreferencesManager.getUserId(), ownerReq);
                });
    }

    public Observable<UserEditRes> updateUserData(OwnerReq owner) {
        return mRestService.editUser(mPreferencesManager.getToken(),
                mPreferencesManager.getUserId(), owner)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(userEditRes -> mRealmManager.updateUser(userEditRes, mPreferencesManager.getUserId()));
    }
    //endregion

    //region ======================== album ========================

    public Observable<CreateResponse> sendAlbum(AlbumReq album) {
        album.setOwner(mPreferencesManager.getUserId());
        return mRestService.saveAlbum(mPreferencesManager.getToken(),
                mPreferencesManager.getUserId(), album);
    }

    public AlbumRealm getAlbumFromDb(String id) {
        return mRealmManager.getAlbum(id);
    }

    public Observable<ResponseBody> removeAlbum(String id) {
        return mRestService.deleteAlbum(mPreferencesManager.getToken(),
                mPreferencesManager.getUserId(), id)
                .compose(((RestCallTransformer<ResponseBody>) mRestCallTransformer))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(responseBody -> mRealmManager.deleteAlbum(id));
    }

    public Observable<CreateResponse> updateAlbum(String id, AlbumReq albumReq) {
        return mRestService.editAlbum(mPreferencesManager.getToken(),
                mPreferencesManager.getUserId(), id, albumReq)
                .compose(((RestCallTransformer<CreateResponse>) mRestCallTransformer))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(createResponse -> mRealmManager.updateAlbum(id, albumReq));
    }

    public List<AlbumRealm> getAllAlbums() {
        return mRealmManager.getAllAlbums();
    }

    public String getUserId() {
        return mPreferencesManager.getUserId();
    }

    public boolean isFavorite(String id) {
        if (isAuth())
            return mRealmManager.isFavorite(id);
        else
            return false;
    }

    public Observable<AddResponse> addToFavorite(PhotoCardDto photoCard) {
        return mRestService.addToFavorite(mPreferencesManager.getToken(),
                mPreferencesManager.getUserId(), photoCard.getId())
                .compose((RestCallTransformer<AddResponse>) mRestCallTransformer)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(addResponse -> {
                    if (addResponse.isSuccess())
                        mRealmManager.addToFavorite(photoCard);
                });
    }

    public void saveHistory(String history) {
        mRealmManager.saveHistory(history);
    }

    public Observable<ResponseBody> removeFromFavorite(String id) {
        return mRestService.deleteFromFavorite(mPreferencesManager.getToken(),
                mPreferencesManager.getUserId(), id)
                .compose(((RestCallTransformer<ResponseBody>) mRestCallTransformer))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public boolean isUser(String id) {
        return mPreferencesManager.getUserId().equals(id);
    }

    public Observable<AddResponse> addViewToPhotoCard(String id) {
        return mRestService.addView(id)
                .compose(((RestCallTransformer<AddResponse>) mRestCallTransformer))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(addResponse -> {
                    if (addResponse.isSuccess())
                        mRealmManager.saveView(id);
                });
    }


    //endregion
}
