package com.example.photon.data.storage.dto;

import com.example.photon.data.network.res.AlbumRes;
import com.example.photon.data.network.res.OwnerRes;
import com.example.photon.data.storage.realm.AlbumRealm;
import com.example.photon.data.storage.realm.OwnerRealm;

import java.util.ArrayList;
import java.util.List;

public class OwnerDto {

    private String name;
    private String login;
    private String avatar;
    private int albumCount;
    private int photocardCount;
    private List<AlbumDto> albums = new ArrayList<>();

    public OwnerDto(OwnerRealm owner) {
        this.name = owner.getName();
        this.login = owner.getLogin();
        this.avatar = owner.getAvatar();
        this.albumCount = owner.getAlbumCount();
        this.photocardCount = owner.getPhotoCardCount();
        for (AlbumRealm album : owner.getAlbums()) {
            albums.add(new AlbumDto(album));
        }
    }

    public OwnerDto(OwnerRes owner) {
        this.name = owner.getName();
        this.login = owner.getLogin();
        this.avatar = owner.getAvatar();
        this.albumCount = owner.getAlbumCount();
        this.photocardCount = owner.getPhotocardCount();
        for (AlbumRes album : owner.getAlbums()) {
            albums.add(new AlbumDto(album));
        }
    }

    public String getName() {
        return name;
    }

    public String getLogin() {
        return login;
    }

    public String getAvatar() {
        return avatar;
    }

    public int getAlbumCount() {
        return albumCount;
    }

    public int getPhotocardCount() {
        return photocardCount;
    }

    public List<AlbumDto> getAlbums() {
        return albums;
    }
}
