package com.example.photon.data.storage.model;

import java.util.ArrayList;
import java.util.List;

public class Search {

    private String mKeyword;
    private List<String> mTags = new ArrayList<>();

    public String getKeyword() {
        return mKeyword;
    }

    public void setKeyword(String keyword) {
        mKeyword = keyword;
    }

    public List<String> getTags() {
        return mTags;
    }

    public void setTags(List<String> tags) {
        mTags = tags;
    }

    public void addTag(String tag) {
        mTags.add(tag);
    }
}
