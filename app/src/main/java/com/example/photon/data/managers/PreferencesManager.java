package com.example.photon.data.managers;

import android.content.Context;
import android.content.SharedPreferences;

import com.squareup.moshi.Moshi;

public class PreferencesManager {

    private static final String PHOTO_CARD_LAST_UPDATE_KEY = "PRODUCT_LAST_UPDATE_KEY";
    private static final String USER_LAST_UPDATE_KEY = "PRODUCT_LAST_UPDATE_KEY";
    private static final String TOKEN_USER_KEY = "TOKEN_USER_KEY";
    private static final String USER_ID_KEY = "USER_ID_KEY";
    private static final String DEFAULT_LAST_UPDATE = "Thu Jan 1 1970 00:00:00 GMT+0000 (UTC)";
    private SharedPreferences mSharedPreferences;

    public PreferencesManager(Context context) {
        mSharedPreferences = context.getSharedPreferences("photon", Context.MODE_PRIVATE);
    }

    //region ======================== Entity Last-Modified updated ========================
    public String getLastPhotoCardUpdate() {
        return mSharedPreferences.getString(PHOTO_CARD_LAST_UPDATE_KEY, DEFAULT_LAST_UPDATE);
    }

    public void saveLastPhotoCardUpdate(String lastModified) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(PHOTO_CARD_LAST_UPDATE_KEY, lastModified);
        editor.apply();
    }

    public String getLastUserUpdate() {
        return mSharedPreferences.getString(USER_LAST_UPDATE_KEY, DEFAULT_LAST_UPDATE);
    }

    public void saveLastUserUpdate(String lastModified) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(USER_LAST_UPDATE_KEY, lastModified);
        editor.apply();
    }
    //endregion

    //region ======================== Data user ========================
    public void saveToken(String token) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(TOKEN_USER_KEY, token);
        editor.apply();
    }

    public void saveUserId(String id) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(USER_ID_KEY, id);
        editor.apply();
    }

    public String getToken() {
        return mSharedPreferences.getString(TOKEN_USER_KEY, "");
    }

    public String getUserId() {
        return mSharedPreferences.getString(USER_ID_KEY, "");
    }
    //endregion
}
