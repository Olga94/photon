package com.example.photon.data.storage.realm;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class History extends RealmObject{

    @PrimaryKey
    private String text;

    public History() {
    }

    public History(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
