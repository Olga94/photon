package com.example.photon.data.storage.realm;

import com.example.photon.data.network.res.PhotoCardRes;
import com.example.photon.data.storage.dto.PhotoCardDto;

import java.io.Serializable;
import java.util.Date;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class PhotoCardRealm extends RealmObject implements Serializable {

    @PrimaryKey
    private String id;
    private String owner;
    private String title;
    private String photo;
    private int views;
    private int favorits;
    private Date created;
    private RealmList<TagRealm> tags;
    private FiltersRealm filters;

    public PhotoCardRealm() {
    }

    public PhotoCardRealm(PhotoCardRes photoCardRes) {
        this.id = photoCardRes.getId();
        this.owner = photoCardRes.getOwner();
        this.title = photoCardRes.getTitle();
        this.photo = photoCardRes.getPhoto();
        this.views = photoCardRes.getViews();
        this.favorits = photoCardRes.getFavorits();
        this.created = photoCardRes.getCreated();
        this.tags = new RealmList<>();
        for (int i = 0; i < photoCardRes.getTags().size(); i++) {
            this.tags.add(new TagRealm(photoCardRes.getTags().get(i)));
        }
        filters = new FiltersRealm(photoCardRes.getFilters());
    }

    public PhotoCardRealm(PhotoCardDto photoCardDto) {
        this.id = photoCardDto.getId();
        this.owner = photoCardDto.getOwner();
        this.title = photoCardDto.getTitle();
        this.photo = photoCardDto.getPhoto();
        this.views = photoCardDto.getViews();
        this.favorits = photoCardDto.getFavorits();
        this.tags = new RealmList<>();
        for (int i = 0; i < photoCardDto.getTags().size(); i++) {
            this.tags.add(new TagRealm(photoCardDto.getTags().get(i)));
        }
        filters = new FiltersRealm(photoCardDto.getFilters());
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPhoto() {
        return photo;
    }

    public int getViews() {
        return views;
    }

    public void addViews() {
        views++;
    }

    public void setViews(int views) {
        this.views = views;
    }

    public int getFavorits() {
        return favorits;
    }

    public RealmList<TagRealm> getTags() {
        return tags;
    }

    public void setTags(RealmList<TagRealm> tags) {
        this.tags = tags;
    }

    public FiltersRealm getFilters() {
        return filters;
    }

    public void setFilters(FiltersRealm filters) {
        this.filters = filters;
    }
}
