package com.example.photon.data.network.res;

import com.example.photon.data.storage.dto.FiltersDto;

import java.util.Date;
import java.util.List;

public class PhotoCardJson {
    public String id;
    public String owner;
    public String title;
    public String photo;
    public String created;
    public int views;
    public int favorits;
    public boolean active;
    public List<String> tags;
    public FiltersDto filters;
}
