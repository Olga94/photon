package com.example.photon.data.storage.realm;

import com.example.photon.data.network.req.AlbumReq;
import com.example.photon.data.network.res.AlbumRes;
import com.example.photon.data.network.res.PhotoCardRes;
import com.example.photon.data.storage.dto.AlbumDto;
import com.example.photon.data.storage.dto.PhotoCardDto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class AlbumRealm extends RealmObject implements Serializable {

    @PrimaryKey
    private String id;
    private String owner;
    private String title;
    private String description;
    private boolean isFavorite;
    private int views;
    private int favorits;
    private RealmList<PhotoCardRealm> photocards = new RealmList<>();

    public AlbumRealm() {
    }

    public AlbumRealm(String remoteId, AlbumReq album) {
        this.id = remoteId;
        this.owner = album.getOwner();
        this.title = album.getTitle();
        this.description = album.getDescription();
    }

    public AlbumRealm(AlbumRes album) {
        this.id = album.getId();
        this.owner = album.getOwner();
        this.title = album.getTitle();
        this.description = album.getDescription();
        this.isFavorite = album.isFavorite();
        this.views = album.getViews();
        this.favorits = album.getFavorits();
        photocards = new RealmList<>();
        for (PhotoCardRes photoCardRes : album.getPhotocards()) {
            if (photoCardRes.isActive())
                photocards.add(new PhotoCardRealm(photoCardRes));
        }
    }

    public AlbumRealm(String name, String description, String owner) {
        this.title = name;
        this.description = description;
        this.owner = owner;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public Integer getViews() {
        return views;
    }

    public void setViews(Integer views) {
        this.views = views;
    }

    public Integer getFavorits() {
        return favorits;
    }

    public RealmList<PhotoCardRealm> getPhotocards() {
        return photocards;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void addPhotoCard(PhotoCardDto photoCardDto) {
        photocards.add(new PhotoCardRealm(photoCardDto));
    }

    public boolean isFavorite() {
        return isFavorite;
    }
}
