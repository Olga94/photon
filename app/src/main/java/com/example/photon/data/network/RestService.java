package com.example.photon.data.network;

import com.example.photon.data.network.req.AlbumReq;
import com.example.photon.data.network.req.LoginReq;
import com.example.photon.data.network.req.OwnerReq;
import com.example.photon.data.network.req.PhotoCardReq;
import com.example.photon.data.network.req.RegistrationReq;
import com.example.photon.data.network.res.AddResponse;
import com.example.photon.data.network.res.CreateResponse;
import com.example.photon.data.network.res.ImageRes;
import com.example.photon.data.network.res.OwnerRes;
import com.example.photon.data.network.res.PhotoCardRes;
import com.example.photon.data.network.res.UserEditRes;
import com.example.photon.utils.ConstantManager;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

public interface RestService {

    @GET("photocard/list")
    Observable<Response<List<PhotoCardRes>>> getPhotoCardsResObs(
            @Header(ConstantManager.IF_MODIFIED_SINCE_HEADER) String lastEntityUpdate);

    @GET("photocard/tags")
    Observable<Response<List<String>>> getTags();

    @GET("user/{userId}")
    Observable<Response<OwnerRes>> getOwner(@Path("userId") String userId);

    @POST("user/signUp")
    Observable<Response<OwnerRes>> signUp(@Header(ConstantManager.IF_MODIFIED_SINCE_HEADER) String lastEntityUpdate,
                                          @Body RegistrationReq registrationReq);

    @POST("user/onClickSignIn")
    Observable<Response<OwnerRes>> signIn(@Header(ConstantManager.IF_MODIFIED_SINCE_HEADER) String lastEntityUpdate,
                                          @Body LoginReq loginReq);

    @Multipart
    @POST("user/{userId}/image/upload")
    Observable<ImageRes> uploadImage(
            @Header(ConstantManager.AUTHORIZATION_HEADER) String token,
            @Path("userId") String userId,
            @Part MultipartBody.Part file);

    @PUT("user/{userId}")
    Observable<UserEditRes> editUser(
            @Header(ConstantManager.AUTHORIZATION_HEADER) String token,
            @Path("userId") String userId,
            @Body OwnerReq owner);

    @POST("user/{userId}/album")
    Observable<CreateResponse> saveAlbum(
            @Header(ConstantManager.AUTHORIZATION_HEADER) String token,
            @Path("userId") String userId,
            @Body AlbumReq album);

    @DELETE("user/{userId}/album/{id}")
    Observable<Response<ResponseBody>> deleteAlbum(
            @Header(ConstantManager.AUTHORIZATION_HEADER) String token,
            @Path("userId") String userId,
            @Path("id") String id);

    @PUT("user/{userId}/album/{id}")
    Observable<Response<CreateResponse>> editAlbum(
            @Header(ConstantManager.AUTHORIZATION_HEADER) String token,
            @Path("userId") String userId,
            @Path("id") String id,
            @Body AlbumReq album);

    @POST("user/{userId}/photocard")
    Observable<CreateResponse> addPhotoCard(
            @Header(ConstantManager.AUTHORIZATION_HEADER) String token,
            @Path("userId") String userId,
            @Body PhotoCardReq photoCardReq);

    @POST("user/{userId}/favorite/{photocardId}")
    Observable<Response<AddResponse>> addToFavorite(
            @Header(ConstantManager.AUTHORIZATION_HEADER) String token,
            @Path("userId") String userId,
            @Path("photocardId") String photocardId);

    @DELETE("user/{userId}/favorite/{photocardId}")
    Observable<Response<ResponseBody>> deleteFromFavorite(
            @Header(ConstantManager.AUTHORIZATION_HEADER) String token,
            @Path("userId") String userId,
            @Path("photocardId") String photocardId);

    @POST("photocard/{photocardId}/view")
    Observable<Response<AddResponse>> addView(@Path("photocardId") String photocardId);
}
