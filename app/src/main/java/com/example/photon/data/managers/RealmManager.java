package com.example.photon.data.managers;

import com.example.photon.data.network.req.AlbumReq;
import com.example.photon.data.network.res.CreateResponse;
import com.example.photon.data.network.res.OwnerRes;
import com.example.photon.data.network.res.PhotoCardRes;
import com.example.photon.data.network.res.UserEditRes;
import com.example.photon.data.storage.dto.PhotoCardDto;
import com.example.photon.data.storage.model.Filter;
import com.example.photon.data.storage.model.Search;
import com.example.photon.data.storage.realm.AlbumRealm;
import com.example.photon.data.storage.realm.History;
import com.example.photon.data.storage.realm.OwnerRealm;
import com.example.photon.data.storage.realm.PhotoCardRealm;
import com.example.photon.data.storage.realm.TagRealm;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.Sort;
import retrofit2.Response;
import rx.Observable;

public class RealmManager {

    private Realm mRealmInstance;

    public void savePhotoCardsToRealm(PhotoCardRes photoCard) {
        Realm realm = Realm.getDefaultInstance();
        PhotoCardRealm photoCardRealm = new PhotoCardRealm(photoCard);
        realm.executeTransaction(realm1 -> realm1.insertOrUpdate(photoCardRealm));
        realm.close();
    }

    public Observable<PhotoCardRealm> getAllPhotoCardsFromRealm(String id) {
        String[] fieldNames = {"created", "views"};
        Sort sort[] = {Sort.ASCENDING, Sort.ASCENDING};
        return getQueryRealmInstance()
                .where(PhotoCardRealm.class)
                .notEqualTo("owner", id)
                .findAllSorted(fieldNames, sort)
                .asObservable()
                .filter(RealmResults::isLoaded)
                .flatMap(Observable::from);
    }

    public Observable<PhotoCardRealm> getSearchPhotoCardsFromReal(String userId, Search search) {
        String[] fieldNames = {"created", "views"};
        Sort[] sort = {Sort.ASCENDING, Sort.ASCENDING};

        RealmQuery<PhotoCardRealm> photoCards = getQueryRealmInstance()
                .where(PhotoCardRealm.class)
                .notEqualTo("owner", userId)
                .contains("title", search.getKeyword());
        if (search.getTags().size() != 0) {
            String[] tags = new String[search.getTags().size()];
            photoCards.in("tags.value", search.getTags().toArray(tags));
        }

        return photoCards
                .findAllSorted(fieldNames, sort)
                .asObservable()
                .filter(RealmResults::isLoaded)
                .flatMap(Observable::from);
    }

    public Observable<PhotoCardRealm> getFilterPhotoCardsFromReal(String userId, Filter filter) {
        String[] fieldNames = {"created", "views"};
        Sort[] sort = {Sort.ASCENDING, Sort.ASCENDING};
        RealmQuery<PhotoCardRealm> photoCards = getQueryRealmInstance()
                .where(PhotoCardRealm.class)
                .notEqualTo("owner", userId);
        if (filter.getDishes().size() != 0) {
            String[] dishes = new String[filter.getDishes().size()];
            photoCards.in("filters.dish", filter.getDishes().toArray(dishes));
        }
        if (filter.getColors().size() != 0) {
            for (int i = 0; i < filter.getColors().size(); i++) {
                photoCards.contains("filters.nuances", filter.getColors().get(i));
            }
        }
        if (filter.getDecors().size() != 0) {
            String[] decors = new String[filter.getDecors().size()];
            photoCards.in("filters.decor", filter.getDecors().toArray(decors));
        }
        if (filter.getTemperatures().size() != 0) {
            String[] temperatures = new String[filter.getTemperatures().size()];
            photoCards.in("filters.temperature", filter.getTemperatures().toArray(temperatures));
        }
        if (filter.getLights().size() != 0) {
            String[] lights = new String[filter.getLights().size()];
            photoCards.in("filters.light", filter.getLights().toArray(lights));
        }
        if (filter.getLightDirs().size() != 0) {
            String[] lightDirs = new String[filter.getLightDirs().size()];
            photoCards.in("filters.lightDirection", filter.getLightDirs().toArray(lightDirs));
        }
        if (filter.getLightCount().size() != 0) {
            String[] lightCount = new String[filter.getLightCount().size()];
            photoCards.in("filters.lightSource", filter.getLightCount().toArray(lightCount));
        }

        return photoCards
                .findAllSorted(fieldNames, sort)
                .asObservable()
                .filter(RealmResults::isLoaded)
                .flatMap(Observable::from);
    }

    public void saveUser(OwnerRes ownerRes) {
        Realm realm = Realm.getDefaultInstance();
        OwnerRealm ownerRealm = new OwnerRealm(ownerRes);
        OwnerRealm entry = realm.where(OwnerRealm.class).equalTo("id", ownerRes.getId()).findFirst();
        if (entry != null)
            ownerRealm.setId(entry.getId());
        realm.executeTransaction(realm1 -> realm1.insertOrUpdate(ownerRealm));
        realm.close();
    }

    public void saveTags(List<String> listResponse) {
        Realm realm = Realm.getDefaultInstance();
        RealmList<TagRealm> tagRealms = new RealmList<>();
        for (int i = 0; i < listResponse.size(); i++) {
            tagRealms.add(new TagRealm(listResponse.get(i)));
        }
        realm.executeTransaction(realm1 -> realm1.insertOrUpdate(tagRealms));
        realm.close();
    }

    public Observable<OwnerRealm> getUserFromDb() {
        return getQueryRealmInstance()
                .where(OwnerRealm.class)
                .findFirst()
                .asObservable();
    }

    public void updateUser(UserEditRes userEditRes, String id) {
        Realm realm = Realm.getDefaultInstance();
        OwnerRealm entry = realm.where(OwnerRealm.class).equalTo("id", id).findFirst();
        realm.executeTransaction(realm1 -> {
            entry.setName(userEditRes.getName());
            entry.setLogin(userEditRes.getLogin());
            entry.setAvatar(userEditRes.getAvatar());
        });
        realm.close();
    }

    public void saveAlbum(CreateResponse createResponse, AlbumReq album) {
        Realm realm = Realm.getDefaultInstance();
        AlbumRealm albumRealm = new AlbumRealm(createResponse.getId(), album);
        realm.executeTransaction(realm1 -> realm1.insertOrUpdate(albumRealm));
        realm.close();
    }

    public AlbumRealm getAlbum(String id) {
        return getQueryRealmInstance()
                .where(AlbumRealm.class)
                .equalTo("id", id)
                .findFirst();
    }

    public void deleteAlbum(String id) {
        Realm realm = Realm.getDefaultInstance();
        AlbumRealm album = realm.where(AlbumRealm.class).equalTo("id", id).findFirst();
        realm.executeTransaction(realm1 -> album.deleteFromRealm());
        realm.close();
    }

    public void updateAlbum(String id, AlbumReq albumReq) {
        Realm realm = Realm.getDefaultInstance();
        AlbumRealm entry = realm.where(AlbumRealm.class).equalTo("id", id).findFirst();
        realm.executeTransaction(realm1 -> {
            entry.setTitle(albumReq.getTitle());
            entry.setDescription(albumReq.getDescription());
        });
        realm.close();
    }

    public List<AlbumRealm> getAllAlbums() {
        RealmResults<AlbumRealm> albumRealms = getQueryRealmInstance().where(AlbumRealm.class).findAll();
        return new ArrayList<>(albumRealms);
    }


    public void deletePhotoCard(String id) {
        Realm realm = Realm.getDefaultInstance();
        PhotoCardRealm photoCardRealm = realm.where(PhotoCardRealm.class).equalTo("id", id).findFirst();
        realm.executeTransaction(realm1 -> {
            if (photoCardRealm != null) photoCardRealm.deleteFromRealm();
        });
        realm.close();
    }

    public boolean isFavorite(String id) {
        return getQueryRealmInstance()
                .where(AlbumRealm.class)
                .equalTo("isFavorite", true)
                .findFirst()
                .getPhotocards()
                .where()
                .equalTo("id", id)
                .findFirst() != null;
    }

    public void addToFavorite(PhotoCardDto photoCard) {
        Realm realm = Realm.getDefaultInstance();
        AlbumRealm entry = realm.where(AlbumRealm.class).equalTo("isFavorite", true).findFirst();
        realm.executeTransaction(realm1 -> entry.addPhotoCard(photoCard));
        realm.close();
    }

    public Observable<TagRealm> getTags() {
        return getQueryRealmInstance()
                .where(TagRealm.class)
                .findAll()
                .asObservable()
                .filter(RealmResults::isLoaded)
                .flatMap(Observable::from);
    }

    public Observable<String> getHistory() {
        return getQueryRealmInstance()
                .where(History.class)
                .findAll()
                .asObservable()
                .filter(RealmResults::isLoaded)
                .flatMap(Observable::from)
                .map(History::getText);

    }

    public void saveHistory(String text) {
        Realm realm = Realm.getDefaultInstance();
        History history = new History(text);
        realm.executeTransaction(realm1 -> realm1.insertOrUpdate(history));
        realm.close();
    }

    public void saveView(String id) {
        Realm realm = Realm.getDefaultInstance();
        PhotoCardRealm photoCardRealm = realm.where(PhotoCardRealm.class).equalTo("id", id).findFirst();
        realm.executeTransaction(realm1 -> photoCardRealm.addViews());
        realm.close();

    }

    private Realm getQueryRealmInstance() {
        if (mRealmInstance == null || mRealmInstance.isClosed()) {
            mRealmInstance = Realm.getDefaultInstance();
        }
        return mRealmInstance;
    }

    public void removeUser(String userId) {
        Realm realm = Realm.getDefaultInstance();
        OwnerRealm ownerRealm = realm.where(OwnerRealm.class).equalTo("id", userId).findFirst();
        realm.executeTransaction(realm1 -> ownerRealm.deleteFromRealm());
        realm.close();
    }
}
