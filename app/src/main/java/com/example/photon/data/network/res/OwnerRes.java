package com.example.photon.data.network.res;

import com.example.photon.data.storage.realm.OwnerRealm;

import java.util.ArrayList;
import java.util.List;

public class OwnerRes {

    private String id;
    private String name;
    private String login;
    private String avatar;
    private String token;
    private int albumCount;
    private int photocardCount;
    private List<AlbumRes> albums = new ArrayList<>();

    public OwnerRes(OwnerRealm owner) {
        this.name = owner.getName();
        this.login = owner.getLogin();
        this.avatar = owner.getAvatar();
        this.albumCount = owner.getAlbumCount();
        this.photocardCount = owner.getAlbumCount();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getAlbumCount() {
        return albumCount;
    }

    public void setAlbumCount(int albumCount) {
        this.albumCount = albumCount;
    }

    public int getPhotocardCount() {
        return photocardCount;
    }

    public void setPhotocardCount(int photocardCount) {
        this.photocardCount = photocardCount;
    }

    public List<AlbumRes> getAlbums() {
        return albums;
    }

    public void setAlbums(List<AlbumRes> albums) {
        this.albums = albums;
    }
}
