package com.example.photon.data.network;

import com.example.photon.data.managers.DataManager;
import com.example.photon.data.network.error.AccessesError;
import com.example.photon.data.network.error.ErrorUtils;
import com.example.photon.data.network.error.NetworkAvailableError;
import com.example.photon.data.network.res.OwnerRes;
import com.example.photon.utils.ConstantManager;
import com.example.photon.utils.NetworkStatusChecker;
import com.fernandocejas.frodo.annotation.RxLogObservable;

import retrofit2.Response;
import rx.Observable;

public class RestCallLastTransformer<R> implements Observable.Transformer<Response<R>, R> {
    @Override
    @RxLogObservable
    public Observable<R> call(Observable<Response<R>> responseObservable) {
        Observable<Boolean> networkStatus = NetworkStatusChecker.isInternetAvailable();

        return networkStatus
                .flatMap(aBoolean -> aBoolean ? responseObservable : Observable.error(new NetworkAvailableError()))
                .flatMap(rResponse -> {
                    switch (rResponse.code()) {
                        case 200:
                        case 201:
                            String lastModified = rResponse.headers().get(ConstantManager.LAST_MODIFIED_HEADER);
                            if (lastModified != null && rResponse.body() instanceof OwnerRes) {
                                DataManager.getInstance().getPreferencesManager().saveLastUserUpdate(lastModified);
                            } else if (lastModified != null) {
                                DataManager.getInstance().getPreferencesManager().saveLastPhotoCardUpdate(lastModified);
                            }
                            return Observable.just(rResponse.body());
                        case 304:
                            return Observable.empty();
                        case 403:
                            return Observable.error(new AccessesError());
                        default:
                            return Observable.error(ErrorUtils.parseError(rResponse));
                    }
                });
    }
}