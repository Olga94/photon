package com.example.photon.data.network.req;

import com.example.photon.data.storage.realm.AlbumRealm;

import java.io.Serializable;

public class AlbumReq implements Serializable{

    private String owner;
    private String title;
    private String description;

    public AlbumReq(String name, String description) {
        this.title = name;
        this.description = description;
    }

    public AlbumReq(AlbumRealm album) {
        owner = album.getOwner();
        title = album.getTitle();
        description = album.getDescription();
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getOwner() {
        return owner;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }
}
