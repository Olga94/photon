package com.example.photon.data.network.res;

public class UserEditRes {
    private String name;
    private String login;
    private String avatar;

    public String getName() {
        return name;
    }

    public String getLogin() {
        return login;
    }

    public String getAvatar() {
        return avatar;
    }
}
