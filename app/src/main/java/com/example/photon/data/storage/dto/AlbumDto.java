package com.example.photon.data.storage.dto;

import com.example.photon.data.network.res.AlbumRes;
import com.example.photon.data.network.res.PhotoCardRes;
import com.example.photon.data.storage.realm.AlbumRealm;
import com.example.photon.data.storage.realm.PhotoCardRealm;

import java.util.ArrayList;
import java.util.List;

public class AlbumDto {

    private String id;
    private String owner;
    private String title;
    private String description;
    private boolean isFavorite;
    private int views;
    private int favorits;
    private List<PhotoCardDto> photoCards = new ArrayList<>();

    public AlbumDto(String title, String description) {
        this.title = title;
        this.description = description;
    }

    public AlbumDto(AlbumRealm albumRealm) {
        this.id = albumRealm.getId();
        this.owner = albumRealm.getOwner();
        this.title = albumRealm.getTitle();
        this.description = albumRealm.getDescription();
        this.isFavorite = albumRealm.isFavorite();
        this.views = albumRealm.getViews();
        this.favorits = albumRealm.getFavorits();
        for (PhotoCardRealm photoCard : albumRealm.getPhotocards()) {
            photoCards.add(new PhotoCardDto(photoCard));
        }
    }

    public AlbumDto(AlbumRes album) {
        this.owner = album.getOwner();
        this.title = album.getTitle();
        this.description = album.getDescription();
        this.isFavorite = album.isFavorite();
        this.views = album.getViews();
        this.favorits = album.getFavorits();
        for (PhotoCardRes photoCard : album.getPhotocards()) {
            photoCards.add(new PhotoCardDto(photoCard));
        }
    }

    public String getId() {
        return id;
    }

    public String getOwner() {
        return owner;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public int getViews() {
        return views;
    }

    public int getFavorits() {
        return favorits;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public List<PhotoCardDto> getPhotocards() {
        return photoCards;
    }
}
