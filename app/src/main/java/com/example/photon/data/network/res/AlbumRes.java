package com.example.photon.data.network.res;

import java.util.List;

public class AlbumRes {

    private String id;
    private String owner;
    private String title;
    private String description;
    private boolean active;
    private boolean isFavorite;
    private int views;
    private int favorits;
    private List<PhotoCardRes> photocards = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public int getViews() {
        return views;
    }

    public void setViews(int views) {
        this.views = views;
    }

    public int getFavorits() {
        return favorits;
    }

    public List<PhotoCardRes> getPhotocards() {
        return photocards;
    }

    public boolean isActive() {
        return active;
    }
}
