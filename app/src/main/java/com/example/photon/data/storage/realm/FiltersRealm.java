package com.example.photon.data.storage.realm;

import com.example.photon.data.storage.dto.FiltersDto;

import java.io.Serializable;

import io.realm.RealmObject;

/**
 * Created by Ольга on 04.06.2017.
 */

public class FiltersRealm extends RealmObject implements Serializable{

    private String dish;
    private String nuances;
    private String decor;
    private String temperature;
    private String light;
    private String lightDirection;
    private String lightSource;

    public FiltersRealm() {
    }

    public FiltersRealm(FiltersDto filter) {
        this.dish = filter.getDish();
        this.nuances = filter.getNuances();
        this.decor = filter.getDecor();
        this.temperature = filter.getTemperature();
        this.light = filter.getLight();
        this.lightDirection = filter.getLightDirection();
        this.lightSource = filter.getLightSource();
    }

    public String getDish() {
        return dish;
    }

    public void setDish(String dish) {
        this.dish = dish;
    }

    public String getNuances() {
        return nuances;
    }

    public void setNuances(String nuances) {
        this.nuances = nuances;
    }

    public String getDecor() {
        return decor;
    }

    public void setDecor(String decor) {
        this.decor = decor;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public String getLight() {
        return light;
    }

    public void setLight(String light) {
        this.light = light;
    }

    public String getLightDirection() {
        return lightDirection;
    }

    public void setLightDirection(String lightDirection) {
        this.lightDirection = lightDirection;
    }

    public String getLightSource() {
        return lightSource;
    }

    public void setLightSource(String lightSource) {
        this.lightSource = lightSource;
    }
}
