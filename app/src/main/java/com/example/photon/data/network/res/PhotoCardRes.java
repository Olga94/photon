package com.example.photon.data.network.res;

import com.example.photon.data.network.req.PhotoCardReq;
import com.example.photon.data.storage.dto.FiltersDto;

import java.util.Date;
import java.util.List;

public class PhotoCardRes {

    private String id;
    private String owner;
    private String title;
    private String photo;
    private Date created;
    private int views;
    private int favorits;
    private boolean active;
    private List<String> tags;
    private FiltersDto filters;

    public PhotoCardRes(String id, String owner, String title, String photo, Date created, int views,
                        int favorits, boolean active, List<String> tags, FiltersDto filters) {
        this.id = id;
        this.owner = owner;
        this.title = title;
        this.photo = photo;
        this.created = created;
        this.views = views;
        this.favorits = favorits;
        this.active = active;
        this.tags = tags;
        this.filters = filters;
    }

    public PhotoCardRes(PhotoCardReq photoCardReq) {
        title = photoCardReq.getTitle();
        photo = photoCardReq.getPhoto();
        tags = photoCardReq.getTags();
        filters = photoCardReq.getFilters();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPhoto() {
        return photo;
    }

    public int getViews() {
        return views;
    }

    public void setViews(int views) {
        this.views = views;
    }

    public int getFavorits() {
        return favorits;
    }

    public List<String> getTags() {
        return tags;
    }

    public FiltersDto getFilters() {
        return filters;
    }

    public boolean isActive() {
        return active;
    }

    public Date getCreated() {
        return created;
    }
}
