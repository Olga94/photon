package com.example.photon.data.storage.dto;

import com.example.photon.data.storage.realm.FiltersRealm;

public class FiltersDto {

    private String dish;
    private String nuances;
    private String decor;
    private String temperature;
    private String light;
    private String lightDirection;
    private String lightSource;

    public FiltersDto() {
    }

    public FiltersDto(FiltersRealm filters) {
        this.dish = filters.getDish();
        this.nuances = filters.getNuances();
        this.decor = filters.getDecor();
        this.temperature = filters.getTemperature();
        this.light = filters.getLight();
        this.lightDirection = filters.getLightDirection();
        this.lightSource = filters.getLightSource();
    }

    public FiltersDto(FiltersDto filters) {
        this.dish = filters.getDish();
        this.nuances = filters.getNuances();
        this.decor = filters.getDecor();
        this.temperature = filters.getTemperature();
        this.light = filters.getLight();
        this.lightDirection = filters.getLightDirection();
        this.lightSource = filters.getLightSource();
    }

    public String getDish() {
        return dish;
    }

    public void setDish(String dish) {
        this.dish = dish;
    }

    public String getNuances() {
        return nuances;
    }

    public void setNuances(String nuances) {
        this.nuances = nuances;
    }

    public String getDecor() {
        return decor;
    }

    public void setDecor(String decor) {
        this.decor = decor;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public String getLight() {
        return light;
    }

    public void setLight(String light) {
        this.light = light;
    }

    public String getLightDirection() {
        return lightDirection;
    }

    public void setLightDirection(String lightDirection) {
        this.lightDirection = lightDirection;
    }

    public String getLightSource() {
        return lightSource;
    }

    public void setLightSource(String lightSource) {
        this.lightSource = lightSource;
    }
}
