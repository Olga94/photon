package com.example.photon.data.network.error;

public class AccessesError extends Throwable {
    public AccessesError() {
        super("Неверный логин или пароль");
    }
}
