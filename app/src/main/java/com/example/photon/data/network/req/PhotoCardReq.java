package com.example.photon.data.network.req;

import com.example.photon.data.storage.dto.FiltersDto;

import java.util.List;

public class PhotoCardReq {

    private String album;
    private String title;
    private String photo;
    private List<String> tags;
    private FiltersDto filters;

    public PhotoCardReq(String title, List<String> tags, FiltersDto filters) {
        this.title = title;
        this.tags = tags;
        this.filters = filters;
    }

    public String getAlbum() {
        return album;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public FiltersDto getFilters() {
        return filters;
    }

    public void setFilters(FiltersDto filters) {
        this.filters = filters;
    }
}
