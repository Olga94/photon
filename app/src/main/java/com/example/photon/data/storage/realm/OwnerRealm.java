package com.example.photon.data.storage.realm;

import com.example.photon.data.network.res.AlbumRes;
import com.example.photon.data.network.res.OwnerRes;

import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class OwnerRealm extends RealmObject {

    @PrimaryKey
    private String id;
    private String name;
    private String login;
    private String avatar;
    private int albumCount;
    private int photocardCount;
    private RealmList<AlbumRealm> albums = new RealmList<>();

    public OwnerRealm() {
    }

    public OwnerRealm(OwnerRes ownerRes) {
        id = ownerRes.getId();
        name = ownerRes.getName();
        login = ownerRes.getLogin();
        avatar = ownerRes.getAvatar();
        albumCount = ownerRes.getAlbumCount();
        photocardCount = ownerRes.getPhotocardCount();
        for (AlbumRes album : ownerRes.getAlbums()) {
            if (album.isActive())
                albums.add(new AlbumRealm(album));
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public int getAlbumCount() {
        return albumCount;
    }

    public int getPhotoCardCount() {
        return photocardCount;
    }

    public void setAlbumCount(int albumCount) {
        this.albumCount = albumCount;
    }

    public void setPhotocardCount(int photocardCount) {
        this.photocardCount = photocardCount;
    }

    public void setAlbums(List<AlbumRes> albumResList) {
        albums = new RealmList<>();
        for (AlbumRes album : albumResList) {
            albums.add(new AlbumRealm(album));
        }
    }

    public RealmList<AlbumRealm> getAlbums() {
        return albums;
    }
}
