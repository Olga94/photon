package com.example.photon.data.network.req;

public class RegistrationReq {

    private String name;
    private String login;
    private String email;
    private String password;

    public RegistrationReq(String name, String login, String email, String password) {
        this.name = name;
        this.login = login;
        this.email = email;
        this.password = password;
    }
}
