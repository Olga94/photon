package com.example.photon.data.storage.dto;

import com.example.photon.data.storage.realm.TagRealm;

public class TagDto {

    private String tag;

    public TagDto(TagRealm tagRealm) {
        this.tag = tagRealm.getValue();
    }

    public String getTag() {
        return tag;
    }
}
