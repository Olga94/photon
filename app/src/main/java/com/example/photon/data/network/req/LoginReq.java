package com.example.photon.data.network.req;

public class LoginReq {

    private String email;
    private String password;

    public LoginReq(String email, String password) {
        this.email = email;
        this.password = password;
    }
}
