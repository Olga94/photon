package com.example.photon.data.storage.model;

public class Tag {
    private boolean check;
    private String tag;

    public Tag(String tag) {
        this.tag = tag;
    }

    public void setCheck(boolean check) {
        this.check = check;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public boolean isCheck() {
        return check;
    }

    public String getTag() {
        return tag;
    }
}
