package com.example.photon.data.network;

import com.example.photon.data.network.error.ErrorUtils;
import com.example.photon.data.network.error.NetworkAvailableError;
import com.example.photon.utils.NetworkStatusChecker;
import com.fernandocejas.frodo.annotation.RxLogObservable;

import retrofit2.Response;
import rx.Observable;

public class RestCallTransformer <R> implements Observable.Transformer<Response<R>, R> {
    @Override
    @RxLogObservable
    public Observable<R> call(Observable<Response<R>> responseObservable) {
        Observable<Boolean> networkStatus = NetworkStatusChecker.isInternetAvailable();

        return networkStatus
                .flatMap(aBoolean -> aBoolean ? responseObservable : Observable.error(new NetworkAvailableError()))
                .flatMap(rResponse -> {
                    switch (rResponse.code()) {
                        case 200:
                        case 201:
                        case 202:
                        case 204:
                            return Observable.just(rResponse.body());
                        default:
                            return Observable.error(ErrorUtils.parseError(rResponse));
                    }
                });
    }
}