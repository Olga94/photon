package com.example.photon.data.storage.model;

public class Find {

    private Search mSearch;
    private Filter mFilter;

    public Find() {
    }

    public Find(Search search) {
        mSearch = search;
    }

    public Find(Filter filter) {
        mFilter = filter;
    }

    public Search getSearch() {
        return mSearch;
    }

    public Filter getFilter() {
        return mFilter;
    }
}
