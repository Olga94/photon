package com.example.photon.data.storage.dto;

import com.example.photon.data.network.res.PhotoCardRes;
import com.example.photon.data.storage.realm.PhotoCardRealm;
import com.example.photon.data.storage.realm.TagRealm;

import java.util.ArrayList;
import java.util.List;

public class PhotoCardDto {

    private String id;
    private String owner;
    private String title;
    private String photo;
    private int views;
    private int favorits;
    private List<String> tags = new ArrayList<>();
    private FiltersDto filters;

    public PhotoCardDto(PhotoCardRealm photoCard) {
        id = photoCard.getId();
        owner = photoCard.getOwner();
        title = photoCard.getTitle();
        photo = photoCard.getPhoto();
        views = photoCard.getViews();
        favorits = photoCard.getFavorits();
        for (TagRealm tag : photoCard.getTags()) {
            tags.add(tag.getValue());
        }
        filters = new FiltersDto(photoCard.getFilters());
    }

    public PhotoCardDto(PhotoCardRes photoCard) {
        owner = photoCard.getOwner();
        title = photoCard.getTitle();
        photo = photoCard.getPhoto();
        views = photoCard.getViews();
        favorits = photoCard.getFavorits();
        for (String tag : photoCard.getTags()) {
            tags.add(tag);
        }
        filters = new FiltersDto(photoCard.getFilters());
    }

    public String getId() {
        return id;
    }

    public String getOwner() {
        return owner;
    }

    public String getTitle() {
        return title;
    }

    public String getPhoto() {
        return photo;
    }

    public int getViews() {
        return views;
    }

    public List<String> getTags() {
        return tags;
    }

    public int getFavorits() {
        return favorits;
    }

    public FiltersDto getFilters() {
        return filters;
    }
}
