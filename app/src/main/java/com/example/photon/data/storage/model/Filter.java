package com.example.photon.data.storage.model;

import java.util.ArrayList;
import java.util.List;

public class Filter {
    private List<String> mDishes = new ArrayList<>();
    private List<String> mColors = new ArrayList<>();
    private List<String> mDecors = new ArrayList<>();
    private List<String> mTemperatures = new ArrayList<>();
    private List<String> mLights = new ArrayList<>();
    private List<String> mLightDirs = new ArrayList<>();
    private List<String> mLightCount = new ArrayList<>();

    public List<String> getDishes() {
        return mDishes;
    }

    public void addDish(String dish) {
        mDishes.add(dish);
    }

    public List<String> getColors() {
        return mColors;
    }

    public void addColor(String color) {
        mColors.add(color);
    }

    public List<String> getDecors() {
        return mDecors;
    }

    public void addDecor(String decor) {
        mDecors.add(decor);
    }

    public List<String> getTemperatures() {
        return mTemperatures;
    }

    public void addTemperature(String temperature) {
        mTemperatures.add(temperature);
    }

    public List<String> getLights() {
        return mLights;
    }

    public void addLight(String light) {
        mLights.add(light);
    }

    public List<String> getLightDirs() {
        return mLightDirs;
    }

    public void addLightDir(String lightDir) {
        mLightDirs.add(lightDir);
    }

    public List<String> getLightCount() {
        return mLightCount;
    }

    public void addCountLight(String lightCount) {
        mLightCount.add(lightCount);
    }
}
