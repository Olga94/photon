package com.example.photon.data.network.error;

class ApiError extends Throwable {
    private int statusCode;

    public ApiError(int statusCode) {
        super("status code: " + statusCode);
        this.statusCode = statusCode;
    }

    public ApiError(String message) {
        super(message);
    }

    public ApiError(){}
}
