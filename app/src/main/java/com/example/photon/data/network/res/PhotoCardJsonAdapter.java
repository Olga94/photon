package com.example.photon.data.network.res;

import com.google.gson.internal.bind.util.ISO8601Utils;
import com.squareup.moshi.FromJson;
import com.squareup.moshi.ToJson;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class PhotoCardJsonAdapter {

    @FromJson
    PhotoCardRes photoCardResFromJson(PhotoCardJson photoCardJson) {
        return new PhotoCardRes(
                photoCardJson.id,
                photoCardJson.owner,
                photoCardJson.title,
                photoCardJson.photo,
                photoCardJson.created != null ? parseToDate(photoCardJson.created) : new Date(),
                photoCardJson.views,
                photoCardJson.favorits,
                photoCardJson.active,
                photoCardJson.tags,
                photoCardJson.filters);
    }

    @ToJson
    PhotoCardJson photoCardResToJson(PhotoCardRes photoCardRes) {
        DateFormat dateFormat = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss", Locale.ENGLISH);
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));

        PhotoCardJson photoCardJson = new PhotoCardJson();
        photoCardJson.id = photoCardRes.getId();
        photoCardJson.owner = photoCardRes.getOwner();
        photoCardJson.title = photoCardRes.getTitle();
        photoCardJson.photo =  photoCardRes.getPhoto();
        photoCardJson.created = dateFormat.format(photoCardRes.getCreated());
        photoCardJson.views = photoCardRes.getViews();
        photoCardJson.favorits = photoCardRes.getFavorits();
        photoCardJson.active = photoCardRes.isActive();
        photoCardJson.tags = photoCardRes.getTags();
        photoCardJson.filters = photoCardRes.getFilters();

        return photoCardJson;
    }

    private Date parseToDate(String date) {
        try {
            return ISO8601Utils.parse(date, new ParsePosition(0));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }
}

