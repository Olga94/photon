package com.example.photon.di.components;

import android.content.Context;

import com.example.photon.di.modules.AppModule;

import dagger.Component;

@Component(modules = AppModule.class)
public interface AppComponent {
    Context getContext();
}
