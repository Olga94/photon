package com.example.photon.di.components;

import com.example.photon.data.managers.DataManager;
import com.example.photon.di.modules.LocalModule;
import com.example.photon.di.modules.NetworkModule;

import javax.inject.Singleton;

import dagger.Component;

@Component(dependencies = AppComponent.class, modules = {NetworkModule.class, LocalModule.class})
@Singleton
public interface DataManagerComponent {
    void inject(DataManager dataManager);
}
