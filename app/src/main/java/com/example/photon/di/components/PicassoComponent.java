package com.example.photon.di.components;

import com.example.photon.di.modules.PicassoCacheModule;
import com.example.photon.di.scopes.RootScope;
import com.squareup.picasso.Picasso;

import dagger.Component;

@Component(dependencies = AppComponent.class, modules = PicassoCacheModule.class)
@RootScope
public interface PicassoComponent {
    Picasso getPicasso();
}
