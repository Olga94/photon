package com.example.photon.di.modules;

import android.content.Context;

import dagger.Module;
import dagger.Provides;

import com.example.photon.data.managers.PreferencesManager;
import com.example.photon.data.managers.RealmManager;

import javax.inject.Singleton;

@Module
public class LocalModule {
    @Provides
    @Singleton
    PreferencesManager providePreferencesManager(Context context) {
        return new PreferencesManager(context);
    }

    @Provides
    @Singleton
    RealmManager provideRealmManager() {
        return new RealmManager();
    }
}
