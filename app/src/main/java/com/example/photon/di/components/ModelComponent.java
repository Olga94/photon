package com.example.photon.di.components;

import com.example.photon.di.modules.ModelModule;
import com.example.photon.mvp.models.AbstractModel;

import javax.inject.Singleton;

import dagger.Component;

@Component(modules = ModelModule.class)
@Singleton
public interface ModelComponent {
    void inject(AbstractModel abstractModel);
}
