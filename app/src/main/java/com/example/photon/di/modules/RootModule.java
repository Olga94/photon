package com.example.photon.di.modules;

import com.example.photon.di.scopes.RootScope;
import com.example.photon.mvp.presenters.RootPresenter;

import dagger.Provides;

@dagger.Module
public class RootModule {
    @Provides
    @RootScope
    RootPresenter provideRootPresenter() {
        return new RootPresenter();
    }
}