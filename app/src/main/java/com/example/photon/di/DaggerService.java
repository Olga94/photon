package com.example.photon.di;

import android.content.Context;
import android.support.annotation.Nullable;

import java.util.HashMap;
import java.util.Map;

public class DaggerService {

    private static Map<Class, Object> sComponentMap = new HashMap<>();
    public static String SERVICE_NAME = "MY_DAGGER_SERVICE";

    @SuppressWarnings("unchecked")
    public static <T> T getDaggerComponent(Context context) {
        return (T) context.getSystemService(SERVICE_NAME);
    }


    public static void registerComponent(Class componentClass, Object daggerCompoent) {
        sComponentMap.put(componentClass, daggerCompoent);
    }

    @Nullable
    @SuppressWarnings("unchecked")
    public static <T> T getComponent(Class<T> componentClass) {
        Object component = sComponentMap.get(componentClass);
        return (T) component;
    }

}
