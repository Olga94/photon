package com.example.photon.di.modules;

import com.birbit.android.jobqueue.JobManager;
import com.birbit.android.jobqueue.config.Configuration;
import com.example.photon.data.managers.DataManager;
import com.example.photon.utils.App;
import com.example.photon.utils.ConstantManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ModelModule {
    @Provides
    @Singleton
    DataManager provideDataManager() {
        return DataManager.getInstance();
    }

    @Provides
    JobManager provideJobManager() {
        Configuration configuration = new Configuration.Builder(App.getContext())
                .minConsumerCount(ConstantManager.MIN_CONSUMER_COUNT) //минимальное кол-во потоков для решения задачи
                .maxConsumerCount(ConstantManager.MAX_CONSUMER_COUNT) //максимальное кол-во потоков для решения задачи
                .loadFactor(ConstantManager.LOAD_FACTOR) // кол-во задач на один поток
                .consumerKeepAlive(ConstantManager.KEEP_ALIVE) // ожидание 2 минуты на поток
                .build();

        return new JobManager(configuration);
    }
}
