package com.example.photon.jobs;

import android.annotation.TargetApi;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.widget.Toast;

import com.birbit.android.jobqueue.Job;
import com.birbit.android.jobqueue.Params;
import com.birbit.android.jobqueue.RetryConstraint;
import com.example.photon.data.managers.DataManager;
import com.example.photon.data.network.req.PhotoCardReq;
import com.example.photon.data.network.res.PhotoCardRes;
import com.example.photon.utils.App;
import com.example.photon.utils.ConstantManager;
import com.example.photon.utils.LocalStorageAvatar;

import java.io.File;

import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class SendPhotoCard extends Job {

    private final PhotoCardReq mPhotoCardReq;
    private final String mImageUri;

    public SendPhotoCard(PhotoCardReq photoCardReq, String imageUri) {
        super(new Params(JobPriority.HIGH)
                .requireNetwork()
                .persist());

        mPhotoCardReq = photoCardReq;
        mImageUri = imageUri;
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    public void onAdded() {
        File file = null;

        try {
            file = new File(LocalStorageAvatar.getPath(App.getContext(), Uri.parse(mImageUri)));
            file = Compressor.getDefault(App.getContext()).compressToFile(file);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (file != null) {
            RequestBody sendFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);

            MultipartBody.Part mBody = MultipartBody.Part.createFormData("avatar", file.getName(), sendFile);

            DataManager.getInstance()
                    .sendPhotoCard(mBody, mPhotoCardReq)
                    .subscribe(createResponse -> {
                        PhotoCardRes photoCardRes = new PhotoCardRes(mPhotoCardReq);
                        photoCardRes.setId(createResponse.getId());
                        photoCardRes.setOwner(DataManager.getInstance().getPreferencesManager().getUserId());
                        DataManager.getInstance().getRealmManager().savePhotoCardsToRealm(photoCardRes);
                    });
        } else {
            Observable.just("Повторите загрузку, произошла ошибка")
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(message -> Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show());
        }
    }

    @Override
    public void onRun() throws Throwable {


    }

    @Override
    protected void onCancel(int cancelReason, @Nullable Throwable throwable) {

    }

    @Override
    protected RetryConstraint shouldReRunOnThrowable(@NonNull Throwable throwable, int runCount, int maxRunCount) {
        return RetryConstraint.createExponentialBackoff(runCount, ConstantManager
                .INITIAL_BACK_OFF_IN_MS);
    }
}