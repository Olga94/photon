package com.example.photon.jobs;

import android.annotation.TargetApi;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.birbit.android.jobqueue.Job;
import com.birbit.android.jobqueue.Params;
import com.birbit.android.jobqueue.RetryConstraint;
import com.example.photon.data.managers.DataManager;
import com.example.photon.data.network.req.AlbumReq;
import com.example.photon.utils.ConstantManager;

public class SendAlbum extends Job {

    private final AlbumReq mAlbum;

    public SendAlbum(AlbumReq album) {
        super(new Params(JobPriority.MID)
                .requireNetwork()
                .persist());

        mAlbum = album;
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    public void onAdded() {
        DataManager.getInstance().sendAlbum(mAlbum)
                .subscribe(createResponse -> DataManager.getInstance().getRealmManager().saveAlbum(createResponse, mAlbum));
    }

    @Override
    public void onRun() throws Throwable {


    }

    @Override
    protected void onCancel(int cancelReason, @Nullable Throwable throwable) {

    }

    @Override
    protected RetryConstraint shouldReRunOnThrowable(@NonNull Throwable throwable, int runCount, int maxRunCount) {
        return RetryConstraint.createExponentialBackoff(runCount, ConstantManager
                .INITIAL_BACK_OFF_IN_MS);
    }
}
