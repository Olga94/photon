package com.example.photon.utils;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class MenuItemHolder {
    private final CharSequence itemTitle;
    private final int iconResId;
    private View.OnClickListener mListener = null;
    private MenuItem.OnMenuItemClickListener mMenuListener;
    private boolean isCustomView;

    public MenuItemHolder(CharSequence itemTitle, int iconResId, View.OnClickListener listener) {
        this.itemTitle = itemTitle;
        this.iconResId = iconResId;
        this.mListener = listener;
        isCustomView = true;
    }

    public MenuItemHolder(CharSequence itemTitle, int iconResId, MenuItem.OnMenuItemClickListener listener) {
        this.itemTitle = itemTitle;
        this.iconResId = iconResId;
        this.mMenuListener = listener;
        isCustomView = false;
    }

    public CharSequence getTitle() {
        return itemTitle;
    }

    public int getIconResId() {
        return iconResId;
    }

    public View.OnClickListener getListener() {
        return mListener;
    }

    public MenuItem.OnMenuItemClickListener getMenuListener() {
        return mMenuListener;
    }

    public boolean isCustomView() {
        return isCustomView;
    }
}
