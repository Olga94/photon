package com.example.photon.utils;

public class ConstantManager {
    public static final String BASE_URL = "http://207.154.248.163:5000/";
    public static final String LAST_MODIFIED_HEADER = "Last-Modified";
    public static final String IF_MODIFIED_SINCE_HEADER = "If-Modified-Since";
    public static final String AUTHORIZATION_HEADER = "Authorization";
    public static final String PATTERN_LOGIN = "^[a-zA-Z_0-9]{1,}$";
    public static final String PATTERN_PASSWORD = "^[a-zA-Z0-9]{8,24}$";
    public static final String PATTERN_NAME_ALBUM = "^[a-zA-Zа-яА-я0-9 ]{3,}$";
    public static final String POPUP_CONSTANT = "mPopup";
    public static final String POPUP_FORCE_SHOW_ICON = "setForceShowIcon";
    public static final String FILE_PROVIDER_AUTHORITY = "com.example.photon.fileprovider";

    public static final int PICK_PHOTO_FROM_GALLERY = 111;
    public static final int PICK_PHOTO_FROM_CAMERA = 222;
    public static final int REQUEST_PROFILE_PHOTO_GALLERY = 1001;
    public static final int REQUEST_PROFILE_PHOTO_CAMERA = 1002;
    public static final int REQUEST_PERMISSION_FOR_GALLERY_CODE = 3000;
    public static final int REQUEST_PERMISSION_FOR_GALLERY_PHOTOCARD_CODE = 3001;
    public static final int REQUEST_PERMISSION_CAMERA_CODE = 3002;
    public static final int MIN_CONSUMER_COUNT = 1;
    public static final int MAX_CONSUMER_COUNT = 3;
    public static final int LOAD_FACTOR = 3;
    public static final int KEEP_ALIVE = 120;

    public static final long MAX_CONNECTION_TIMEOUT = 5000;
    public static final long MAX_READ_TIMEOUT = 5000;
    public static final long MAX_WRITE_TIMEOUT = 5000;
    public static final long INITIAL_BACK_OFF_IN_MS = 1000;

}
