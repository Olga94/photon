package com.example.photon.utils;

import android.app.Application;
import android.content.Context;
import android.widget.Toast;

import com.example.photon.di.components.DaggerAppComponent;
import com.example.photon.di.modules.AppModule;
import com.example.photon.di.modules.RootModule;
import com.example.photon.ui.activities.DaggerRootActivity_RootComponent;
import com.example.photon.ui.activities.RootActivity;
import com.example.photon.di.DaggerService;
import com.example.photon.di.components.AppComponent;
import com.example.photon.mortar.ScreenScoper;
import com.facebook.stetho.Stetho;
import com.squareup.leakcanary.LeakCanary;

import io.realm.Realm;
import mortar.MortarScope;
import mortar.bundler.BundleServiceRunner;

public class App extends Application {

    private static Context sContext;
    private static AppComponent appComponent;
    private MortarScope mMortarScope;
    private MortarScope mRootActivityScope;
    private static RootActivity.RootComponent mRootComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        if (LeakCanary.isInAnalyzerProcess(this)) {
            return;
        }
        LeakCanary.install(this);

        Realm.init(this);

        sContext = getApplicationContext();
        Stetho.initializeWithDefaults(sContext);

        createDaggerComponent();
        createRootActivityComponent();

        mMortarScope = MortarScope.buildRootScope()
                .withService(DaggerService.SERVICE_NAME, appComponent)
                .build("Root");

        mRootActivityScope = mMortarScope.buildChild()
                .withService(DaggerService.SERVICE_NAME, mRootComponent)
                .withService(BundleServiceRunner.SERVICE_NAME, new BundleServiceRunner())
                .build(RootActivity.class.getName());

        ScreenScoper.registerScope(mMortarScope);
        ScreenScoper.registerScope(mRootActivityScope);
    }

    private void createRootActivityComponent() {
        mRootComponent = DaggerRootActivity_RootComponent.builder()
                .appComponent(appComponent)
                .rootModule(new RootModule())
                .build();
    }

    @Override
    public Object getSystemService(String name) {
        return (mMortarScope != null && mMortarScope.hasService(name)) ? mMortarScope.getService(name) : super.getSystemService(name);
    }

    public static Context getContext() {return sContext;}

    public static AppComponent getAppComponent() {
        return appComponent;
    }

    private void createDaggerComponent() {
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(sContext))
                .build();
    }
}
