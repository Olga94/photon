package com.example.photon.utils;

import android.util.Patterns;

public class Validation {

    public static boolean validEmail(String email) {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static boolean validLogin(String login) {
        return login.matches(ConstantManager.PATTERN_LOGIN);
    }

    public static boolean validName(String name) {
        return name.length() >= 3;
    }

    public static boolean validPassword(String pass) {
        return pass.matches(ConstantManager.PATTERN_PASSWORD);
    }

    public static boolean validDescription(String description) {
        return description.length() >= 3 && description.length() <= 400;
    }

    public static boolean validNameAlbum(String name) {
        return name.matches(ConstantManager.PATTERN_NAME_ALBUM);
    }
}
