package com.example.photon.utils;

import android.content.Context;
import android.support.v7.widget.PopupMenu;
import android.view.View;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class MenuHelper {

    public static PopupMenu createMenu(Context context, int menu, View view) {
        PopupMenu popup = new PopupMenu(context, view);
        try {
            Field[] fields = popup.getClass().getDeclaredFields();
            for (Field field : fields) {
                if (field.getName().equals(ConstantManager.POPUP_CONSTANT)) {
                    field.setAccessible(true);
                    Object menuPopupHelper = field.get(popup);
                    Class<?> classPopupHelper = Class.forName(menuPopupHelper.getClass().getName());
                    Method setForceIcons = classPopupHelper.getMethod(ConstantManager.POPUP_FORCE_SHOW_ICON, boolean.class);
                    setForceIcons.invoke(menuPopupHelper, true);
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        popup.inflate(menu);
        return popup;
    }
}
